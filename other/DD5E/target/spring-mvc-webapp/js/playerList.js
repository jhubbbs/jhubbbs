$(document).ready(function () {
    


    $('#add-button').click(function (event) {
        event.preventDefault();

        $.ajax({//NEED TO MAKE IN CONTROLLER
            type: 'POST',
            url: 'player',
            data: JSON.stringify({
                name: $('#add-name').val(),
                baseClass: $('#add-base-class').val(),
                race: $('#add-race').val()
            }),
            contentType: 'application/json; charset=utf-8',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },
            dataType: 'json'
        });
    });


    $("#search-button").click(function (event) { //NEED TO WRITE THIS IN CONTROLLER
        event.preventDefault();
        $.ajax({
            type: 'GET',
            url: 'player',
            dataType: 'json',
            data: JSON.stringify({
                name: $('#search-name').val()
            }),
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            }
        });

    });
});
