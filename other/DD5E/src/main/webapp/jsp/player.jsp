<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="s" uri="http://www.springframework.org/tags" %>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>DD5e - Character</title>
        <!-- add Bootstrap -->
        <link href="${pageContext.request.contextPath}/css/bootstrap.min.css"
              rel="stylesheet">


    </head>
    <body>
        <div class="container">
            <h1>DD5e - Character</h1>
            <hr />
            <div class="navbar-inverse">
                <ul class="nav nav-tabs">
                    <li role="presentation">
                        <a href="${pageContext.request.contextPath}/home">Home</a>
                    </li>
                    <li role="presentation" class="active">
                        <a href="${pageContext.request.contextPath}/player">Character</a>
                    </li>
                </ul>
            </div>
            <div class="row">
                <table id="demographicsTable">
                    <tbody id="demographicRows"></tbody>
                </table>
            </div>
            <div class="row">
                <div class="col-md-6 col-lg-6 col-xs-6">

                    <h2>Core Stats</h2>
                    <table id="statsTable" class="table table-hover table-striped">
                        <tbody id="statRows"></tbody>
                    </table>
                </div>

            </div>
        </div>             

        <script src="${pageContext.request.contextPath}/js/jquery-2.2.4.min.js"></script>
        <script src="${pageContext.request.contextPath}/js/bootstrap.min.js"></script>
        <script src="${pageContext.request.contextPath}/js/playerList.js"></script>
    </body>
</html>
