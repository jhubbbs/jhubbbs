<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="s" uri="http://www.springframework.org/tags" %>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>DD5e Home</title>
        <!-- add Bootstrap -->
        <link href="${pageContext.request.contextPath}/css/bootstrap.min.css"
              rel="stylesheet">



    </head>
    <body>
        <div class="container">
            <h1>DD5e Home</h1>
            <hr />
            <div class="navbar-inverse">
                <ul class="nav nav-tabs">
                    <li role="presentation" class="active">
                        <a href="${pageContext.request.contextPath}/home">Home</a>
                    </li>

                </ul>
            </div>
            <div class="row">

                <div class="col-md-6 col-lg-6 col-xs-6">

                    <h2>Add New Character</h2>
                    <div id="validationErrors" class="warning bg-danger"></div>

                    <form class="form-horizontal" role="form">
                        <div class="form-group">
                            <label for="add-name" 
                                   class="col-md-4 control-label">Name:</label>
                            <div class="col-md-8">
                                <input type="text" class="form-control" id="add-name" placeholder="Name" />
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="add-base-class" 
                                   class="col-md-4 control-label">Class:</label>
                            <div class="col-md-8">
                                <input type="text" class="form-control" id="add-base-class" placeholder="Class" />
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="add-race" 
                                   class="col-md-4 control-label">Race:</label>
                            <div class="col-md-8">
                                <input type="text" class="form-control" id="add-race" placeholder="Race" />
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-offset-4 col-md-8">
                                <button type="submit"
                                        id="add-button"
                                        class="btn btn-default">
                                    Add
                                </button>
                            </div>
                        </div>
                    </form>
                    
                    
                    
                    <h2>Load Character</h2>
                    <form class="form-horizontal" role="form">
                        <div class="form-group">
                            <label for="search-name" 
                                   class="col-md-4 control-label">Name:</label>
                            <div class="col-md-8">
                                <input type="text" class="form-control" id="search-name" placeholder="Name" />
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-md-offset-4 col-md-8">
                                <button type="submit"
                                        id="search-button"
                                        class="btn btn-default">
                                    Load
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>


        <script src="${pageContext.request.contextPath}/js/jquery-2.2.4.min.js"></script>
        <script src="${pageContext.request.contextPath}/js/bootstrap.min.js"></script>
        <script src="${pageContext.request.contextPath}/js/playerList.js"></script>
    </body>
</html>
