/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sg.dd5e.dao;


import com.sg.dd5e.model.Player;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 *
 * @author apprentice
 */
public class PlayerListDao {
    private Map<Integer, Player> playerMap = new HashMap<>();
    private static int playerIdCounter = 0;
    
    
    public void generatePlayers(){
        
        
        //for testing
        
    }
    
    public void calculateStats(int playerId){
        
        
        playerMap.get(playerId).setTotalStr(
                playerMap.get(playerId).getBaseStr() + playerMap.get(playerId).getModStr());
        
        playerMap.get(playerId).setTotalDex(
                playerMap.get(playerId).getBaseDex() + playerMap.get(playerId).getModDex());
        
        playerMap.get(playerId).setTotalCon(
                playerMap.get(playerId).getBaseCon() + playerMap.get(playerId).getModCon());
        
        playerMap.get(playerId).setTotalInt(
                playerMap.get(playerId).getBaseInt() + playerMap.get(playerId).getModInt());
        
        playerMap.get(playerId).setTotalWis(
                playerMap.get(playerId).getBaseWis() + playerMap.get(playerId).getModWis());
        
        playerMap.get(playerId).setTotalCha(
                playerMap.get(playerId).getBaseCha() + playerMap.get(playerId).getModCha());
        
    }

    
    public int addPlayer(Player player) {
        
        player.setPlayerId(playerIdCounter);
        
        playerMap.put(player.getPlayerId(), player);
        
        playerIdCounter++;
        
        return playerIdCounter - 1;
        
    }

    
    public Player getPlayerById(int playerId) {
        // Get the contact from the hashmap which uses the ID as its key
        return playerMap.get(playerId);
    }

    
    public List<Player> getAllPlayers() {
        // We want all of our contacts, but do we really need the hashmap?
        // Not really - we just need the list of contacts.
        // The ID is still on the Contact object, so values() would have what we need
        List<Player> allPlayers = new ArrayList<>(playerMap.values());
        return allPlayers;
    }

    
    public void updatePlayer(Player player) {
        // Updating a record on a hashmap is as simple as put with the Key and Value
        playerMap.put(player.getPlayerId(), player);
    }

    
    public void removePlayer(int playerId) {
        // remove the contact from the hashmap
        playerMap.remove(playerId);
    }

}
