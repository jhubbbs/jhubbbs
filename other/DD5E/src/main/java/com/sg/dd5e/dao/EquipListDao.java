/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sg.dd5e.dao;


import com.sg.dd5e.model.Equip;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 *
 * @author apprentice
 */
public class EquipListDao {
    private Map<Integer, Equip> equipMap = new HashMap<>();
    private static int equipIdCounter = 0;
    
    
    public void generateEquips(){
        
        
        //for testing
        
    }
    

    
    public void addEquip(Equip equip) {
        // We need to keep track of the ID manually right now
        // In our DB implementation, the database will assign the ID
        equip.setEquipId(equipIdCounter);
        // Once we assign the ID, we need to increment the counter for the next item
        equipIdCounter++;
        // add our item to the map
        equipMap.put(equip.getEquipId(), equip);
        
    }

    
    public Equip getEquipById(int equipId) {
        // Get the contact from the hashmap which uses the ID as its key
        return equipMap.get(equipId);
    }

    
    public List<Equip> getAllEquips() {
        // We want all of our contacts, but do we really need the hashmap?
        // Not really - we just need the list of contacts.
        // The ID is still on the Contact object, so values() would have what we need
        List<Equip> allEquips = new ArrayList<>(equipMap.values());
        return allEquips;
    }

    
    public void updateEquip(Equip equip) {
        // Updating a record on a hashmap is as simple as put with the Key and Value
        equipMap.put(equip.getEquipId(), equip);
    }

    
    public void removeEquip(int equipId) {
        // remove the contact from the hashmap
        equipMap.remove(equipId);
    }

}
