/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sg.dd5e.controller;


import com.sg.dd5e.dao.EquipListDao;
import com.sg.dd5e.dao.ItemListDao;
import com.sg.dd5e.dao.PlayerListDao;
import com.sg.dd5e.model.Player;
import javax.inject.Inject;
import javax.validation.Valid;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 *
 * @author parallels
 */
@Controller
public class HomeController {
    
    private PlayerListDao pdao;
    //private EquipListDao edao;
    //private ItemListDao idao;
    
   
  
    @Inject
    public HomeController(PlayerListDao dao) {
        this.pdao = dao;
        //this.edao = edao;
        //this.idao = idao;
    }
    

    @RequestMapping(value = {"/", "/home"}, method = RequestMethod.GET)
    public String displayHomePage() {
        return "home";
    }


    @RequestMapping(value = "/player/{id}", method = RequestMethod.GET)
    @ResponseBody
    public Player getPlayer(@PathVariable("id") int id) {
        return pdao.getPlayerById(id);
    }

    

    @RequestMapping(value = "/player", method = RequestMethod.POST)
    @ResponseStatus(HttpStatus.CREATED)
    @ResponseBody
    public void createPlayer(@Valid @RequestBody Player player) {
        int id = pdao.addPlayer(player);
        
       //we need to instead load the player.jsp and then get player
       getPlayer(id);
    }
    
    

    
}
