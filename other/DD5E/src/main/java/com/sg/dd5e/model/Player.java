/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sg.dd5e.model;



/**
 *
 * @author apprentice
 */
public class Player {
    private int playerId;
    private String name;
    private int level;
    private int exp;
    private String race;
    private String baseClass;
    private String alignment;
    private double gold;
    
    private int maxHP;
    
    private int ac;
    
    private int baseStr;
    private int baseDex;
    private int baseCon;
    private int baseInt;
    private int baseWis;
    private int baseCha;
    
    private int modStr;
    private int modDex;
    private int modCon;
    private int modInt;
    private int modWis;
    private int modCha;
    
    private int totalStr;
    private int totalDex;
    private int totalCon;
    private int totalInt;
    private int totalWis;
    private int totalCha;
    
    private Equip armor;
    private Equip rightHand;
    private Equip leftHand;
    private Equip other;

    /**
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * @param name the name to set
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * @return the level
     */
    public int getLevel() {
        return level;
    }

    /**
     * @param level the level to set
     */
    public void setLevel(int level) {
        this.level = level;
    }

    /**
     * @return the exp
     */
    public int getExp() {
        return exp;
    }

    /**
     * @param exp the exp to set
     */
    public void setExp(int exp) {
        this.exp = exp;
    }

    /**
     * @return the race
     */
    public String getRace() {
        return race;
    }

    /**
     * @param race the race to set
     */
    public void setRace(String race) {
        this.race = race;
    }

    /**
     * @return the baseClass
     */
    public String getBaseClass() {
        return baseClass;
    }

    /**
     * @param baseClass the baseClass to set
     */
    public void setBaseClass(String baseClass) {
        this.baseClass = baseClass;
    }

    /**
     * @return the alignment
     */
    public String getAlignment() {
        return alignment;
    }

    /**
     * @param alignment the alignment to set
     */
    public void setAlignment(String alignment) {
        this.alignment = alignment;
    }

    /**
     * @return the gold
     */
    public double getGold() {
        return gold;
    }

    /**
     * @param gold the gold to set
     */
    public void setGold(double gold) {
        this.gold = gold;
    }

    /**
     * @return the baseStr
     */
    public int getBaseStr() {
        return baseStr;
    }

    /**
     * @param baseStr the baseStr to set
     */
    public void setBaseStr(int baseStr) {
        this.baseStr = baseStr;
    }

    /**
     * @return the baseDex
     */
    public int getBaseDex() {
        return baseDex;
    }

    /**
     * @param baseDex the baseDex to set
     */
    public void setBaseDex(int baseDex) {
        this.baseDex = baseDex;
    }

    /**
     * @return the baseCon
     */
    public int getBaseCon() {
        return baseCon;
    }

    /**
     * @param baseCon the baseCon to set
     */
    public void setBaseCon(int baseCon) {
        this.baseCon = baseCon;
    }

    /**
     * @return the baseInt
     */
    public int getBaseInt() {
        return baseInt;
    }

    /**
     * @param baseInt the baseInt to set
     */
    public void setBaseInt(int baseInt) {
        this.baseInt = baseInt;
    }

    /**
     * @return the baseWis
     */
    public int getBaseWis() {
        return baseWis;
    }

    /**
     * @param baseWis the baseWis to set
     */
    public void setBaseWis(int baseWis) {
        this.baseWis = baseWis;
    }

    /**
     * @return the baseCha
     */
    public int getBaseCha() {
        return baseCha;
    }

    /**
     * @param baseCha the baseCha to set
     */
    public void setBaseCha(int baseCha) {
        this.baseCha = baseCha;
    }

    /**
     * @return the modStr
     */
    public int getModStr() {
        return modStr;
    }

    /**
     * @param modStr the modStr to set
     */
    public void setModStr(int modStr) {
        this.modStr = modStr;
    }

    /**
     * @return the modDex
     */
    public int getModDex() {
        return modDex;
    }

    /**
     * @param modDex the modDex to set
     */
    public void setModDex(int modDex) {
        this.modDex = modDex;
    }

    /**
     * @return the modCon
     */
    public int getModCon() {
        return modCon;
    }

    /**
     * @param modCon the modCon to set
     */
    public void setModCon(int modCon) {
        this.modCon = modCon;
    }

    /**
     * @return the modInt
     */
    public int getModInt() {
        return modInt;
    }

    /**
     * @param modInt the modInt to set
     */
    public void setModInt(int modInt) {
        this.modInt = modInt;
    }

    /**
     * @return the modWis
     */
    public int getModWis() {
        return modWis;
    }

    /**
     * @param modWis the modWis to set
     */
    public void setModWis(int modWis) {
        this.modWis = modWis;
    }

    /**
     * @return the modCha
     */
    public int getModCha() {
        return modCha;
    }

    /**
     * @param modCha the modCha to set
     */
    public void setModCha(int modCha) {
        this.modCha = modCha;
    }

    /**
     * @return the totalStr
     */
    public int getTotalStr() {
        return totalStr;
    }

    /**
     * @param totalStr the totalStr to set
     */
    public void setTotalStr(int totalStr) {
        this.totalStr = totalStr;
    }

    /**
     * @return the totalDex
     */
    public int getTotalDex() {
        return totalDex;
    }

    /**
     * @param totalDex the totalDex to set
     */
    public void setTotalDex(int totalDex) {
        this.totalDex = totalDex;
    }

    /**
     * @return the totalCon
     */
    public int getTotalCon() {
        return totalCon;
    }

    /**
     * @param totalCon the totalCon to set
     */
    public void setTotalCon(int totalCon) {
        this.totalCon = totalCon;
    }

    /**
     * @return the totalInt
     */
    public int getTotalInt() {
        return totalInt;
    }

    /**
     * @param totalInt the totalInt to set
     */
    public void setTotalInt(int totalInt) {
        this.totalInt = totalInt;
    }

    /**
     * @return the totalWis
     */
    public int getTotalWis() {
        return totalWis;
    }

    /**
     * @param totalWis the totalWis to set
     */
    public void setTotalWis(int totalWis) {
        this.totalWis = totalWis;
    }

    /**
     * @return the totalCha
     */
    public int getTotalCha() {
        return totalCha;
    }

    /**
     * @param totalCha the totalCha to set
     */
    public void setTotalCha(int totalCha) {
        this.totalCha = totalCha;
    }

    /**
     * @return the armor
     */
    public Equip getArmor() {
        return armor;
    }

    /**
     * @param armor the armor to set
     */
    public void setArmor(Equip armor) {
        this.armor = armor;
    }

    /**
     * @return the rightHand
     */
    public Equip getRightHand() {
        return rightHand;
    }

    /**
     * @param rightHand the rightHand to set
     */
    public void setRightHand(Equip rightHand) {
        this.rightHand = rightHand;
    }

    /**
     * @return the leftHand
     */
    public Equip getLeftHand() {
        return leftHand;
    }

    /**
     * @param leftHand the leftHand to set
     */
    public void setLeftHand(Equip leftHand) {
        this.leftHand = leftHand;
    }

    /**
     * @return the other
     */
    public Equip getOther() {
        return other;
    }

    /**
     * @param other the other to set
     */
    public void setOther(Equip other) {
        this.other = other;
    }


    /**
     * @return the playerId
     */
    public int getPlayerId() {
        return playerId;
    }

    /**
     * @param playerId the playerId to set
     */
    public void setPlayerId(int playerId) {
        this.playerId = playerId;
    }

    /**
     * @return the maxHP
     */
    public int getMaxHP() {
        return maxHP;
    }

    /**
     * @param maxHP the maxHP to set
     */
    public void setMaxHP(int maxHP) {
        this.maxHP = maxHP;
    }


    /**
     * @return the ac
     */
    public int getAc() {
        return ac;
    }

    /**
     * @param ac the ac to set
     */
    public void setAc(int ac) {
        this.ac = ac;
    }
}
