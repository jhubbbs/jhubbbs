/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sg.dd5e.controller;


import com.sg.dd5e.dao.EquipListDao;
import com.sg.dd5e.dao.ItemListDao;
import com.sg.dd5e.dao.PlayerListDao;
import com.sg.dd5e.model.Equip;
import com.sg.dd5e.model.Item;
import com.sg.dd5e.model.Player;
import java.util.List;
import javax.inject.Inject;
import javax.validation.Valid;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 *
 * @author parallels
 */
@Controller
public class PlayerController {
    

    private PlayerListDao pdao;
    //private EquipListDao edao;
    //private ItemListDao idao;
    
    private int activePlayerId;
    private int activeHP;
    
  
    @Inject
    public PlayerController(PlayerListDao dao) {
        this.pdao = dao;
        //this.edao = edao;
       // this.idao = idao;
    }

    @RequestMapping(value = {"/player"}, method = RequestMethod.GET)
    public String displayPlayerPage() {
        return "player";
    }


    /*
    @RequestMapping(value = "/equip/{id}", method = RequestMethod.GET)
    @ResponseBody
    public Equip getEquip(@PathVariable("id") int id) {
        return edao.getEquipById(id);
    }
    
    @RequestMapping(value = "/item/{id}", method = RequestMethod.GET)
    @ResponseBody
    public Item getItem(@PathVariable("id") int id) {
        return idao.getItemById(id);
    }

    
    @RequestMapping(value = "/items", method = RequestMethod.GET)
    @ResponseBody
    public List<Item> getAllItems() {

        if (idao.getAllItems().isEmpty()) {//FOR TESTING ONLY
            idao.generateItems();
        }

        return idao.getAllItems();
    }
    
    @RequestMapping(value = "/equips", method = RequestMethod.GET)
    @ResponseBody
    public List<Equip> getAllEquips() {

        if (edao.getAllEquips().isEmpty()) {//FOR TESTING ONLY
            edao.generateEquips();
        }

        return edao.getAllEquips();
    }


    @RequestMapping(value = "/item", method = RequestMethod.POST)
    @ResponseStatus(HttpStatus.CREATED)
    @ResponseBody
    public Item createItem(@Valid @RequestBody Item item) {
        idao.addItem(item);
        return item;
    }

    @RequestMapping(value = "/equip", method = RequestMethod.POST)
    @ResponseStatus(HttpStatus.CREATED)
    @ResponseBody
    public Equip createEquip(@Valid @RequestBody Equip equip) {
        edao.addEquip(equip);
        return equip;
    }
    

    @RequestMapping(value = "/item/{id}", method = RequestMethod.DELETE)
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void deleteItem(@PathVariable("id") int id) {
        idao.removeItem(id);
    }
    
    @RequestMapping(value = "/equip/{id}", method = RequestMethod.DELETE)
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void deleteEquip(@PathVariable("id") int id) {
        edao.removeEquip(id);
    }

   */
    @RequestMapping(value = "/player/{id}", method = RequestMethod.PUT)
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void putPlayer(@PathVariable("id") int id, @Valid @RequestBody Player player) {
        player.setPlayerId(id);
        pdao.updatePlayer(player);
    }
}
