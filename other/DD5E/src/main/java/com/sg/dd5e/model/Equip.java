/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sg.dd5e.model;

/**
 *
 * @author apprentice
 */
public class Equip {
    private int equipId;
    private String name;
    private String description;
    private String type;
    
    private int equipStr;
    private int equipDex;
    private int equipCon;
    private int equipInt;
    private int equipWis;
    private int equipCha;

    /**
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * @param name the name to set
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * @return the description
     */
    public String getDescription() {
        return description;
    }

    /**
     * @param description the description to set
     */
    public void setDescription(String description) {
        this.description = description;
    }

    /**
     * @return the type
     */
    public String getType() {
        return type;
    }

    /**
     * @param type the type to set
     */
    public void setType(String type) {
        this.type = type;
    }

    /**
     * @return the equipStr
     */
    public int getEquipStr() {
        return equipStr;
    }

    /**
     * @param equipStr the equipStr to set
     */
    public void setEquipStr(int equipStr) {
        this.equipStr = equipStr;
    }

    /**
     * @return the equipDex
     */
    public int getEquipDex() {
        return equipDex;
    }

    /**
     * @param equipDex the equipDex to set
     */
    public void setEquipDex(int equipDex) {
        this.equipDex = equipDex;
    }

    /**
     * @return the equipCon
     */
    public int getEquipCon() {
        return equipCon;
    }

    /**
     * @param equipCon the equipCon to set
     */
    public void setEquipCon(int equipCon) {
        this.equipCon = equipCon;
    }

    /**
     * @return the equipInt
     */
    public int getEquipInt() {
        return equipInt;
    }

    /**
     * @param equipInt the equipInt to set
     */
    public void setEquipInt(int equipInt) {
        this.equipInt = equipInt;
    }

    /**
     * @return the equipWis
     */
    public int getEquipWis() {
        return equipWis;
    }

    /**
     * @param equipWis the equipWis to set
     */
    public void setEquipWis(int equipWis) {
        this.equipWis = equipWis;
    }

    /**
     * @return the equipCha
     */
    public int getEquipCha() {
        return equipCha;
    }

    /**
     * @param equipCha the equipCha to set
     */
    public void setEquipCha(int equipCha) {
        this.equipCha = equipCha;
    }

    /**
     * @return the equipId
     */
    public int getEquipId() {
        return equipId;
    }

    /**
     * @param equipId the equipId to set
     */
    public void setEquipId(int equipId) {
        this.equipId = equipId;
    }
}
