DROP DATABASE IF EXISTS `filmapp_test`;
CREATE DATABASE IF NOT EXISTS `filmapp_test`;

USE `filmapp_test`;

CREATE TABLE IF NOT EXISTS `films` (
 `filmId` int(20) NOT NULL AUTO_INCREMENT,
 `title` varchar(100) NOT NULL,
 `releaseYear` varchar(4) NOT NULL,
 `mpaa` varchar(10),
 `director` varchar(50) NOT NULL,
 `note` varchar(200),
 `score` varchar(1),
 `imgUrl` varchar(200),
 PRIMARY KEY (`filmId`)
);

