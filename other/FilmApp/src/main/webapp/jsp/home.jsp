<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="s" uri="http://www.springframework.org/tags" %>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Films App</title>
        <link href="${pageContext.request.contextPath}/css/bootstrap.min.css"
              rel="stylesheet">
        <style>
            .capsule {background-color: #98878F; border-radius:10px;}
            h1 {color: #985E6D; text-shadow: 2px 2px #192231; font-style: italic;}
            h2 {color: #192231;}
            body {color: #192231;}
            a {color: #985E6D;}
            th {background-color: #98878F;}
            .table-striped>tbody>tr:nth-child(even)>td, 
            .table-striped>tbody>tr:nth-child(even)>th {
                background-color: gainsboro; 
            }
        </style>
    </head>
    <body style="background-color: #494E6B">
        <div class="container">
            <h1>Films App</h1>
            <hr />


            <div class="row">
                <div class="col-md-8 col-lg-8 col-xs-11">

                    <form class="form-horizontal" role="form">
                        <div class="form-group">

                            <div class="col-md-6 col-lg-6 col-xs-10">
                                <input type="text" class="form-control" id="search-term" placeholder="Search by Title, Director, or Year" />
                            </div>
                            <div class="col-md-2 col-lg-2 col-xs-2">
                                <button type="submit"
                                        id="search-button"
                                        class="btn btn-default">
                                    Search
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
            <div class="col">
                <div class="capsule col-md-5 col-lg-5 col-xs-12">
                    <h2>Films</h2>
                    <table id="filmTable" class="table table-striped">
                        <tr>
                            <th width="40%">Title</th>
                            <th width="30%">Director</th>
                            <th width="15%"></th>
                            <th width="15%"></th>
                        </tr>
                        <!--Films via JS populate here-->
                        <tbody id="contentRows"></tbody>
                    </table>
                </div>

                <div class="col-md-1 col-lg-1 col-xs-1">
                </div>

                <div class="capsule col-md-5 col-lg-5 col-xs-12">
                    <!-- Add film form -->
                    <h2>Add New Film</h2>
                    <div id="validationErrors" class="warning bg-danger"></div>

                    <form class="form-horizontal" role="form">
                        <div class="form-group">
                            <label for="add-title" 
                                   class="col-md-4 control-label">Title:</label>
                            <div class="col-md-8">
                                <input type="text" class="form-control" id="add-title" placeholder="Title" />
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="add-release-year" 
                                   class="col-md-4 control-label">Release Year:</label>
                            <div class="col-md-8">
                                <input type="text" class="form-control" id="add-release-year" placeholder="Release Year" />
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="add-director" 
                                   class="col-md-4 control-label">Director:</label>
                            <div class="col-md-8">
                                <input type="text" class="form-control" id="add-director" placeholder="Director" />
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="add-mpaa" 
                                   class="col-md-4 control-label">MPAA Rating:</label>
                            <div class="col-md-8">
                                <input type="text" class="form-control" id="add-mpaa" placeholder="MPAA Rating" />
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="add-note" 
                                   class="col-md-4 control-label">Note:</label>
                            <div class="col-md-8">
                                <textarea class="form-control" rows="5" id="add-note" placeholder="Note (< 100 characters)"></textarea>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="add-score" class="col-md-4 control-label">Score:</label>
                            <div class="col-md-8">
                                <select class="form-control" id="add-score" placeholder="Score">
                                    <option></option>
                                    <option>1</option>
                                    <option>2</option>
                                    <option>3</option>
                                    <option>4</option>
                                    <option>5</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="add-img-url" 
                                   class="col-md-4 control-label">Image URL:</label>
                            <div class="col-md-8">
                                <input type="text" class="form-control" id="add-img-url" placeholder="Image URL" />
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-md-offset-4 col-md-8">
                                <button type="submit"
                                        id="add-button"
                                        class="btn btn-default">
                                    Add
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>

            <div class="modal fade" id="detailsModal" tabindex="-1" role="dialog"
                 aria-labelledby="detailsModalLabel" aria-hidden="true">
                <div class="modal-dialog">
                    <div class="capsule modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal">
                                <span aria-hidden="true">&times;</span>
                                <span class="sr-only">Close</span>
                            </button>
                            <h4 class="modal-title" id="detailsModalLabel">Film Details</h4>
                        </div>
                        <div class="modal-body">
                            <div class="row">
                                <div class="col-md-5 col-lg-5 ">
                                
                                <h1 id="film-title"></h1>
                                <h2 id="film-director"></h2>
                                <h3 id="film-releaseYear"></h3>
                                <h3 id="film-mpaa"></h3>
                                </div>
                                <div class="col-md-5 col-lg-5 ">
                                <img src="" id="details-img" style="width:300px;height:400px;">
                                </div>
                            </div>
                            <div class ="row">
                                <table class="table">
                                    <tr>
                                        <th>Score:</th>
                                        <td id="film-score"></td>
                                    </tr>
                                    <tr>
                                        <th>Note:</th>
                                        <td id="film-note"></td>
                                    </tr>
                                </table>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal fade" id="editModal" tabindex="-1" role="dialog"
                 aria-labelledby="editDetailsModalLabel" aria-hidden="true">
                <div class="modal-dialog">
                    <div class="capsule modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal">
                                <span aria-hidden="true">&times;</span>
                                <span class="sr-only">Close</span>
                            </button>
                            <h4 class="modal-title" id="editDetailsModalLabel">Edit Film</h4>
                        </div>
                        <div class="modal-body">
                            <form class="form-horizontal" role="form">
                                <div class="form-group">
                                    <label for="edit-title" 
                                           class="col-md-4 control-label">Title:</label>
                                    <div class="col-md-8">
                                        <input type="text" class="form-control" id="edit-title" placeholder="Title" />
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="edit-release-year" 
                                           class="col-md-4 control-label">Release Year:</label>
                                    <div class="col-md-8">
                                        <input type="text" class="form-control" id="edit-release-year" placeholder="Release Year" />
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="edit-director" 
                                           class="col-md-4 control-label">Director:</label>
                                    <div class="col-md-8">
                                        <input type="text" class="form-control" id="edit-director" placeholder="Director" />
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="edit-mpaa" 
                                           class="col-md-4 control-label">MPAA Rating:</label>
                                    <div class="col-md-8">
                                        <input type="text" class="form-control" id="edit-mpaa" placeholder="MPAA Rating" />
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="edit-note" 
                                           class="col-md-4 control-label">Note:</label>
                                    <div class="col-md-8">
                                        <textarea class="form-control" rows="5" id="edit-note" placeholder="Note"></textarea>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="edit-score" class="col-md-4 control-label">Score:</label>
                                    <div class="col-md-8">
                                        <select class="form-control" id="edit-score" placeholder="Score">
                                            <option></option>
                                            <option>1</option>
                                            <option>2</option>
                                            <option>3</option>
                                            <option>4</option>
                                            <option>5</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="edit-img-url" 
                                           class="col-md-4 control-label">Image URL:</label>
                                    <div class="col-md-8">
                                        <input type="text" class="form-control" id="edit-img-url" placeholder="Image URL" />
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-md-offset-4 col-md-8">
                                        <button type="submit"
                                                id="edit-button"
                                                class="btn btn-default" data-dismiss="modal">
                                            Edit Film
                                        </button>
                                        <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>

                                        <input type="hidden" id="edit-film-id" />
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <hr>
        <p style="text-align: center;">by jhubbbs</p>

        <script src="${pageContext.request.contextPath}/js/jquery-2.2.4.min.js"></script>
        <script src="${pageContext.request.contextPath}/js/bootstrap.min.js"></script>
        <script src="${pageContext.request.contextPath}/js/filmApp.js"></script>
    </body>
</html>
