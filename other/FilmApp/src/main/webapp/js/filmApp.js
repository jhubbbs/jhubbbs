
$(document).ready(function () {
    loadFilms();

    // add the onclick handling for our add button
    $('#add-button').click(function (event) {
        event.preventDefault();
        // need to submit this via AJAX
        $.ajax({
            type: 'POST',
            url: 'film',
            // make the JSON film
            data: JSON.stringify({
                title: $('#add-title').val(),
                releaseYear: $('#add-release-year').val(),
                mpaa: $('#add-mpaa').val(),
                director: $('#add-director').val(),
                note: $('#add-note').val(),
                score: $('#add-score').val(),
                imgUrl: $('#add-img-url').val()
            }),
            contentType: 'application/json; charset=utf-8',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },
            dataType: 'json'
        }).success(function (data, status) {
            // clear the form and reload the summary table
            $('#add-title').val('');
            $('#add-release-year').val('');
            $('#add-director').val('');
            $('#add-mpaa').val('');
            $('#add-note').val('');
            $('#add-score').val('');
            $('#add-img-url').val('');

            // reload the summary table
            $('#validationErrors').empty();
            loadFilms();
        }).error(function (data, status) {
            $('#validationErrors').empty();
            $.each(data.responseJSON.fieldErrors, function (index, validationError) {
                var errorDiv = $('#validationErrors');
                errorDiv.append(validationError.message).append($('<br>'));
            });
        });
    });

    $('#edit-button').click(function (event) {
        event.preventDefault();
        // update our film via AJAX
        $.ajax({
            type: 'PUT',
            url: 'film/' + $('#edit-film-id').val(),
            data: JSON.stringify({
                filmId: $('#edit-film-id').val(),
                title: $('#edit-title').val(),
                releaseYear: $('#edit-release-year').val(),
                mpaa: $('#edit-mpaa').val(),
                director: $('#edit-director').val(),
                note: $('#edit-note').val(),
                score: $('#edit-score').val(),
                imgUrl: $('#edit-img-url').val()
            }),
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },
            'dataType': 'json'
        }).success(function () {
            loadFilms();
        });
    });

    $("#search-button").click(function (event) {
        event.preventDefault();

        var searchTerm = $('#search-term').val();
        if (searchTerm === '') {
            searchTerm = "EMPTYSTRING";
        }
        $.ajax({
            type: 'GET',
            url: 'search/' + searchTerm

        }).success(function (searchResults, status) {
            $('#search-term').val('');

            fillFilmTable(searchResults, status);
        });
    });
});

// Load films into our summary table
function loadFilms() {
    //  Get our JSON object from the endpoint
    $.ajax({
        url: 'films'
    }).success(function (data, status) {
        fillFilmTable(data, status);
    });
}

// used with Search button and loadFilm function
function fillFilmTable(filmLibrary, status) {
    // clear the previous list
    clearFilmTable();

    // store our tbody in a variable 
    var summaryTable = $('#contentRows');

    $.each(filmLibrary, function (arrayPosition, myFilm) {
        summaryTable.append($('<tr>')
                .append($('<td>')
                        .append($('<a>')
                                .attr({
                                    'data-film-id': myFilm.filmId,
                                    'data-toggle': 'modal',
                                    'data-target': '#detailsModal'
                                })
                                .text(myFilm.title + ' (' + myFilm.releaseYear + ')')))
                .append($('<td>').text(myFilm.director))
                .append($('<td>')
                        .append($('<a>')
                                .attr({
                                    'data-film-id': myFilm.filmId,
                                    'data-toggle': 'modal',
                                    'data-target': '#editModal'
                                })
                                .text('Edit')))
                .append($('<td>')
                        .append($('<a>')
                                .attr({
                                    'onClick': 'deleteFilm(' + myFilm.filmId + ')'
                                })
                                .text('Delete'))));
    });
}

// Clear all the rows from our summary table
function clearFilmTable() {
    $('#contentRows').empty();
}

// Run in response to the show.bs.modal event
// It gets the film data and renders it to the dialog
$('#detailsModal').on('show.bs.modal', function (event) {
    var element = $(event.relatedTarget);
    var filmId = element.data('film-id');
    var modal = $(this);
    // Get our object via AJAX
    $.ajax({
        type: 'GET',
        url: 'film/' + filmId
    }).success(function (sampleFilm) {
        modal.find('#film-id').text(sampleFilm.filmId);
        $('#details-img').attr('src', sampleFilm.imgUrl);
        modal.find('#film-title').text(sampleFilm.title);
        modal.find('#film-releaseYear').text(sampleFilm.releaseYear);
        modal.find('#film-director').text(sampleFilm.director);
        modal.find('#film-mpaa').text(sampleFilm.mpaa);
        modal.find('#film-note').text(sampleFilm.note);
        modal.find('#film-score').text(sampleFilm.score+"/5");

    });
});

$('#editModal').on('show.bs.modal', function (event) {
    var element = $(event.relatedTarget);
    var filmId = element.data('film-id');
    var modal = $(this);

    // get our object via AJAX
    $.ajax({
        type: 'GET',
        url: 'film/' + filmId
    }).success(function (sampleEditFilm) {
        modal.find('#edit-film-id').val(sampleEditFilm.filmId);
        modal.find('#edit-title').val(sampleEditFilm.title);
        modal.find('#edit-release-year').val(sampleEditFilm.releaseYear);
        modal.find('#edit-director').val(sampleEditFilm.director);
        modal.find('#edit-mpaa').val(sampleEditFilm.mpaa);
        modal.find('#edit-note').val(sampleEditFilm.note);
        modal.find('#edit-score').val(sampleEditFilm.score);
        modal.find('#edit-img-url').val(sampleEditFilm.imgUrl);
    });
});

function deleteFilm(id) {
    var answer = confirm('Do you really want to delete this film?');

    if (answer === true) {
        $.ajax({
            type: 'DELETE',
            url: 'film/' + id
        }).success(function () {
            // reload summary
            loadFilms();
        });
    }
}        