/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.filmapp.service;

import com.mycompany.filmapp.dao.FilmAppDaoInterface;
import com.mycompany.filmapp.model.Film;
import java.util.List;
import javax.inject.Inject;
import javax.validation.Valid;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 *
 * @author jhubbbs
 */
@Controller
public class FilmAppDaoAPI {

    private FilmAppDaoInterface dao;

    @Inject
    public FilmAppDaoAPI(FilmAppDaoInterface dao) {
        this.dao = dao;
    }

    @RequestMapping(value = {"/", "/home"}, method = RequestMethod.GET)
    public String displayHomePage() {
        return "home";
    }

    @RequestMapping(value = "/films", method = RequestMethod.GET)
    @ResponseBody
    public List<Film> getAllFilms() {
        return dao.getAllFilms();
    }

    @RequestMapping(value = "/search/{searchTerm}", method = RequestMethod.GET)
    @ResponseBody
    public List<Film> getFilmsBySearch(@PathVariable("searchTerm") String searchTerm) {

        if (searchTerm.equals("EMPTYSTRING")) {
            return dao.getAllFilms();
        } else {
            return dao.getFilmsBySearch(searchTerm);
        }

    }

    @RequestMapping(value = "/film/{id}", method = RequestMethod.PUT)
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void updateFilm(@PathVariable("id") int id, @Valid @RequestBody Film film) {
        film.setFilmId(id);
        dao.updateFilm(film);
    }

    @RequestMapping(value = "/film/{id}", method = RequestMethod.DELETE)
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void deleteFilm(@PathVariable("id") int id) {
        dao.deleteFilm(id);
    }

    @RequestMapping(value = "/film/{id}", method = RequestMethod.GET)
    @ResponseBody
    public Film getFilm(@PathVariable("id") int id) {
        return dao.getFilmById(id);
    }

    @RequestMapping(value = "/film", method = RequestMethod.POST)
    @ResponseStatus(HttpStatus.CREATED)
    @ResponseBody
    public Film createFilm(@Valid @RequestBody Film film) {
        dao.addFilm(film);
        return film;
    }
}
