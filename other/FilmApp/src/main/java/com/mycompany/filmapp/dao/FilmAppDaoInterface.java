/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.filmapp.dao;

import com.mycompany.filmapp.model.Film;
import java.util.List;

/**
 *
 * @author jhubbbs
 */
public interface FilmAppDaoInterface {

    public void addFilm(Film film);

    public void deleteFilm(int filmId);

    public void updateFilm(Film film);

    public Film getFilmById(int id);

    public List<Film> getAllFilms();

    public List<Film> getFilmsBySearch(String searchTerm);
}
