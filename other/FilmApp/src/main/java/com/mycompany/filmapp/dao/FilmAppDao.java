/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.filmapp.dao;

import com.mycompany.filmapp.model.Film;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
/**
 *
 * @author jhubbbs
 */
public class FilmAppDao implements FilmAppDaoInterface {

    private JdbcTemplate jdbcTemplate;
    
    public void setJdbcTemplate(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    private static final String SQL_INSERT_FILM = "insert into films (title, releaseYear, mpaa, director, note, score, imgUrl) values (?, ?, ?, ?, ?, ?, ?)";
    private static final String SQL_DELETE_FILM = "delete from films where filmId = ?";
    private static final String SQL_UPDATE_FILM = "update films set title = ?, releaseYear = ?, mpaa = ?, director = ?, note = ?, score = ?, imgUrl = ? where filmId = ?";
    private static final String SQL_SELECT_FILM = "select * from films where filmId = ?";
    private static final String SQL_SELECT_ALL_FILMS = "select * from films";
    private static final String SQL_SELECT_FILMS_SEARCH = "select * from films where title like ? "
            + "or releaseYear like ? or director like ? ";


    @Transactional(propagation = Propagation.REQUIRED, readOnly = false)
    @Override
    public void addFilm(Film film) {
        jdbcTemplate.update(SQL_INSERT_FILM,
                film.getTitle(),
                film.getReleaseYear(),
                film.getMpaa(),
                film.getDirector(),
                film.getNote(),
                film.getScore(),
                film.getImgUrl());
        film.setFilmId(jdbcTemplate.queryForObject("select LAST_INSERT_ID()", Integer.class));
    }

    @Override
    public void deleteFilm(int filmId) {
        jdbcTemplate.update(SQL_DELETE_FILM, filmId);
    }

    @Override
    public void updateFilm(Film film) {
        jdbcTemplate.update(SQL_UPDATE_FILM,
                film.getTitle(),
                film.getReleaseYear(),
                film.getMpaa(),
                film.getDirector(),
                film.getNote(),
                film.getScore(),
                film.getImgUrl(),
                film.getFilmId());
    }

    @Override
    public Film getFilmById(int filmId) {
        try {
            return jdbcTemplate.queryForObject(SQL_SELECT_FILM, new FilmMapper(), filmId);
        } catch (EmptyResultDataAccessException ex) {
            return null;
        }
    }
    
    @Override
    public List<Film> getFilmsBySearch(String searchTerm) { 
        try {
            
            List<Film> list = jdbcTemplate.query(SQL_SELECT_FILMS_SEARCH, new FilmMapper(), "%"+searchTerm+"%", "%"+searchTerm+"%", "%"+searchTerm+"%");
            return list;
        } catch (EmptyResultDataAccessException ex) {
            return null;
        }
    }

    @Override
    public List<Film> getAllFilms() {
        return jdbcTemplate.query(SQL_SELECT_ALL_FILMS, new FilmMapper());
    }

    private static final class FilmMapper implements RowMapper<Film> {

        @Override
        public Film mapRow(ResultSet rs, int i) throws SQLException {
            Film film = new Film();
            film.setTitle(rs.getString("title"));
            film.setReleaseYear(rs.getString("releaseYear"));
            film.setMpaa(rs.getString("mpaa"));
            film.setDirector(rs.getString("director"));
            film.setNote(rs.getString("note"));
            film.setScore(rs.getString("score"));
            film.setImgUrl(rs.getString("imgUrl"));
            film.setFilmId(rs.getInt("filmId"));
            return film;
        }
    }

}
