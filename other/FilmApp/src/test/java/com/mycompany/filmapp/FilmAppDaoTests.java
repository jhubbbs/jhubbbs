/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.filmapp;

import com.mycompany.filmapp.dao.FilmAppDaoInterface;
import com.mycompany.filmapp.model.Film;
import java.util.List;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Test;
import static org.junit.Assert.*;
import org.junit.Before;
import org.junit.BeforeClass;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.jdbc.core.JdbcTemplate;

/**
 *
 * @author jhubbbs
 */
public class FilmAppDaoTests {
    
    FilmAppDaoInterface dao;
    
    public FilmAppDaoTests() {
    }


    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
        // Ask Spring for my DAO
        ApplicationContext ctx = new ClassPathXmlApplicationContext("test-applicationContext.xml");
        dao = (FilmAppDaoInterface) ctx.getBean("filmAppDao");

        // Grab a JdbcTemplate to use for cleaning up
        JdbcTemplate cleaner = (JdbcTemplate) ctx.getBean("jdbcTemplate");
        
        cleaner.execute("delete from films");
    }

    @After
    public void tearDown() {
    }

    @Test
    public void addGetUpdateDeleteFilmTest() {

        // Create and Read tests
        Film film = new Film();
        film.setTitle("James");
        film.setReleaseYear("Joyce");
        film.setMpaa("123 Main");
        film.setDirector("Hometown");
        film.setNote("OH");
        film.setScore("12345");
        film.setImgUrl("123456");
        dao.addFilm(film);

        Film fromDb = dao.getFilmById(film.getFilmId());

        assertTrue(areEqual(film, fromDb));

        // Update Test
        film.setTitle("JamesZZ");
        film.setReleaseYear("JoyceZZ");
        film.setMpaa("123 MainZZ");
        film.setDirector("HometownZZ");
        film.setNote("OHZZ");
        film.setScore("12345ZZ");
        film.setImgUrl("123456ZZ");
        
        dao.updateFilm(film);
        fromDb = dao.getFilmById(film.getFilmId());

        assertTrue(areEqual(film, fromDb));

        // Delete Test
        dao.deleteFilm(film.getFilmId());

        fromDb = dao.getFilmById(film.getFilmId());

        assertNull(fromDb);

    }

    @Test
    public void getAllFilmsTest() {
        Film film = new Film();
        
        film.setTitle("James");
        film.setReleaseYear("Joyce");
        film.setMpaa("123 Main");
        film.setDirector("Hometown");
        film.setNote("OH");
        film.setScore("12345");
        film.setImgUrl("123456");
        dao.addFilm(film);

        Film film1 = new Film();
        film1.setTitle("JamesAA");
        film1.setReleaseYear("JoyceAA");
        film1.setMpaa("123 MainAA");
        film1.setDirector("HometownAA");
        film1.setNote("OHAA");
        film1.setScore("12345AA");
        film1.setImgUrl("123456AA");
        dao.addFilm(film1);

        Film film2 = new Film();
        film2.setTitle("JamesBB");
        film2.setReleaseYear("JoyceBB");
        film2.setMpaa("123 MainBB");
        film2.setDirector("HometownBB");
        film2.setNote("OHBB");
        film2.setScore("12345BB");
        film2.setImgUrl("123456BB");
        dao.addFilm(film2);

        List<Film> list = dao.getAllFilms();

        assertEquals(3, list.size());

        Film fromDb = dao.getFilmById(film.getFilmId());
        Film fromDb1 = dao.getFilmById(film1.getFilmId());
        Film fromDb2 = dao.getFilmById(film2.getFilmId());

        assertTrue(areEqual(film, fromDb));
        assertTrue(areEqual(film1, fromDb1));
        assertTrue(areEqual(film2, fromDb2));
    }

    private boolean areEqual(Film film1, Film film2) {
        return film1.getFilmId() == film2.getFilmId()
                && film1.getTitle().equals(film2.getTitle())
                && film1.getReleaseYear().equals(film2.getReleaseYear())
                && film1.getMpaa().equals(film2.getMpaa())
                && film1.getDirector().equals(film2.getDirector())
                && film1.getNote().equals(film2.getNote())
                && film1.getScore().equals(film2.getScore())
                && film1.getImgUrl().equals(film2.getImgUrl());
    }

    
}
