/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pkg167;

/**
 *
 * @author apprentice
 */
public class Student {
    private String name;
    private double grade;
    private double average;
    
    
    public String getName(){
        return this.name;
    }
    
    public void setName(String name){
        this.name = name;
    }
    
    public double getGrade(){
        return this.grade;
    }
    public void setGrade(double grade){
        this.grade = grade;
    }
    public double getAverage(){
        return this.average;
    }
    public void setAverage(double average){
        this.average = average;
    }
}
