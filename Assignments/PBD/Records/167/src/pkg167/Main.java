/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pkg167;

import java.util.Scanner;

/**
 *
 * @author apprentice
 */
public class Main {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        Scanner sc = new Scanner(System.in);
        
        
        Student[] s1 = new Student[3];
        for (int i = 0; i < s1.length; i++) {
            s1[i] = new Student();
        }
        
        
        
        System.out.println("Enter the first student's name: ");
        s1[0].setName(sc.nextLine());
        System.out.println("Enter the first student's grade: ");
        s1[0].setGrade(sc.nextDouble());
        sc.nextLine();
        System.out.println("Enter the first student's average: ");
        s1[0].setAverage(sc.nextDouble());
        sc.nextLine();
        
        System.out.println("Enter the second student's name: ");
        s1[1].setName(sc.nextLine());
        System.out.println("Enter the second student's grade: ");
        s1[1].setGrade(sc.nextDouble());
        sc.nextLine();
        System.out.println("Enter the second student's average: ");
        s1[1].setAverage(sc.nextDouble());
        sc.nextLine();
        
        System.out.println("Enter the third student's name: ");
        s1[2].setName(sc.nextLine());
        System.out.println("Enter the third student's grade: ");
        s1[2].setGrade(sc.nextDouble());
        sc.nextLine();
        System.out.println("Enter the third student's average: ");
        s1[2].setAverage(sc.nextDouble());
        sc.nextLine();
        
        
        System.out.println(s1[0].getName());
        System.out.println(s1[0].getGrade());
        System.out.println(s1[0].getAverage());
        System.out.println(s1[1].getName());
        System.out.println(s1[1].getGrade());
        System.out.println(s1[1].getAverage());
        System.out.println(s1[2].getName());
        System.out.println(s1[2].getGrade());
        System.out.println(s1[2].getAverage());
    }
    
    
    
}
