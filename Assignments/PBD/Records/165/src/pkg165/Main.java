/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pkg165;

/**
 *
 * @author apprentice
 */
import java.net.URL;
import java.util.Scanner;

public class Main {

    static class Address {

        String street;
        String city;
        String state;
        int zip;

       public String toString() { //needs to be toString, otherwise memory location displayed
            return (this.street + ", " + this.city + "  " +this.state+ "  "+ this.zip);
        }
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) throws Exception {
        // TODO code application logic here
        URL addys = new URL("https://cs.leanderisd.org/txt/fake-addresses.txt");

        Address[] addybook = new Address[5];

        Scanner fin = new Scanner(addys.openStream());

        for (int i = 0; i < 5; i++) {
            addybook[i] = new Address();
            addybook[i].street = fin.nextLine();
            addybook[i].city = fin.nextLine();
            addybook[i].state = fin.next();
            addybook[i].zip = fin.nextInt();
            fin.skip("\n");
        }
        fin.close();
        
        
        
        for (int i = 0; i < 5; i++) {
            System.out.println((i + 1) + ". " + addybook[i]);
        }
    }

}
