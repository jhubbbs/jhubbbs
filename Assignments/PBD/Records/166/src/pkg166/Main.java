/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pkg166;

import java.util.Scanner;

/**
 *
 * @author apprentice
 */
public class Main {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        Scanner sc = new Scanner(System.in);
        
        
        Student s1 = new Student();
        
        System.out.println("Enter the first student's name: ");
        s1.setName(sc.nextLine());
        System.out.println("Enter the first student's grade: ");
        s1.setGrade(sc.nextDouble());
        sc.nextLine();
        System.out.println("Enter the first student's average: ");
        s1.setAverage(sc.nextDouble());
        sc.nextLine();
        
        System.out.println(s1.getName());
        System.out.println(s1.getGrade());
        System.out.println(s1.getAverage());
    }
    
    
    
}
