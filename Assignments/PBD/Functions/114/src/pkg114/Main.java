/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pkg114;

import java.util.Scanner;

/**
 *
 * @author apprentice
 */
public class Main {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        Scanner sc = new Scanner(System.in);

        double a, b;
        String op;

        do {
            System.out.print("> ");
            a = sc.nextDouble();
            op = sc.next();
            b = sc.nextDouble();

            if (op.equals("+")) {
                add(a, b);
            } else if(op.equals("-")){
                subtract(a, b);
            }else if(op.equals("*")){
                multiply(a, b);
            }else if(op.equals("/")){
                divide(a, b);
            }else {
                System.out.println("Undefined operator: '" + op + "'.");
            }
        } while (a != 0);
        System.out.println("Goodbye");
    }
    
    public static void add(double a, double b){
        System.out.println(a+b);
    }
    public static void subtract(double a, double b){
        System.out.println(a-b);
    }
    public static void multiply(double a, double b){
        System.out.println(a*b);
    }
    public static void divide(double a, double b){
        System.out.println(a/b);
    }
}
