/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pkg32;

import java.util.Scanner;

/**
 *
 * @author apprentice
 */
public class Main {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        Scanner sc = new Scanner(System.in);
        System.out.println("Is it an animal, vegetable, or mineral?");
        String type = sc.nextLine();
        
        System.out.println("Is it larger than a breadbox? Enter y/n");
        String bread = sc.nextLine();
        
        if(type.equals("animal")){
            if(bread.equals("y")){
                System.out.println("Moose?");
            }
        }else{
            System.out.println("Squirrel?");
        }
        
        if(type.equals("vegetable")){
            if(bread.equals("y")){
                System.out.println("Watermelon?");
            }else{
                System.out.println("Carrot?");
            }
        }
        
         if(type.equals("mineral")){
            if(bread.equals("y")){
                System.out.println("Camaro?");
            }else{
                System.out.println("Paperclip?");
            }
        }
        
    }
    
}
