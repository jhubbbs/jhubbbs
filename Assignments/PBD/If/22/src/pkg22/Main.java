/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pkg22;

import java.util.Scanner;

/**
 *
 * @author apprentice
 */
public class Main {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        Scanner sc = new Scanner(System.in);
        
        System.out.println("Please enter your age: ");
        int age = sc.nextInt();
        
        if(age < 16){
            System.out.println("You cannot drive");
        }
        if(age < 18){
            System.out.println("You cannot vote");
        }
        if(age < 21){
            System.out.println("You cannot drink");
        }
        if(age < 25){
            System.out.println("You cannot rent a car");
        }
        if(age >= 25){
            System.out.println("You can do anything legal");
        }
        
    }
    
}
