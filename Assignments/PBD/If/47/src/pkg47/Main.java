/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pkg47;

import java.util.Random;
import java.util.Scanner;



/**
 *
 * @author apprentice
 */
public class Main {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        Random r = new Random();
        Scanner sc = new Scanner(System.in);
        
        int correct = 1 + r.nextInt(3);
        System.out.println(correct);
        
        System.out.println("Guess a whole num between 1 and 3");
        int guess = sc.nextInt();
        sc.nextLine();
        
        if(guess == correct){
            System.out.println("Correct!");
        }else{
            System.out.println("Wrong! The correct answer was "+correct);
        }
    }
    
}
