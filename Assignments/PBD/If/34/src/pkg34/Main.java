/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pkg34;

import java.util.Scanner;

/**
 *
 * @author apprentice
 */
public class Main {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        
        Scanner sc = new Scanner(System.in);
        System.out.println("Please enter your age: ");
        int age = sc.nextInt();
        sc.nextLine();
        
        if(age < 16){
            System.out.println("You cannot drive");
        }
        if( age > 16 && age < 18){
            System.out.println("Can drive but not vote");
        }
        if(age > 17 && age < 25){
            System.out.println("Can vote but not rent a car");
        }
        if(age > 24){
            System.out.println("you can do it all!");
        }
        
        
    }
    
}
