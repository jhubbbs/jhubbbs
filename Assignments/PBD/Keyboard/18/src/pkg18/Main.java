/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pkg18;

import java.util.Scanner;

/**
 *
 * @author apprentice
 */
public class Main {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        
        System.out.println("What is your name?");
        String name = sc.nextLine();
        
        System.out.println("Hello "+name);
        System.out.println("How old are you?");
        int age = sc.nextInt();
        
        System.out.println("Did you know that in 5 years you will be "+(age+5)+"?");
        System.out.println("And 5 years ago you were "+ (age-5) +"?");
    }
    
}
