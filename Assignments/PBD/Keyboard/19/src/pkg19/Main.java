/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pkg19;

import java.util.Scanner;

/**
 *
 * @author apprentice
 */
public class Main {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        Scanner sc = new Scanner(System.in);
        
        System.out.println("First num?");
        String string1 = sc.nextLine();
        float num1 = Float.parseFloat(string1);
        
        System.out.println("Second num?");
        String string2 = sc.nextLine();
        float num2 = Float.parseFloat(string2);
        
        System.out.println("Third num?");
        String string3 = sc.nextLine();
        float num3 = Float.parseFloat(string3);
        
        float sum = num1 + num2 + num3;
        float answer = sum / 2;
        System.out.println("The result is: "+answer);
    }
    
}
