/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pkg130;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Scanner;

/**
 *
 * @author apprentice
 */
public class Main {

    private static final String DELIMITER = "~~";

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        Scanner sc = new Scanner(System.in);
        String firstName;
        String lastName;
        String impFirstName;
        String impLastName;
        final String OUT_FILE = "Files/OUT_FILE.txt";
        int count = 0;
        System.out.println("Please enter your first name:\n");
        firstName = sc.nextLine();
        System.out.println("Please enter your last name:\n ");
        lastName = sc.nextLine();

        try {

            PrintWriter outPrinter = new PrintWriter(new FileWriter(OUT_FILE));
            outPrinter.println(firstName + DELIMITER + lastName);
            outPrinter.flush(); 
            outPrinter.close();
           
        } catch (FileNotFoundException fnf) {
            System.out.println("We cannot find the file: " + OUT_FILE);
        } catch (IOException io) {
            System.out.println("Problem writing our sample file");
            System.out.println(io.getMessage());
        }

        System.out.println(
                "\nFile has been written, we will now read it in");

        try {
            Scanner sc2 = new Scanner(new BufferedReader(new FileReader(OUT_FILE)));

            String stuffToReadIn = sc2.nextLine();

            String[] firstLast = stuffToReadIn.split(DELIMITER);
            impFirstName = firstLast[0];
            impLastName = firstLast[1];
            System.out.println("\nName is: " +impLastName+ ", " + impFirstName);

        } catch (FileNotFoundException fnf) {
            System.out.println("\nPROBLEM READING FILE: " + OUT_FILE);
        }
    }
}
