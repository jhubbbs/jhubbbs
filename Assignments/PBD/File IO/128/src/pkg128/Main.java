/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pkg128;

import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;

/**
 *
 * @author apprentice
 */
public class Main {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
    
    final String LETTER = "letter.txt";
    
    try {

            PrintWriter outPrinter = new PrintWriter(new FileWriter(LETTER));
            outPrinter.println("this is a letter\n"
                    + "oh it is grand\n"
                    + "amazing");
            outPrinter.flush(); 
            outPrinter.close();
           
        } catch (FileNotFoundException fnf) {
            System.out.println("We cannot find the file: " + LETTER);
        } catch (IOException io) {
            System.out.println("Problem writing our sample file");
            System.out.println(io.getMessage());
        }
    
    }
}
