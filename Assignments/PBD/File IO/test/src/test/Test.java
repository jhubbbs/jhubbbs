/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package test;

import java.util.Scanner;

/**
 *
 * @author apprentice
 */
public class Test {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        Scanner sc = new Scanner(System.in);
        boolean success = false;
        String stringInput;
        int input;
        do {
            System.out.println("enter int");
            stringInput = sc.nextLine();
            if (stringInput.isEmpty()) {
                success=true;
                System.out.println("blank int");
            }else{
                
                input = Integer.parseInt(stringInput);

                if (input >= 0 && input <= 1000) {
                    success = true;
                    System.out.println("int: " + input);
                } else {
                    System.out.println("That is not within range.");
                }
            }
        }while (success == false);
          
    }

}
