/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pkg129;

import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Scanner;

/**
 *
 * @author apprentice
 */
public class Main {

    /**
     * @param args the command line arguments
     */
    private static final String DELIMITER = "~~";
    
    public static void main(String[] args) {
        // TODO code application logic here
        
        Scanner sc = new Scanner(System.in);
        String name;
        int num;
        final String SCORES = "scores.txt";
        
        System.out.println("Please enter your name:\n");
        name = sc.nextLine();
        System.out.println("Enter number");
        num = sc.nextInt();
        sc.nextLine();

        try {

            PrintWriter outPrinter = new PrintWriter(new FileWriter(SCORES));
            outPrinter.println(name + DELIMITER + num);
            outPrinter.flush(); 
            outPrinter.close();
           
        } catch (FileNotFoundException fnf) {
            System.out.println("We cannot find the file: " + SCORES);
        } catch (IOException io) {
            System.out.println("Problem writing our sample file");
            System.out.println(io.getMessage());
        }

        System.out.println(
                "\nFile has been written");
        
        
    }
    
}
