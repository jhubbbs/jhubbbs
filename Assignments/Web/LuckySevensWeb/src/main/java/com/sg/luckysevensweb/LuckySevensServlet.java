/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sg.luckysevensweb;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Random;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author apprentice
 */
@WebServlet(name = "LuckySevensServlet", urlPatterns = {"/LuckySevensServlet"})
public class LuckySevensServlet extends HttpServlet {




    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        
        RequestDispatcher rd = request.getRequestDispatcher("index.jsp");
        rd.forward(request, response);
    }

 
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        
        
        
        int bid = Integer.parseInt(request.getParameter("bid"));
        
        int startingBalance = bid;
        int rolls = 0;
        int maxBalance = bid;
        int maxBalanceRoll = 1;
        
        Random rGen = new Random();
        int d1;
        int d2;
        
        do{
            rolls++;
            
            d1 = rGen.nextInt(5)+1;
            d2 = rGen.nextInt(5)+1;
            
            if(bid > maxBalance){
                maxBalance = bid;
                maxBalanceRoll = rolls;
            }
            
            if(d1+d2==7){
                bid = bid + 4;
            } else{
                bid--;
            }
            
            
            
        } while(bid > 0);
        
        
        ArrayList resultsList = new ArrayList<>();

        resultsList.add(startingBalance);
        resultsList.add(rolls);
        resultsList.add(maxBalance);
        resultsList.add(maxBalanceRoll);
        
        
        request.setAttribute("resultsList", resultsList);
        RequestDispatcher rd = request.getRequestDispatcher("gameResults.jsp");
        rd.forward(request, response);
    }

    
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
