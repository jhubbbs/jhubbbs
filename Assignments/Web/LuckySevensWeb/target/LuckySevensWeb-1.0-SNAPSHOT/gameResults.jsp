<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Results</title>
    </head>
    <body>
        <h1>RESULTS</h1>
        
         <fmt:setLocale value="en_US"/>

        Starting balance: <fmt:formatNumber value="${resultsList.get(0)}" type="currency"/><br>
        Number of Rolls: <c:out value="${resultsList.get(1)}"/><br>
        Max Balance: <fmt:formatNumber value="${resultsList.get(2)}" type="currency"/><br>
        Max Balance At Roll: <c:out value="${resultsList.get(3)}"/><br>
        <br>


        <form action="LuckySevensServlet" method="GET">

            <input type="submit" value="Play Again!">
        </form>



    </body>
</html>
