CREATE DATABASE IF NOT EXISTS `addressbook`;

USE `addressbook`;

CREATE TABLE IF NOT EXISTS `addresses` (
 `addressId` int(20) NOT NULL AUTO_INCREMENT,
 `firstName` varchar(20) NOT NULL,
 `lastName` varchar(20) NOT NULL,
 `street` varchar(30) NOT NULL,
 `city` varchar(20) NOT NULL,
 `state` varchar(20) NOT NULL,
 `zip` varchar(8) NOT NULL,
 PRIMARY KEY (`addressId`)
);

