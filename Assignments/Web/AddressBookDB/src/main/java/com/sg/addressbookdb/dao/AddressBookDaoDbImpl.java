/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sg.addressbookdb.dao;

import com.sg.addressbookdb.model.Address;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author apprentice
 */
public class AddressBookDaoDbImpl implements AddressBookDao {
   
    private JdbcTemplate jdbcTemplate;
    
    public void setJdbcTemplate(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }
    
    private static final String SQL_INSERT_ADDRESS = "insert into addresses (firstName, lastName, street, city, state, zip) values (?, ?, ?, ?, ?, ?)";
    private static final String SQL_DELETE_ADDRESS = "delete from addresses where addressId = ?";
    private static final String SQL_UPDATE_ADDRESS = "update addresses set firstName = ?, lastName = ?, street = ?, city = ?, state = ?, zip = ? where addressId = ?";
    private static final String SQL_SELECT_ADDRESS = "select * from addresses where addressId = ?";
    private static final String SQL_SELECT_ALL_ADDRESSES = "select * from addresses";
    private static final String SQL_SELECT_ADDRESSES_SEARCH = "select * from addresses where firstName like ? "
            + "or lastName like ? or street like ? or city like ? "
            + "or state like ? or zip like ?";

    
    @Transactional(propagation = Propagation.REQUIRED, readOnly = false)
    @Override
    public void addAddress(Address address) {
        jdbcTemplate.update(SQL_INSERT_ADDRESS,
                address.getFirstName(),
                address.getLastName(),
                address.getStreet(),
                address.getCity(),
                address.getState(),
                address.getZip());
        address.setAddressId(jdbcTemplate.queryForObject("select LAST_INSERT_ID()", Integer.class));
    }

    @Override
    public void deleteAddress(int addressId) {
        jdbcTemplate.update(SQL_DELETE_ADDRESS, addressId);
    }

    @Override
    public void updateAddress(Address address) {
        jdbcTemplate.update(SQL_UPDATE_ADDRESS,
                address.getFirstName(),
                address.getLastName(),
                address.getStreet(),
                address.getCity(),
                address.getState(),
                address.getZip(),
                address.getAddressId());
    }

    @Override
    public Address getAddressById(int addressId) {
        try {
            return jdbcTemplate.queryForObject(SQL_SELECT_ADDRESS, new AddressMapper(), addressId);
        } catch (EmptyResultDataAccessException ex) {
            return null;
        }
    }
    
    @Override
    public List<Address> getAddressesBySearch(String searchTerm) { 
        try {
            
            List<Address> list = jdbcTemplate.query(SQL_SELECT_ADDRESSES_SEARCH, new AddressMapper(), "%"+searchTerm+"%", "%"+searchTerm+"%", "%"+searchTerm+"%", "%"+searchTerm+"%", "%"+searchTerm+"%", "%"+searchTerm+"%");
            return list;
        } catch (EmptyResultDataAccessException ex) {
            return null;
        }
    }

    @Override
    public List<Address> getAllAddresses() {
        return jdbcTemplate.query(SQL_SELECT_ALL_ADDRESSES, new AddressMapper());
    }

    private static final class AddressMapper implements RowMapper<Address> {

        @Override
        public Address mapRow(ResultSet rs, int i) throws SQLException {
            Address address = new Address();
            address.setFirstName(rs.getString("firstName"));
            address.setLastName(rs.getString("lastName"));
            address.setStreet(rs.getString("street"));
            address.setCity(rs.getString("city"));
            address.setState(rs.getString("state"));
            address.setZip(rs.getString("zip"));
            address.setAddressId(rs.getInt("addressId"));
            return address;
        }
    }
}
