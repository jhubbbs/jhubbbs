/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sg.addressbookdb;

import com.sg.addressbookdb.dao.AddressBookDao;
import com.sg.addressbookdb.model.Address;
import java.util.List;
import org.junit.After;
import org.junit.AfterClass;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.jdbc.core.JdbcTemplate;

/**
 *
 * @author apprentice
 */
public class AddressBookDaoTests {
    
    AddressBookDao dao;
    
    public AddressBookDaoTests() {
    }


    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
        
        ApplicationContext ctx = new ClassPathXmlApplicationContext("test-applicationContext.xml");
        dao = (AddressBookDao) ctx.getBean("addressBookDaoDbImpl");

        JdbcTemplate cleaner = (JdbcTemplate) ctx.getBean("jdbcTemplate");
        
        cleaner.execute("delete from addresses");
    }

    @After
    public void tearDown() {
    }

    @Test
    public void addGetUpdateDeleteAddressTest() {

        Address address = new Address();
        address.setFirstName("James");
        address.setLastName("Joyce");
        address.setStreet("123 Main");
        address.setCity("Hometown");
        address.setState("OH");
        address.setZip("12345");

        dao.addAddress(address);

        Address fromDb = dao.getAddressById(address.getAddressId());

        assertTrue(areEqual(address, fromDb));

        address.setFirstName("JamesZZ");
        address.setLastName("JoyceZZ");
        address.setStreet("123 MainZZ");
        address.setCity("HometownZZ");
        address.setState("OHZZ");
        address.setZip("12345ZZ");

        dao.updateAddress(address);
        fromDb = dao.getAddressById(address.getAddressId());

        assertTrue(areEqual(address, fromDb));

        dao.deleteAddress(address.getAddressId());

        fromDb = dao.getAddressById(address.getAddressId());

        assertNull(fromDb);

    }

    @Test
    public void getAllAddressesTest() {
        
        Address address = new Address();
        address.setFirstName("James");
        address.setLastName("Joyce");
        address.setStreet("123 Main");
        address.setCity("Hometown");
        address.setState("OH");
        address.setZip("12345");
        dao.addAddress(address);

        Address address1 = new Address();
        address1.setFirstName("JamesAA");
        address1.setLastName("JoyceAA");
        address1.setStreet("123 MainAA");
        address1.setCity("HometownAA");
        address1.setState("OHAA");
        address1.setZip("12345AA");
        dao.addAddress(address1);

        Address address2 = new Address();
        address2.setFirstName("JamesBB");
        address2.setLastName("JoyceBB");
        address2.setStreet("123 MainBB");
        address2.setCity("HometownBB");
        address2.setState("OHBB");
        address2.setZip("12345BB");
        dao.addAddress(address2);

        List<Address> list = dao.getAllAddresses();

        assertEquals(3, list.size());

        Address fromDb = dao.getAddressById(address.getAddressId());
        Address fromDb1 = dao.getAddressById(address1.getAddressId());
        Address fromDb2 = dao.getAddressById(address2.getAddressId());

        assertTrue(areEqual(address, fromDb));
        assertTrue(areEqual(address1, fromDb1));
        assertTrue(areEqual(address2, fromDb2));
    }

    private boolean areEqual(Address address1, Address address2) {
        return address1.getAddressId() == address2.getAddressId()
                && address1.getFirstName().equals(address2.getFirstName())
                && address1.getLastName().equals(address2.getLastName())
                && address1.getStreet().equals(address2.getStreet())
                && address1.getCity().equals(address2.getCity())
                && address1.getState().equals(address2.getState())
                && address1.getZip().equals(address2.getZip());
    }

    
}
