/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

// When the document is ready, populate our summary table
$(document).ready(function () {
    loadAddresses();

    // add the onclick handling for our add button
    $('#add-button').click(function (event) {
        event.preventDefault();
        // need to submit this via AJAX
        $.ajax({
            type: 'POST',
            url: 'address',
            // make the JSON address
            data: JSON.stringify({
                firstName: $('#add-first-name').val(),
                lastName: $('#add-last-name').val(),
                street: $('#add-street').val(),
                city: $('#add-city').val(),
                state: $('#add-state').val(),
                zip: $('#add-zip').val()

            }),
            contentType: 'application/json; charset=utf-8',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },
            dataType: 'json'
        }).success(function (data, status) {
            // clear the form and reload the summary table
            $('#add-first-name').val('');
            $('#add-last-name').val('');
            $('#add-street').val('');
            $('#add-city').val('');
            $('#add-state').val('');
            $('#add-zip').val('');

            // reload the summary table
            $('#validationErrors').empty();
            loadAddresses();
        }).error(function (data, status) {
            $('#validationErrors').empty();
            $.each(data.responseJSON.fieldErrors, function (index, validationError) {
                var errorDiv = $('#validationErrors');
                errorDiv.append(validationError.message).append($('<br>'));
            });
        });
    });

    $('#edit-button').click(function (event) {
        event.preventDefault();
        // update our address via AJAX
        $.ajax({
            type: 'PUT',
            url: 'address/' + $('#edit-address-id').val(),
            data: JSON.stringify({
                addressId: $('#edit-address-id').val(),
                firstName: $('#edit-first-name').val(),
                lastName: $('#edit-last-name').val(),
                street: $('#edit-street').val(),
                city: $('#edit-city').val(),
                state: $('#edit-state').val(),
                zip: $('#edit-zip').val()

            }),
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },
            'dataType': 'json'
        }).success(function () {
            loadAddresses();
        });
    });

    $("#search-button").click(function (event) {
        event.preventDefault();

        var searchTerm = $('#search-term').val();
        $.ajax({
            type: 'GET',
            url: 'search/' + searchTerm

        }).success(function (searchResults, status) {
            $('#search-term').val('');

            fillAddressTable(searchResults, status);
        });
    });
});

function loadAddresses() {
    //  Get our JSON object from the endpoint
    $.ajax({
        url: 'addresses'
    }).success(function (data, status) {
        fillAddressTable(data, status);
    });
}

// used with Search button and loadAddress function
function fillAddressTable(addressBook, status) {
    // clear the previous list
    clearAddressTable();

    // store our tbody in a variable 
    var summaryTable = $('#contentRows');

    $.each(addressBook, function (arrayPosition, person) {
        summaryTable.append($('<tr>')
                .append($('<td>')
                        .append($('<a>')
                                .attr({
                                    'data-address-id': person.addressId,
                                    'data-toggle': 'modal',
                                    'data-target': '#detailsModal'
                                })
                                .text(person.firstName + ' ' + person.lastName)))
                .append($('<td>').text(person.city))
                .append($('<td>')
                        .append($('<a>')
                                .attr({
                                    'data-address-id': person.addressId,
                                    'data-toggle': 'modal',
                                    'data-target': '#editModal'
                                })
                                .text('Edit')))
                .append($('<td>')
                        .append($('<a>')
                                .attr({
                                    'onClick': 'deleteAddress(' + person.addressId + ')'
                                })
                                .text('Delete'))));
    });
}

// Clear all the rows from our summary table
function clearAddressTable() {
    $('#contentRows').empty();
}

// Run in response to the show.bs.modal event
// It gets the address data and renders it to the dialog
$('#detailsModal').on('show.bs.modal', function (event) {
    var element = $(event.relatedTarget);
    var addressId = element.data('address-id');
    var modal = $(this);
    // Get our object via AJAX
    $.ajax({
        type: 'GET',
        url: 'address/' + addressId
    }).success(function (sampleAddress) {
        modal.find('#address-id').text(sampleAddress.addressId);
        modal.find('#address-firstName').text(sampleAddress.firstName);
        modal.find('#address-lastName').text(sampleAddress.lastName);
        modal.find('#address-street').text(sampleAddress.street);
        modal.find('#address-city').text(sampleAddress.city);
        modal.find('#address-state').text(sampleAddress.state);
        modal.find('#address-zip').text(sampleAddress.zip);

    });
});

$('#editModal').on('show.bs.modal', function (event) {
    var element = $(event.relatedTarget);
    var addressId = element.data('address-id');
    var modal = $(this);

    // get our object via AJAX
    $.ajax({
        type: 'GET',
        url: 'address/' + addressId
    }).success(function (sampleEditAddress) {
        modal.find('#edit-address-id').val(sampleEditAddress.addressId);
        modal.find('#edit-first-name').val(sampleEditAddress.firstName);
        modal.find('#edit-last-name').val(sampleEditAddress.lastName);
        modal.find('#edit-street').val(sampleEditAddress.street);
        modal.find('#edit-city').val(sampleEditAddress.city);
        modal.find('#edit-state').val(sampleEditAddress.state);
        modal.find('#edit-zip').val(sampleEditAddress.zip);

    });
});

function deleteAddress(id) {
    var answer = confirm('Do you really want to delete this address?');

    if (answer === true) {
        $.ajax({
            type: 'DELETE',
            url: 'address/' + id
        }).success(function () {
            // reload summary
            loadAddresses();
        });
    }
}        