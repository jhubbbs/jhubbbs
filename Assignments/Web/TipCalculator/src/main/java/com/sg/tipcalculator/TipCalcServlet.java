/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sg.tipcalculator;

import java.io.IOException;
import java.util.ArrayList;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author apprentice
 */
@WebServlet(name = "TipCalcServlet", urlPatterns = {"/TipCalcServlet"})
public class TipCalcServlet extends HttpServlet {

    

   
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        
        RequestDispatcher rd = request.getRequestDispatcher("index.jsp");
        rd.forward(request, response);
    }

   
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        
        double amount = Double.parseDouble(request.getParameter("amount"));
        double tipRate = Double.parseDouble(request.getParameter("tipRate"));
        
        double tipAmount = amount * (tipRate / 100);
        
        double total = tipAmount + amount;
        
        ArrayList resultsList = new ArrayList();
        
        resultsList.add(amount);
        resultsList.add(tipRate);
        resultsList.add(tipAmount);
        resultsList.add(total);
        
        
        request.setAttribute("resultsList", resultsList);
        RequestDispatcher rd = request.getRequestDispatcher("tipCalcResults.jsp");
        rd.forward(request, response);
    }

    
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
