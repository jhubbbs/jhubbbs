<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Tip Calculator Results</title>
    </head>
    <body>
        <h1>Results</h1>
        
        
        <fmt:setLocale value="en_US"/>
        
        
        
        
        Amount:
        <fmt:formatNumber value="${resultsList.get(0)}" type="currency"/><br>
            
        Tip %:
            <c:out value="${resultsList.get(1)}"/>
            <br>
        Tip:
            <fmt:formatNumber value="${resultsList.get(2)}" type="currency"/>
            <br>
            <hr>
        
        Total:
            <fmt:formatNumber value="${resultsList.get(3)}" type="currency"/>
            <br>
        
        
        
        <br>

        <form action="TipCalcServlet" method="GET">
            <input type="submit" value="Again!">
        </form>
    </body>
</html>
