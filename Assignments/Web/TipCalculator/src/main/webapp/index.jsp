<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Tip Calculator</title>
    </head>
    <body>
        <h1>Tip Calculator</h1>
        
        <form action="TipCalcServlet" method="POST">

            Amount: <input type="number" name="amount" min=".01" step=".01" required><br><br>
            Tip %: <input type="number" name="tipRate" min="0" step=".01" required><br><br>
       
            <input type="submit" value="Calculate!">

        </form>
        
    </body>
</html>
