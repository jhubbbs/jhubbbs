<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Flooring Calculator</title>
    </head>
    <body>
        <h1>Flooring Calculator</h1>
        
        
        <form action="FlooringCalcServlet" method="POST">

            Width: <input type="number" name="width" min=".01" step=".01" required><br><br>
            Length: <input type="number" name="length" min=".01" step=".01" required><br><br>
            Mat Cost/SqFt: <input type ="number" name="matRate" min=".01" step=".01" required><br><br>

            <input type="submit" value="Calculate!">


        </form>
    </body>
</html>
