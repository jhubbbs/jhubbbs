/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sg.flooringcalculator;

import java.io.IOException;
import java.util.ArrayList;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author apprentice
 */
@WebServlet(name = "FlooringCalcServlet", urlPatterns = {"/FlooringCalcServlet"})
public class FlooringCalcServlet extends HttpServlet {

    

   
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
       
        RequestDispatcher rd = request.getRequestDispatcher("index.jsp");
        rd.forward(request, response);
    }

    
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        
        double width = Double.parseDouble(request.getParameter("width"));
        double length = Double.parseDouble(request.getParameter("length"));
        double matRate = Double.parseDouble(request.getParameter("matRate"));
        double area = width * length;
        
        double matCost = matRate * area;
        double labCost = 0;
        
        double testArea = area;
        while(testArea > 0){
            
            testArea = testArea - 5;
            labCost = labCost + 21.5;
            //21.5 per 5sqft laborRate. 5sqft intervals only
        
        }
        
        
        ArrayList resultsList = new ArrayList();
        
        resultsList.add(area);
        resultsList.add(matCost);
        resultsList.add(labCost);
        resultsList.add(matCost + labCost);
        
        request.setAttribute("resultsList", resultsList);
        RequestDispatcher rd = request.getRequestDispatcher("flooringCalcResults.jsp");
        rd.forward(request, response);
        
        
    }

    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
