/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sg.unitconverter;

import java.io.IOException;
import java.util.ArrayList;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author apprentice
 */
@WebServlet(name = "UnitConverterServlet", urlPatterns = {"/UnitConverterServlet"})
public class UnitConverterServlet extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        RequestDispatcher rd = request.getRequestDispatcher("index.jsp");
        rd.forward(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        String conType = request.getParameter("conType");
        String unitType = request.getParameter("unitType");
        String unitType2 = request.getParameter("unitType2");
        double amount = Double.parseDouble(request.getParameter("amount"));
        double conAmount = 0;

        if (!unitType.equals(unitType2)) {

            if (conType.equals("Temperature")) {

                switch (unitType) {
                    case "Fahrenheit":
                        if (unitType2.equals("Celsius")) {
                            conAmount = (amount - 32) * 0.5555555;

                        } else if (unitType2.equals("Kelvin")) {
                            conAmount = (amount + 459.67) * .555555;
                        }
                        break;
                    case "Celsius":
                        if (unitType2.equals("Fahrenheit")) {
                            conAmount = (amount * 1.8) + 32;

                        } else if (unitType2.equals("Kelvin")) {
                            conAmount = amount + 273.15;
                        }
                        break;
                    case "Kelvin":
                        if (unitType2.equals("Fahrenheit")) {
                            conAmount = (amount * 1.8) - 459.67;

                        } else if (unitType2.equals("Celsius")) {
                            conAmount = amount - 273.15;
                        }
                        break;
                    default:
                        break;
                }

            } else if (conType.equals("Currency")) {
                switch (unitType) {
                    case "USD":
                        if (unitType2.equals("CAD")) {
                            conAmount = amount * 1.33;

                        } else if (unitType2.equals("JPY")) {
                            conAmount = amount * 104.23;

                        }
                        break;
                    case "CAD":
                        if (unitType2.equals("USD")) {
                            conAmount = amount * .75;

                        } else if (unitType2.equals("JPY")) {
                            conAmount = amount * 78.06;

                        }
                        break;
                    case "JPY":
                        if (unitType2.equals("USD")) {
                            conAmount = amount * .0096;

                        } else if (unitType2.equals("CAD")) {
                            conAmount = amount * 0.013;

                        }
                        break;
                    default:
                        break;
                }
            }
        } else {
            conAmount = amount;
        }

        ArrayList resultsList = new ArrayList();

        resultsList.add(conType);
        resultsList.add(unitType);
        resultsList.add(unitType2);
        resultsList.add(amount);
        resultsList.add(conAmount);

        request.setAttribute("resultsList", resultsList);
        RequestDispatcher rd = request.getRequestDispatcher("converterResults.jsp");
        rd.forward(request, response);
    }

    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
