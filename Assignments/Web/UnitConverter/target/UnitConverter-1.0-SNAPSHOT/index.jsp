<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<script type="text/javascript" src="<c:url value='/js/jquery-1.10.2.js' />"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Unit Converter</title>
    </head>
    <body>
        <h1>Unit Converter</h1>


        <script>

            $(function () {
                $('#qmt-unitType, #qmt-unitType2, #label-unitType, #label-unitType2')
                        .hide();
            });
        </script>
        <script>
            $(function () {
                $("#qmt-conType").on("change", function () {
                    $(function () {
                        $('#qmt-unitType, #qmt-unitType2, #label-unitType, #label-unitType2')
                                .show();
                    });

                    var levelClass = $('#qmt-conType').find('option:selected').attr('class');
                    console.log(levelClass);
                    $('#qmt-unitType option').each(function () {
                        var self = $(this);
                        if (self.hasClass(levelClass) || typeof (levelClass) == "undefined") {
                            self.show();
                        } else {
                            self.hide();
                        }
                    });
                    $('#qmt-unitType2 option').each(function () {
                        var self = $(this);
                        if (self.hasClass(levelClass) || typeof (levelClass) == "undefined") {
                            self.show();
                        } else {
                            self.hide();
                        }
                    });
                });
            });
        </script>




        <form action="UnitConverterServlet" method="POST">

            <label for="qmt-conType">Conversion Type:</label>
            <select id="qmt-conType" name="conType">
                <option></option>
                <option class="temperature" value="Temperature">Temperatures</option>
                <option class="currency" value="Currency">Currencies</option>
            </select>
            <br>
            <br>

            <label for="qmt-unitType" id="label-unitType">From Unit:</label>
            <select id="qmt-unitType" name="unitType">
                <option></option>
                <option class="temperature" value="Fahrenheit">&nbsp;&nbsp;&nbsp;Fahrenheit</option>
                <option class="temperature" value="Celsius">&nbsp;&nbsp;&nbsp;Celsius</option>
                <option class="temperature" value="Kelvin">&nbsp;&nbsp;&nbsp;Kelvin</option>
                <option class="currency" value="USD">&nbsp;&nbsp;&nbsp;USD</option>
                <option class="currency" value="CAD">&nbsp;&nbsp;&nbsp;CAD</option>
                <option class="currency" value="JPY">&nbsp;&nbsp;&nbsp;JPY</option>
            </select>
            

            <label for="qmt-unitType2" id="label-unitType2">To Unit:</label>
            <select id="qmt-unitType2" name="unitType2">
                <option></option>
                <option class="temperature" value="Fahrenheit">&nbsp;&nbsp;&nbsp;Fahrenheit</option>
                <option class="temperature" value="Celsius">&nbsp;&nbsp;&nbsp;Celsius</option>
                <option class="temperature" value="Kelvin">&nbsp;&nbsp;&nbsp;Kelvin</option>
                <option class="currency" value="USD">&nbsp;&nbsp;&nbsp;USD</option>
                <option class="currency" value="CAD">&nbsp;&nbsp;&nbsp;CAD</option>
                <option class="currency" value="JPY">&nbsp;&nbsp;&nbsp;JPY</option>
            </select>

            <br>
            <br>
            Amount: <input type="number" name="amount" required><br><br>


            <br>
            <br>
            <input type="submit" value="Convert!">

        </form>

    </body>
</html>
