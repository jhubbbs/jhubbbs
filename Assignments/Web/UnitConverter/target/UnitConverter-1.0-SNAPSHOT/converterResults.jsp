<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Unit Converter Results</title>
    </head>
    <body>
        <h1>Results</h1>
        
        You converted:
        <c:out value="${resultsList.get(0)}"/><br>
        
        From:
        <c:out value="${resultsList.get(3)}"/>
        
        <c:out value="${resultsList.get(1)}"/><br>
        
        
        
        To:
        
        
        <c:out value="${resultsList.get(4)}"/>
        
        <c:out value="${resultsList.get(2)}"/><br>
        
        
        
        <form action="UnitConverterServlet" method="GET">
            <input type="submit" value="Again!">
        </form>
    </body>
</html>
