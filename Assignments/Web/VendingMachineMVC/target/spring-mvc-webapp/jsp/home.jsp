<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="s" uri="http://www.springframework.org/tags" %>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Vending Machine</title>
        <!-- add Bootstrap -->
        <link href="${pageContext.request.contextPath}/css/bootstrap.min.css"
              rel="stylesheet">



    </head>
    <body>
        <div class="container">
            <h1>Vending Machine</h1>
            <hr />
            <div class="row">
                <div class="col-md-6 col-lg-6 col-xs-6">
                    <!-- Our list of contacts -->
                    <h2>Items</h2>
                    <table id="itemTable" class="table table-hover table-striped">
                        <tr>
                            <th width="30%">Name</th>
                            <th width="20%">Cost</th>
                            <th width="20%">Quantity</th>
                            <th width="15%"></th>
                            <th width="15%"></th>
                        </tr>
                        <!-- This is where our contacts will be.  We will populate this with JS -->
                        <tbody id="contentRows"></tbody>
                    </table>
                </div>

                <div class="col-md-6 col-lg-6 col-xs-6">
                    <!-- Add money form -->
                    <h2>Add Money</h2>

                    <form class="form-horizontal" role="form">
                        <div class="form-group">
                            <label for="add-money" 
                                   class="col-md-4 control-label">Amount:</label>
                            <div class="col-md-8">
                                <input type="number"  class="form-control" id="add-money" placeholder="$0.00" min=".01" step=".01" required/>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-md-offset-4 col-md-8">
                                <button type="submit"
                                        id="add-money-button"
                                        class="btn btn-default">
                                    Add $
                                </button>
                            </div>
                        </div>
                    </form>

                    <table id="moneyTable" class="table table-hover table-striped">
                        <tr>
                            <th width="100%">BALANCE</th>
                        </tr>
                        <tr>
                            <th id="moneyRows"></th>
                        </tr>
                    </table>

                    <table id="changeTable" class="table table-hover table-striped">
                        <tr>
                            <th width="100%">CHANGE</th>
                        </tr>

                        <tbody id="changeRows"></tbody>

                    </table>
                </div>


            </div>



            <!-- Placing our JS at the end of the document so that the page loads faster-->
            <script src="${pageContext.request.contextPath}/js/jquery-2.2.4.min.js"></script>
            <script src="${pageContext.request.contextPath}/js/bootstrap.min.js"></script>
            <script src="${pageContext.request.contextPath}/js/itemList.js"></script>
    </body>
</html>
