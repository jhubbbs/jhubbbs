/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


$(document).ready(function () {
    loadItems();
    loadMoney();
    loadChange();


    $('#add-money-button').click(function (event) {
        event.preventDefault();

        var amount = $('#add-money').val();
        amount = (amount * 100.00).toFixed(2);

        $.ajax({
            type: 'PUT',
            url: 'money/' + amount

        }).success(function () {

            loadMoney();
        });
    });
});

// Load addresses into our summary table
function loadItems() {
    //  Get our JSON object from the endpoint
    $.ajax({
        url: 'items'
    }).success(function (data, status) {
        fillItemTable(data, status);
    });
}

function loadMoney() {

    $.ajax({
        url: 'money'
    }).success(function (data, status) {
        fillMoneyTable(data, status);
    });
}

function loadChange() {

    $.ajax({
        url: 'change'
    }).success(function (data, status) {
        fillChangeTable(data, status);
    });
}

function fillItemTable(itemList, status) {
    clearItemTable();

    // store our tbody in a variable 
    var summaryTable = $('#contentRows');

    $.each(itemList, function (arrayPosition, person) {
        summaryTable.append($('<tr>')
                .append($('<td>')
                        .append($('<td>').text(person.name)))
                .append($('<td>').text(person.cost))
                .append($('<td>').text(person.quantity))

                .append($('<td>')
                        .append($('<a>')
                                .attr({
                                    'onClick': 'buyItem(' + person.itemId + ')'
                                })
                                .text('Buy'))));
    });
}

function fillMoneyTable(balance, status) {

    clearMoneyTable();

    balance = balance.toLocaleString('en-US', { style: 'currency', currency: 'USD' });

    var summaryTable = $('#moneyRows');

    summaryTable.append($('<tr>')
            .append($('<td>')
                    .append($('<td>').text(balance))));

}
function fillChangeTable(changeList, status) {
    clearChangeTable();

    // store our tbody in a variable 
    var summaryTable = $('#changeRows');

    $.each(changeList, function (arrayPosition, person) {
        summaryTable.append($('<tr>')
                .append($('<td>')
                        .append($('<td>').text(person.toString()))));
    });


    summaryTable.append($('<tr>')
            .append($('<td>')
            .append($('<a>')
                    .attr({
                        'onClick': 'takeChange()'
                    })
                    .text('Take Change'))));

}

function clearItemTable() {
    $('#contentRows').empty();
}

function clearMoneyTable() {
    $('#moneyRows').empty();
}

function clearChangeTable() {
    $('#changeRows').empty();
}

function buyItem(id) {
    var answer = confirm('Do you really want to buy this item?');

    if (answer === true) {
        $.ajax({
            type: 'PUT',
            url: 'item/' + id
        }).success(function () {
            // reload summary
            loadItems();
            loadMoney();
            loadChange();
        });
    }
}
function takeChange() {

    $.ajax({
        type: 'PUT',
        url: 'change'
    }).success(function () {
        // reload summary
        loadItems();
        loadMoney();
        loadChange();
    });

} 