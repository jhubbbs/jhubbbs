/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sg.vendingmachinemvc.dao;

import com.sg.vendingmachinemvc.model.Item;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 *
 * @author apprentice
 */
public class ItemListDao {
    private Map<Integer, Item> itemMap = new HashMap<>();
    private static int itemIdCounter = 0;
    
    
    public void generateItems(){
        
        
        addItem(new Item("soda",3,1));
        addItem(new Item("banana",10,3));
        addItem(new Item("chips", 2.34, 5));
        addItem(new Item("chicken", .04, 42));
        addItem(new Item("walnut soup", 9.99, 14));
        
    }
    

    
    public void addItem(Item item) {
        // We need to keep track of the ID manually right now
        // In our DB implementation, the database will assign the ID
        item.setItemId(itemIdCounter);
        // Once we assign the ID, we need to increment the counter for the next item
        itemIdCounter++;
        // add our item to the map
        itemMap.put(item.getItemId(), item);
        
    }

    
    public Item getItemById(int itemId) {
        // Get the contact from the hashmap which uses the ID as its key
        return itemMap.get(itemId);
    }

    
    public List<Item> getAllItemsByName(String nameToSearch) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    
    public List<Item> getAllItems() {
        // We want all of our contacts, but do we really need the hashmap?
        // Not really - we just need the list of contacts.
        // The ID is still on the Contact object, so values() would have what we need
        List<Item> allItems = new ArrayList<>(itemMap.values());
        return allItems;
    }

    
    public void updateItem(Item item) {
        // Updating a record on a hashmap is as simple as put with the Key and Value
        itemMap.put(item.getItemId(), item);
    }

    
    public void removeItem(int itemId) {
        // remove the contact from the hashmap
        itemMap.remove(itemId);
    }

}
