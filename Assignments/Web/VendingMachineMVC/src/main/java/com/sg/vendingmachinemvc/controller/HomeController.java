/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sg.vendingmachinemvc.controller;

import com.sg.vendingmachinemvc.dao.ItemListDao;
import com.sg.vendingmachinemvc.model.Item;
import java.util.ArrayList;
import java.util.List;
import javax.inject.Inject;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 *
 * @author parallels
 */
@Controller
public class HomeController {
    // Our DAO for the controller - will be wired up for dependency injection

    private ItemListDao dao;
    private double balance = 0;

    private int quarters = 0;
    private int dimes = 0;
    private int nickels = 0;
    private int pennies = 0;

    @Inject
    public HomeController(ItemListDao dao) {
        this.dao = dao;
    }

    @RequestMapping(value = {"/", "/home"}, method = RequestMethod.GET)
    public String displayHomePage() {
        return "home";
    }

// - Retrieve a item (GET)
//        - /item/{itemId}
//        - Response Body: Item in JSON  
    @RequestMapping(value = "/item/{id}", method = RequestMethod.GET)
    @ResponseBody
    public Item getItem(@PathVariable("id") int id) {
        return dao.getItemById(id);
    }

    //- Retrieve ALL items (GET)
//        - /items
//        -ResponseBody: Array of Items, in JSON form
    @RequestMapping(value = "/items", method = RequestMethod.GET)
    @ResponseBody
    public List<Item> getAllItems() {

        if (dao.getAllItems().isEmpty()) {
            dao.generateItems();
        }

        return dao.getAllItems();
    }

    @RequestMapping(value = "/item/{id}", method = RequestMethod.PUT)
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void buyItem(@PathVariable("id") int id, Item item) {

        if (dao.getItemById(id).getCost() <= balance && dao.getItemById(id).getQuantity() > 0) {

            Item myItem = dao.getItemById(id);
            balance = balance - myItem.getCost();
            myItem.setQuantity(myItem.getQuantity() - 1);

            dao.updateItem(myItem);

            //calculate change, set change, and then set balance to zero
            while (balance > 0) {
                if (balance - .25 >= 0) {
                    quarters++;
                    balance = balance - .25;
                } else if (balance - .10 >= 0) {
                    dimes++;
                    balance = balance - .10;
                } else if (balance - .05 >= 0) {
                    nickels++;
                    balance = balance - .05;
                } else {
                    pennies++;
                    balance = balance - .01;
                }
            }
            balance = 0;

        }
    }

    @RequestMapping(value = "/money/{amount}", method = RequestMethod.PUT)
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void addMoney(@PathVariable("amount") String amount) {

        double addMe = Double.parseDouble(amount) / 100;
        balance = balance + addMe;

    }

    @RequestMapping(value = "/money", method = RequestMethod.GET)
    @ResponseBody
    public double getMoney() {
        
        
        return balance;
    }

    @RequestMapping(value = "/change", method = RequestMethod.GET)
    @ResponseBody
    public List<String> getChange() {

        List<String> changeList = new ArrayList<>();
        changeList.add("Quarters: " + String.valueOf(quarters));
        changeList.add("Dimes: " + String.valueOf(dimes));
        changeList.add("Nickels: " + String.valueOf(nickels));
        changeList.add("Pennies: " + String.valueOf(pennies));

        return changeList;
    }

    @RequestMapping(value = "/change", method = RequestMethod.PUT)
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void takeChange() {

        quarters = 0;
        dimes = 0;
        nickels = 0;
        pennies = 0;
    }
}
