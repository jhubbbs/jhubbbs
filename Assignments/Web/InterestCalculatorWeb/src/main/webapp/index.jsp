<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Interest Calculator</title>
    </head>
    <body>
        <h1>Interest Calculator</h1>


        <form action="CalcServlet" method="POST">

            Starting balance: <input type="number" name="starting" min=".01" step=".01" required><br><br>
            Annual rate: <input type="number" name="rate" min=".01" step=".01" required><br><br>
            Number of years: <input type ="number" name="years" min="1" required><br><br>

            <input type="submit" value="Calculate!">


        </form>

    </body>
</html>
