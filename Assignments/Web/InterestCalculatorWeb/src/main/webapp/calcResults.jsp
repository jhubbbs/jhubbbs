<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Interest Calculator Results</title>
    </head>
    <body>
        <h1>Results</h1>

        <fmt:setLocale value="en_US"/>

        <c:forEach items="${resultsList}" var="year">
            <br>
            <br>
            Year: ${year.key}<br>
            Starting balance:
            <fmt:formatNumber value="${year.value.get(0)}" type="currency"/>
            <br>

            Interest earned: 
            <fmt:formatNumber value="${year.value.get(1)}" type="currency"/>
            <br>
            Ending balance: 
            <fmt:formatNumber value="${year.value.get(2)}" type="currency"/>
            <br>

        </c:forEach>

        <br>

        <form action="CalcServlet" method="GET">
            <input type="submit" value="Again!">
        </form>

    </body>
</html>
