/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sg.interestcalculatorweb;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author apprentice
 */
@WebServlet(name = "CalcServlet", urlPatterns = {"/CalcServlet"})
public class CalcServlet extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        RequestDispatcher rd = request.getRequestDispatcher("index.jsp");
        rd.forward(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        double starting = Double.parseDouble(request.getParameter("starting"));
        double annualRate = Double.parseDouble(request.getParameter("rate"));
        int years = Integer.parseInt(request.getParameter("years"));

        double balance = starting;
        double quarterly = annualRate / 4;
        double interest;
        double tempBalance = balance;

        HashMap<Integer, ArrayList> resultsList = new HashMap();

        for (Integer currentYear = 1; currentYear <= years; currentYear++) {

            resultsList.put(currentYear, new ArrayList());

            resultsList.get(currentYear).add(balance);

            tempBalance = tempBalance * (1 + (quarterly / 100));
            tempBalance = tempBalance * (1 + (quarterly / 100));
            tempBalance = tempBalance * (1 + (quarterly / 100));
            tempBalance = tempBalance * (1 + (quarterly / 100));

            interest = tempBalance - balance;
            resultsList.get(currentYear).add(interest);

            balance = tempBalance;
            resultsList.get(currentYear).add(balance);

        }
        //key: year, start balance, interest, ending balance

        request.setAttribute("resultsList", resultsList);
        RequestDispatcher rd = request.getRequestDispatcher("calcResults.jsp");
        rd.forward(request, response);

    }

    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
