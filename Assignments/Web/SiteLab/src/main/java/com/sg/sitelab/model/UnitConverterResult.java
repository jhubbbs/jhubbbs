/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sg.sitelab.model;

/**
 *
 * @author apprentice
 */
public class UnitConverterResult {
    private String conType;
    private String unitType;
    private String unitType2;
    private double amount;
    private double conAmount;

    /**
     * @return the conType
     */
    public String getConType() {
        return conType;
    }

    /**
     * @param conType the conType to set
     */
    public void setConType(String conType) {
        this.conType = conType;
    }

    /**
     * @return the unitType
     */
    public String getUnitType() {
        return unitType;
    }

    /**
     * @param unitType the unitType to set
     */
    public void setUnitType(String unitType) {
        this.unitType = unitType;
    }

    /**
     * @return the unitType2
     */
    public String getUnitType2() {
        return unitType2;
    }

    /**
     * @param unitType2 the unitType2 to set
     */
    public void setUnitType2(String unitType2) {
        this.unitType2 = unitType2;
    }

    /**
     * @return the amount
     */
    public double getAmount() {
        return amount;
    }

    /**
     * @param amount the amount to set
     */
    public void setAmount(double amount) {
        this.amount = amount;
    }

    /**
     * @return the conAmount
     */
    public double getConAmount() {
        return conAmount;
    }

    /**
     * @param conAmount the conAmount to set
     */
    public void setConAmount(double conAmount) {
        this.conAmount = conAmount;
    }
}
