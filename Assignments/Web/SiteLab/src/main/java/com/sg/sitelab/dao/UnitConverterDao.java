/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sg.sitelab.dao;

import com.sg.sitelab.model.UnitConverterResult;
import java.util.HashMap;
import java.util.Map;

/**
 *
 * @author apprentice
 */
public class UnitConverterDao {

    private Map<Integer, UnitConverterResult> resultMap = new HashMap<>();

    

    public void addResult(UnitConverterResult result) {
        
        //only one result stored at a time, just overwrite spot 0
        resultMap.put(0, result);
        
        
    }
    
    
    public UnitConverterResult getResult() {
        
        UnitConverterResult myResult = resultMap.get(0);
        
        return myResult;
    }
    
}
