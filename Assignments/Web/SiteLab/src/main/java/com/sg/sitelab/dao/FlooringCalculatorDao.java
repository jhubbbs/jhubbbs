/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sg.sitelab.dao;

import com.sg.sitelab.model.FlooringCalcResult;
import java.util.HashMap;
import java.util.Map;

/**
 *
 * @author apprentice
 */
public class FlooringCalculatorDao {

    private Map<Integer, FlooringCalcResult> resultMap = new HashMap<>();

    

    public void addResult(FlooringCalcResult result) {
        
        //only one result stored at a time, just overwrite spot 0
        resultMap.put(0, result);
        
        
    }
    
    
    public FlooringCalcResult getResult() {
        
        FlooringCalcResult myResult = resultMap.get(0);
        
        return myResult;
    }
    
    public int getSize() {

        return resultMap.size();
    }
}
