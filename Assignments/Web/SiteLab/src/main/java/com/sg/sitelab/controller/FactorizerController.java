/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sg.sitelab.controller;

import com.sg.sitelab.dao.FactorizerDao;
import com.sg.sitelab.model.FactorizerResult;
import java.util.ArrayList;
import javax.servlet.http.HttpServletRequest;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 *
 * @author apprentice
 */
@Controller
public class FactorizerController {
    
     private FactorizerDao dao = new FactorizerDao();
    
    
    
    
    @RequestMapping(value="/factorizer", method=RequestMethod.GET)
    public String displayFactorizer(){
        return "factorizer";
    }
    
    
    @RequestMapping(value="doFactorizer",method=RequestMethod.POST)
    public String doFactorizer(HttpServletRequest req){
        
        int number = Integer.parseInt(req.getParameter("number"));
        
        int sum = 0;
        ArrayList factors = new ArrayList<>();

        boolean isPrime = false;
        boolean isPerfect = false;

        for (int x = 1; x < number; x++) {
            if (number % x == 0) {
                factors.add(x);
                sum = sum + x;
            }
        }
        
        if (sum == number) {
            isPerfect = true;
        }

        if (factors.size() == 1) {
            isPrime = true;
        }

       
        
        FactorizerResult result = new FactorizerResult();
        result.setNumber(number);
        result.setFactors(factors);
        result.setIsPrime(isPrime);
        result.setIsPerfect(isPerfect);
        
        dao.addResult(result);
        
        return "redirect:displayResultFactorizer";
        
    }
    
    @RequestMapping(value="/displayResultFactorizer", method=RequestMethod.GET)
    public String displayResultFactorizer(Model model){
        
        // Get result
        FactorizerResult myResult = dao.getResult();
        
        // Put the result on the Model
        model.addAttribute("result", myResult);
        
        // return the logical view
        return "factorizerResults";
    }
}
