/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sg.sitelab.model;

import java.util.ArrayList;

/**
 *
 * @author apprentice
 */
public class FactorizerResult {
    private int number;
    private ArrayList factors;
    private boolean isPrime;
    private boolean isPerfect;

    /**
     * @return the number
     */
    public int getNumber() {
        return number;
    }

    /**
     * @param number the number to set
     */
    public void setNumber(int number) {
        this.number = number;
    }

    /**
     * @return the factors
     */
    public ArrayList getFactors() {
        return factors;
    }

    /**
     * @param factors the factors to set
     */
    public void setFactors(ArrayList factors) {
        this.factors = factors;
    }

    /**
     * @return the isPrime
     */
    public boolean isIsPrime() {
        return isPrime;
    }

    /**
     * @param isPrime the isPrime to set
     */
    public void setIsPrime(boolean isPrime) {
        this.isPrime = isPrime;
    }

    /**
     * @return the isPerfect
     */
    public boolean isIsPerfect() {
        return isPerfect;
    }

    /**
     * @param isPerfect the isPerfect to set
     */
    public void setIsPerfect(boolean isPerfect) {
        this.isPerfect = isPerfect;
    }
}
