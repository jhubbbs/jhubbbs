/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sg.sitelab.controller;


import com.sg.sitelab.dao.InterestCalculatorDao;
import com.sg.sitelab.model.InterestCalcResult;
import java.util.ArrayList;
import java.util.HashMap;
import javax.servlet.http.HttpServletRequest;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 *
 * @author apprentice
 */
@Controller
public class InterestCalculatorController {
    
     private InterestCalculatorDao dao = new InterestCalculatorDao();
    
    
    
    
    @RequestMapping(value="/interestCalc", method=RequestMethod.GET)
    public String displayFactorizer(){
        return "interestCalc";
    }
    
    
    @RequestMapping(value="calculateInterest",method=RequestMethod.POST)
    public String calculateInterest(HttpServletRequest request){
        
       double starting = Double.parseDouble(request.getParameter("starting"));
        double annualRate = Double.parseDouble(request.getParameter("rate"));
        int years = Integer.parseInt(request.getParameter("years"));

        double balance = starting;
        double quarterly = annualRate / 4;
        double interest;
        double tempBalance = balance;
        
        

        HashMap<Integer, ArrayList> resultsList = new HashMap();

        for (Integer currentYear = 1; currentYear <= years; currentYear++) {

            resultsList.put(currentYear, new ArrayList());

            resultsList.get(currentYear).add(balance);

            tempBalance = tempBalance * (1 + (quarterly / 100));
            tempBalance = tempBalance * (1 + (quarterly / 100));
            tempBalance = tempBalance * (1 + (quarterly / 100));
            tempBalance = tempBalance * (1 + (quarterly / 100));

            interest = tempBalance - balance;
            resultsList.get(currentYear).add(interest);

            balance = tempBalance;
            resultsList.get(currentYear).add(balance);

        }
        //key: year, start balance, interest, ending balance

       
        
        InterestCalcResult result = new InterestCalcResult();
        result.setResultsList(resultsList);
        
        dao.addResult(result);
        
        return "redirect:displayResultInterestCalculator";
        
    }
    
    @RequestMapping(value="/displayResultInterestCalculator", method=RequestMethod.GET)
    public String displayResultInterestCalculator(Model model){
        
        // Get result
        InterestCalcResult myResult = dao.getResult();
        
        // Put the result on the Model
        model.addAttribute("result", myResult);
        
        // return the logical view
        return "interestCalcResults";
    }
}
