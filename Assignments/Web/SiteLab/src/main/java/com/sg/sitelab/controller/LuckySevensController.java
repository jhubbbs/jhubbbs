/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sg.sitelab.controller;

import com.sg.sitelab.dao.LuckySevensDao;
import com.sg.sitelab.model.LuckySevensResult;
import java.util.Random;
import javax.servlet.http.HttpServletRequest;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 *
 * @author apprentice
 */
@Controller
public class LuckySevensController {
    
     private LuckySevensDao dao = new LuckySevensDao();
    
    
    
    
    @RequestMapping(value="/luckySevens", method=RequestMethod.GET)
    public String displayLuckySevens(){
        return "luckySevens";
    }
    
    
    @RequestMapping(value="playLuckySevens",method=RequestMethod.POST)
    public String playLuckySevens(HttpServletRequest req){
        
        int bid = Integer.parseInt(req.getParameter("bid"));
        
        int startingBalance = bid;
        int rolls = 0;
        int maxBalance = bid;
        int maxBalanceRoll = 1;
        
        Random rGen = new Random();
        int d1;
        int d2;
        
        do{
            rolls++;
            
            d1 = rGen.nextInt(5)+1;
            d2 = rGen.nextInt(5)+1;
            
            if(bid > maxBalance){
                maxBalance = bid;
                maxBalanceRoll = rolls;
            }
            
            if(d1+d2==7){
                bid = bid + 4;
            } else{
                bid--;
            }
            
            
            
        } while(bid > 0);
        
        
        
        LuckySevensResult result = new LuckySevensResult();
        result.setStartingBalance(startingBalance);
        result.setRolls(rolls);
        result.setMaxBalance(maxBalance);
        result.setMaxBalanceRoll(maxBalanceRoll);
        
        dao.addResult(result);
        
        return "redirect:displayResultLuckySevens";
        //return "luckySevensResults";
    }
    
    @RequestMapping(value="/displayResultLuckySevens", method=RequestMethod.GET)
    public String displayResultLuckySevens(Model model){
        
        // Get result
        LuckySevensResult myResult = dao.getResult();
        
        // Put the result on the Model
        model.addAttribute("result", myResult);
        
        // return the logical view
        return "luckySevensResults";
    }
}
