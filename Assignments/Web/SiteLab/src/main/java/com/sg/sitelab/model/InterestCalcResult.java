/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sg.sitelab.model;

import java.util.ArrayList;
import java.util.HashMap;

/**
 *
 * @author apprentice
 */
public class InterestCalcResult {
    private HashMap<Integer, ArrayList> resultsList;

    /**
     * @return the resultsList
     */
    public HashMap<Integer, ArrayList> getResultsList() {
        return resultsList;
    }

    /**
     * @param resultsList the resultsList to set
     */
    public void setResultsList(HashMap<Integer, ArrayList> resultsList) {
        this.resultsList = resultsList;
    }
}
