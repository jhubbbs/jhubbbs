/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sg.sitelab.controller;

import com.sg.sitelab.dao.FlooringCalculatorDao;
import com.sg.sitelab.model.FlooringCalcResult;
import javax.servlet.http.HttpServletRequest;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 *
 * @author apprentice
 */
@Controller
public class FlooringCalculatorController {

    private FlooringCalculatorDao dao = new FlooringCalculatorDao();

    @RequestMapping(value = "/flooringCalc", method = RequestMethod.GET)
    public String displayFlooringCalc() {
        return "flooringCalc";
    }

    @RequestMapping(value = "calculateFlooring", method = RequestMethod.POST)
    public String calculateFlooring(HttpServletRequest request) {

        double width = Double.parseDouble(request.getParameter("width"));
        double length = Double.parseDouble(request.getParameter("length"));
        double matRate = Double.parseDouble(request.getParameter("matRate"));
        double area = width * length;

        double matCost = matRate * area;
        double labCost = 0;

        double testArea = area;
        while (testArea > 0) {

            testArea = testArea - 5;
            labCost = labCost + 21.5;
            //21.5 per 5sqft laborRate. 5sqft intervals only

        }

        
        FlooringCalcResult result = new FlooringCalcResult();
        result.setArea(area);
        result.setMatCost(matCost);
        result.setLabCost(labCost);
        result.setTotalCost(matCost+labCost);

        dao.addResult(result);

        return "redirect:displayResultFlooringCalculator";

    }

    @RequestMapping(value = "/displayResultFlooringCalculator", method = RequestMethod.GET)
    public String displayResultFlooringCalculator(Model model) {

        // Get result
        FlooringCalcResult myResult = dao.getResult();

        // Put the result on the Model
        model.addAttribute("result", myResult);

        // return the logical view
        return "flooringCalcResults";
    }
}
