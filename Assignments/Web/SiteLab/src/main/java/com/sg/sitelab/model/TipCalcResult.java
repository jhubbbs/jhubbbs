/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sg.sitelab.model;

/**
 *
 * @author apprentice
 */
public class TipCalcResult {
    private double baseAmount;
    private double tipRate;
    private double tipAmount;
    private double total;

    /**
     * @return the baseAmount
     */
    public double getBaseAmount() {
        return baseAmount;
    }

    /**
     * @param baseAmount the baseAmount to set
     */
    public void setBaseAmount(double baseAmount) {
        this.baseAmount = baseAmount;
    }

    /**
     * @return the tipRate
     */
    public double getTipRate() {
        return tipRate;
    }

    /**
     * @param tipRate the tipRate to set
     */
    public void setTipRate(double tipRate) {
        this.tipRate = tipRate;
    }

    /**
     * @return the tipAmount
     */
    public double getTipAmount() {
        return tipAmount;
    }

    /**
     * @param tipAmount the tipAmount to set
     */
    public void setTipAmount(double tipAmount) {
        this.tipAmount = tipAmount;
    }

    /**
     * @return the total
     */
    public double getTotal() {
        return total;
    }

    /**
     * @param total the total to set
     */
    public void setTotal(double total) {
        this.total = total;
    }
    
}
