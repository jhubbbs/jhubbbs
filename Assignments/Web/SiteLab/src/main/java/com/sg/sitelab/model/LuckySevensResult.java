/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sg.sitelab.model;

/**
 *
 * @author apprentice
 */
public class LuckySevensResult {

    
    private int startingBalance;
    private int rolls;
    private int maxBalance;
    private int maxBalanceRoll;

    
    /**
     * @return the startingBalance
     */
    public int getStartingBalance() {
        return startingBalance;
    }

    /**
     * @param startingBalance the startingBalance to set
     */
    public void setStartingBalance(int startingBalance) {
        this.startingBalance = startingBalance;
    }

    /**
     * @return the rolls
     */
    public int getRolls() {
        return rolls;
    }

    /**
     * @param rolls the rolls to set
     */
    public void setRolls(int rolls) {
        this.rolls = rolls;
    }

    /**
     * @return the maxBalance
     */
    public int getMaxBalance() {
        return maxBalance;
    }

    /**
     * @param maxBalance the maxBalance to set
     */
    public void setMaxBalance(int maxBalance) {
        this.maxBalance = maxBalance;
    }

    /**
     * @return the maxBalanceRoll
     */
    public int getMaxBalanceRoll() {
        return maxBalanceRoll;
    }

    /**
     * @param maxBalanceRoll the maxBalanceRoll to set
     */
    public void setMaxBalanceRoll(int maxBalanceRoll) {
        this.maxBalanceRoll = maxBalanceRoll;
    }

   
}
