/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sg.sitelab.model;

/**
 *
 * @author apprentice
 */
public class FlooringCalcResult {
    
    private double matCost;
    private double labCost;
    private double area;
    private double totalCost;

    /**
     * @return the matCost
     */
    public double getMatCost() {
        return matCost;
    }

    /**
     * @param matCost the matCost to set
     */
    public void setMatCost(double matCost) {
        this.matCost = matCost;
    }

    /**
     * @return the labCost
     */
    public double getLabCost() {
        return labCost;
    }

    /**
     * @param labCost the labCost to set
     */
    public void setLabCost(double labCost) {
        this.labCost = labCost;
    }

    /**
     * @return the area
     */
    public double getArea() {
        return area;
    }

    /**
     * @param area the area to set
     */
    public void setArea(double area) {
        this.area = area;
    }

    /**
     * @return the totalCost
     */
    public double getTotalCost() {
        return totalCost;
    }

    /**
     * @param totalCost the totalCost to set
     */
    public void setTotalCost(double totalCost) {
        this.totalCost = totalCost;
    }
}
