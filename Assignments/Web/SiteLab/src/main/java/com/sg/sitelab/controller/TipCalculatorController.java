/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sg.sitelab.controller;

import com.sg.sitelab.dao.TipCalculatorDao;
import com.sg.sitelab.model.TipCalcResult;
import javax.servlet.http.HttpServletRequest;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 *
 * @author apprentice
 */
@Controller
public class TipCalculatorController {

    private TipCalculatorDao dao = new TipCalculatorDao();

    @RequestMapping(value = "/tipCalc", method = RequestMethod.GET)
    public String displayTipCalc() {
        return "tipCalc";
    }

    @RequestMapping(value = "calculateTip", method = RequestMethod.POST)
    public String calculateTip(HttpServletRequest request) {

        double amount = Double.parseDouble(request.getParameter("amount"));
        double tipRate = Double.parseDouble(request.getParameter("tipRate"));
        
        double tipAmount = amount * (tipRate / 100);
        
        double total = tipAmount + amount;
        
      
        
        TipCalcResult result = new TipCalcResult();
        result.setBaseAmount(amount);
        result.setTipRate(tipRate);
        result.setTipAmount(tipAmount);
        result.setTotal(total);

        dao.addResult(result);

        return "redirect:displayResultTipCalculator";

    }

    @RequestMapping(value = "/displayResultTipCalculator", method = RequestMethod.GET)
    public String displayResultTipCalculator(Model model) {

        // Get result
        TipCalcResult myResult = dao.getResult();

        // Put the result on the Model
        model.addAttribute("result", myResult);

        // return the logical view
        return "tipCalcResults";
    }
}
