/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sg.sitelab.controller;

import com.sg.sitelab.dao.UnitConverterDao;
import com.sg.sitelab.model.UnitConverterResult;
import javax.servlet.http.HttpServletRequest;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 *
 * @author apprentice
 */
@Controller
public class UnitConverterController {

    private UnitConverterDao dao = new UnitConverterDao();

    @RequestMapping(value = "/unitConverter", method = RequestMethod.GET)
    public String displayUnitConverter() {
        return "unitConverter";
    }

    @RequestMapping(value = "convertUnit", method = RequestMethod.POST)
    public String convertUnit(HttpServletRequest request) {

        String conType = request.getParameter("conType");
        String unitType = request.getParameter("unitType");
        String unitType2 = request.getParameter("unitType2");
        double amount = Double.parseDouble(request.getParameter("amount"));
        double conAmount = 0;

        if (!unitType.equals(unitType2)) {

            if (conType.equals("Temperature")) {

                switch (unitType) {
                    case "Fahrenheit":
                        if (unitType2.equals("Celsius")) {
                            conAmount = (amount - 32) * 0.5555555;

                        } else if (unitType2.equals("Kelvin")) {
                            conAmount = (amount + 459.67) * .555555;
                        }
                        break;
                    case "Celsius":
                        if (unitType2.equals("Fahrenheit")) {
                            conAmount = (amount * 1.8) + 32;

                        } else if (unitType2.equals("Kelvin")) {
                            conAmount = amount + 273.15;
                        }
                        break;
                    case "Kelvin":
                        if (unitType2.equals("Fahrenheit")) {
                            conAmount = (amount * 1.8) - 459.67;

                        } else if (unitType2.equals("Celsius")) {
                            conAmount = amount - 273.15;
                        }
                        break;
                    default:
                        break;
                }

            } else if (conType.equals("Currency")) {
                switch (unitType) {
                    case "USD":
                        if (unitType2.equals("CAD")) {
                            conAmount = amount * 1.33;

                        } else if (unitType2.equals("JPY")) {
                            conAmount = amount * 104.23;

                        }
                        break;
                    case "CAD":
                        if (unitType2.equals("USD")) {
                            conAmount = amount * .75;

                        } else if (unitType2.equals("JPY")) {
                            conAmount = amount * 78.06;

                        }
                        break;
                    case "JPY":
                        if (unitType2.equals("USD")) {
                            conAmount = amount * .0096;

                        } else if (unitType2.equals("CAD")) {
                            conAmount = amount * 0.013;

                        }
                        break;
                    default:
                        break;
                }
            }
        } else {
            conAmount = amount;
        }

        UnitConverterResult result = new UnitConverterResult();
        result.setConType(conType);
        result.setUnitType(unitType);
        result.setUnitType2(unitType2);
        result.setAmount(amount);
        result.setConAmount(conAmount);

        dao.addResult(result);

        return "redirect:displayResultUnitConverter";

    }

    @RequestMapping(value = "/displayResultUnitConverter", method = RequestMethod.GET)
    public String displayResultUnitConverter(Model model) {

        // Get result
        UnitConverterResult myResult = dao.getResult();

        // Put the result on the Model
        model.addAttribute("result", myResult);

        // return the logical view
        return "unitConverterResults";
    }
}
