<%@taglib prefix="s" uri="http://www.springframework.org/tags" %>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@taglib prefix="sf" uri="http://www.springframework.org/tags/form" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Flooring Calculator</title>
        <!-- Bootstrap CSS -->
        <link href="${pageContext.request.contextPath}/css/bootstrap.min.css" rel="stylesheet" />
        <!-- SWC Icon -->
        <link rel="shortcut icon" href="${pageContext.request.contextPath}/img/icon.png">
    </head>
    <body>
        <div class="container">
            <h1>Flooring Calculator</h1>
            <hr />
            <div class="navbar">
                <ul class="nav nav-tabs">
                    <li role="presentation" >
                        <a href="${pageContext.request.contextPath}/home">Home</a>
                    </li>
                    <li role="presentation" >
                        <a href="${pageContext.request.contextPath}/luckySevens">Lucky Sevens</a>
                    </li>
                    <li role="presentation" >
                        <a href="${pageContext.request.contextPath}/factorizer">Factorizer</a>
                    </li>
                    <li role="presentation">
                        <a href="${pageContext.request.contextPath}/interestCalc">Interest Calculator</a>
                    </li>
                    <li role="presentation" class="active">
                        <a href="${pageContext.request.contextPath}/flooringCalc">Flooring Calculator</a>
                    </li>
                    <li role="presentation">
                        <a href="${pageContext.request.contextPath}/tipCalc">Tip Calculator</a>
                    </li>
                    <li role="presentation">
                        <a href="${pageContext.request.contextPath}/unitConverter">Unit Converter</a>
                    </li>
                </ul>
            </div>
            <form class="form-horizontal" role="form" action="calculateFlooring" method="POST">

                Width: <input type="number" name="width" min=".01" step=".01" required><br><br>
                Length: <input type="number" name="length" min=".01" step=".01" required><br><br>
                Mat Cost/SqFt: <input type ="number" name="matRate" min=".01" step=".01" required><br><br>
                <br>
                <button type="submit" id="add-button"
                        class="btn btn-default">Factorize!</button>
            </form>

        </div>
        <script src="${pageContext.request.contextPath}/js/jquery-2.2.4.min.js"></script>
        <script src="${pageContext.request.contextPath}/js/bootstrap.min.js"></script>
    </body>
</html>
