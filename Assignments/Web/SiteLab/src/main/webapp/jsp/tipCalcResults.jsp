<%@taglib prefix="s" uri="http://www.springframework.org/tags" %>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@taglib prefix="sf" uri="http://www.springframework.org/tags/form" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Tip Calculator - Results</title>
        <!-- Bootstrap CSS -->
        <link href="${pageContext.request.contextPath}/css/bootstrap.min.css" rel="stylesheet" />
        <!-- SWC Icon -->
        <link rel="shortcut icon" href="${pageContext.request.contextPath}/img/icon.png">
    </head>
    <body>
        <div class="container">
            <h1>Tip Calculator - Results</h1>
            <hr />
            <div class="navbar">
                <ul class="nav nav-tabs">
                    <li role="presentation" >
                        <a href="${pageContext.request.contextPath}/home">Home</a>
                    </li>
                    <li role="presentation" >
                        <a href="${pageContext.request.contextPath}/luckySevens">Lucky Sevens</a>
                    </li>
                    <li role="presentation" >
                        <a href="${pageContext.request.contextPath}/factorizer">Factorizer</a>
                    </li>
                    <li role="presentation" >
                        <a href="${pageContext.request.contextPath}/interestCalc">Interest Calculator</a>
                    </li>
                    <li role="presentation" >
                        <a href="${pageContext.request.contextPath}/flooringCalc">Flooring Calculator</a>
                    </li>
                    <li role="presentation" class="active">
                        <a href="${pageContext.request.contextPath}/tipCalc">Tip Calculator</a>
                    </li>
                    <li role="presentation">
                        <a href="${pageContext.request.contextPath}/unitConverter">Unit Converter</a>
                    </li>
                </ul>
            </div>


            <fmt:setLocale value="en_US"/>




            Amount:
            <fmt:formatNumber value="${result.baseAmount}" type="currency"/><br>

            Tip %:
            <c:out value="${result.tipRate}"/>
            <br>
            Tip:
            <fmt:formatNumber value="${result.tipAmount}" type="currency"/>
            <br>
            <hr>

            Total:
            <fmt:formatNumber value="${result.total}" type="currency"/>
            <br>



            <br>


            <form action="tipCalc" method="GET">

                <input type="submit" value="Again!">
            </form>


        </div>
        <script src="${pageContext.request.contextPath}/js/jquery-2.2.4.min.js"></script>
        <script src="${pageContext.request.contextPath}/js/bootstrap.min.js"></script>
    </body>
</html>
