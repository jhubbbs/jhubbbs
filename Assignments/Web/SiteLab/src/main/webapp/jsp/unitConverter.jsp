<%@taglib prefix="s" uri="http://www.springframework.org/tags" %>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@taglib prefix="sf" uri="http://www.springframework.org/tags/form" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Unit Converter</title>
        <!-- Bootstrap CSS -->
        <link href="${pageContext.request.contextPath}/css/bootstrap.min.css" rel="stylesheet" />
        <!-- SWC Icon -->
        <link rel="shortcut icon" href="${pageContext.request.contextPath}/img/icon.png">
        <script src="${pageContext.request.contextPath}/js/jquery-2.2.4.min.js"></script>
        <script src="${pageContext.request.contextPath}/js/bootstrap.min.js"></script>
    </head>
    <body>
        <div class="container">
            <h1>Unit Converter</h1>
            <hr />
            <div class="navbar">
                <ul class="nav nav-tabs">
                    <li role="presentation" >
                        <a href="${pageContext.request.contextPath}/home">Home</a>
                    </li>
                    <li role="presentation" >
                        <a href="${pageContext.request.contextPath}/luckySevens">Lucky Sevens</a>
                    </li>
                    <li role="presentation" >
                        <a href="${pageContext.request.contextPath}/factorizer">Factorizer</a>
                    </li>
                    <li role="presentation">
                        <a href="${pageContext.request.contextPath}/interestCalc">Interest Calculator</a>
                    </li>
                    <li role="presentation" >
                        <a href="${pageContext.request.contextPath}/flooringCalc">Flooring Calculator</a>
                    </li>
                    <li role="presentation" >
                        <a href="${pageContext.request.contextPath}/tipCalc">Tip Calculator</a>
                    </li>
                    <li role="presentation" class="active">
                        <a href="${pageContext.request.contextPath}/unitConverter">Unit Converter</a>
                    </li>
                </ul>
            </div>





            <form class="form-horizontal" role="form" action="convertUnit" method="POST">

                <label for="qmt-conType">Conversion Type:</label>
                <select id="qmt-conType" name="conType">
                    <option></option>
                    <option class="temperature" value="Temperature">Temperatures</option>
                    <option class="currency" value="Currency">Currencies</option>
                </select>
                <br>
                <br>

                <label for="qmt-unitType" id="label-unitType">From Unit:</label>
                <select id="qmt-unitType" name="unitType">
                    <option></option>
                    <option class="temperature" value="Fahrenheit">&nbsp;&nbsp;&nbsp;Fahrenheit</option>
                    <option class="temperature" value="Celsius">&nbsp;&nbsp;&nbsp;Celsius</option>
                    <option class="temperature" value="Kelvin">&nbsp;&nbsp;&nbsp;Kelvin</option>
                    <option class="currency" value="USD">&nbsp;&nbsp;&nbsp;USD</option>
                    <option class="currency" value="CAD">&nbsp;&nbsp;&nbsp;CAD</option>
                    <option class="currency" value="JPY">&nbsp;&nbsp;&nbsp;JPY</option>
                </select>


                <label for="qmt-unitType2" id="label-unitType2">To Unit:</label>
                <select id="qmt-unitType2" name="unitType2">
                    <option></option>
                    <option class="temperature" value="Fahrenheit">&nbsp;&nbsp;&nbsp;Fahrenheit</option>
                    <option class="temperature" value="Celsius">&nbsp;&nbsp;&nbsp;Celsius</option>
                    <option class="temperature" value="Kelvin">&nbsp;&nbsp;&nbsp;Kelvin</option>
                    <option class="currency" value="USD">&nbsp;&nbsp;&nbsp;USD</option>
                    <option class="currency" value="CAD">&nbsp;&nbsp;&nbsp;CAD</option>
                    <option class="currency" value="JPY">&nbsp;&nbsp;&nbsp;JPY</option>
                </select>

                <br>
                <br>
                Amount: <input type="number" name="amount" required><br><br>


                <br>
                <br>
                <button type="submit" id="add-button"
                        class="btn btn-default">Convert!</button>
            </form>

        </div>

        <script>

            $(function () {
                $('#qmt-unitType, #qmt-unitType2, #label-unitType, #label-unitType2')
                        .hide();
            });
        </script>
        <script>
            $(function () {
                $("#qmt-conType").on("change", function () {
                    $(function () {
                        $('#qmt-unitType, #qmt-unitType2, #label-unitType, #label-unitType2')
                                .show();
                    });

                    var levelClass = $('#qmt-conType').find('option:selected').attr('class');
                    console.log(levelClass);
                    $('#qmt-unitType option').each(function () {
                        var self = $(this);
                        if (self.hasClass(levelClass) || typeof (levelClass) == "undefined") {
                            self.show();
                        } else {
                            self.hide();
                        }
                    });
                    $('#qmt-unitType2 option').each(function () {
                        var self = $(this);
                        if (self.hasClass(levelClass) || typeof (levelClass) == "undefined") {
                            self.show();
                        } else {
                            self.hide();
                        }
                    });
                });
            });
        </script>
        
    </body>
</html>
