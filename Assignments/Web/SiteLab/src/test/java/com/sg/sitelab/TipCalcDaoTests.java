/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sg.sitelab;


import com.sg.sitelab.dao.TipCalculatorDao;
import com.sg.sitelab.model.TipCalcResult;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author apprentice
 */
public class TipCalcDaoTests {
    
    public TipCalcDaoTests() {
    }
    
    TipCalculatorDao dao = new TipCalculatorDao();

    // TODO add test methods here.
    // The methods must be annotated with annotation @Test. For example:
    //
    // @Test
    // public void hello() {}
    
    @Test
    public void testAddResultTipCalc(){
        
        TipCalcResult myResult = new TipCalcResult();
        
        dao.addResult(myResult);
        
        int size = dao.getSize();
        
        assertEquals(1, size);
        
}
 
    @Test
    public void testGetResultTipCalc(){
        
        TipCalcResult myResult = new TipCalcResult();
        
        myResult.setBaseAmount(11);
        myResult.setTipRate(25);
        myResult.setTipAmount(42);
        myResult.setTotal(8);
        
        dao.addResult(myResult);
        
        
        TipCalcResult myNewResult = dao.getResult();
        
        assertEquals(11, myNewResult.getBaseAmount(), .001);
        assertEquals(25, myNewResult.getTipRate(), .001);
        assertEquals(42, myNewResult.getTipAmount(), .001);
        assertEquals(8, myNewResult.getTotal(), .001);
    }
}
