/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sg.sitelab;

import com.sg.sitelab.dao.FlooringCalculatorDao;
import com.sg.sitelab.model.FlooringCalcResult;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author apprentice
 */
public class FlooringCalcDaoTests {

    public FlooringCalcDaoTests() {
    }

    FlooringCalculatorDao dao = new FlooringCalculatorDao();

    // TODO add test methods here.
    // The methods must be annotated with annotation @Test. For example:
    //
    // @Test
    // public void hello() {}
    @Test
    public void testAddResultFlooringCalc() {

        FlooringCalcResult myResult = new FlooringCalcResult();

        dao.addResult(myResult);

        int size = dao.getSize();

        assertEquals(1, size);

    }

    @Test
    public void testGetResultFlooringCalc() {

        FlooringCalcResult myResult = new FlooringCalcResult();

        myResult.setArea(50);
        myResult.setMatCost(29);
        myResult.setLabCost(427);
        myResult.setTotalCost(89);

        dao.addResult(myResult);

        FlooringCalcResult myNewResult = dao.getResult();

        assertEquals(50, myNewResult.getArea(), .001);
        assertEquals(29, myNewResult.getMatCost(), .001);
        assertEquals(427, myNewResult.getLabCost(), .001);
        assertEquals(89, myNewResult.getTotalCost(), .001);
    }
}
