/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sg.sitelab;

import com.sg.sitelab.dao.LuckySevensDao;
import com.sg.sitelab.model.LuckySevensResult;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author apprentice
 */
public class LuckySevensDaoTests {
    
    public LuckySevensDaoTests() {
    }
    
    LuckySevensDao dao = new LuckySevensDao();

    // TODO add test methods here.
    // The methods must be annotated with annotation @Test. For example:
    //
    // @Test
    // public void hello() {}
    
    @Test
    public void testAddResultLuckySevens(){
        
        LuckySevensResult myResult = new LuckySevensResult();
        
        dao.addResult(myResult);
        
        int size = dao.getSize();
        
        assertEquals(1, size);
        
}
 
    @Test
    public void testGetResultLuckySevens(){
        
        LuckySevensResult myResult = new LuckySevensResult();
        
        myResult.setRolls(10);
        myResult.setStartingBalance(25);
        myResult.setMaxBalance(42);
        myResult.setMaxBalanceRoll(8);
        
        dao.addResult(myResult);
        
        
        LuckySevensResult myNewResult = dao.getResult();
        
        assertEquals(10, myNewResult.getRolls());
        assertEquals(25, myNewResult.getStartingBalance());
        assertEquals(42, myNewResult.getMaxBalance());
        assertEquals(8, myNewResult.getMaxBalanceRoll());
    }
}
