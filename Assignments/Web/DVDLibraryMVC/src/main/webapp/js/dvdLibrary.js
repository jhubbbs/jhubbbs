/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

// When the document is ready, populate our summary table
$(document).ready(function () {
    loadDVDs();

    // add the onclick handling for our add button
    $('#add-button').click(function (event) {
        event.preventDefault();
        // need to submit this via AJAX
        $.ajax({
            type: 'POST',
            url: 'dvd',
            // make the JSON dvd
            data: JSON.stringify({
                title: $('#add-title').val(),
                releaseDate: $('#add-release-date').val(),
                mpaa: $('#add-mpaa').val(),
                director: $('#add-director').val(),
                studio: $('#add-studio').val(),
                note: $('#add-note').val()
            }),
            contentType: 'application/json; charset=utf-8',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },
            dataType: 'json'
        }).success(function (data, status) {
            // clear the form and reload the summary table
            $('#add-title').val('');
            $('#add-release-date').val('');
            $('#add-mpaa').val('');
            $('#add-director').val('');
            $('#add-studio').val('');
            $('#add-note').val('');

            // reload the summary table
            $('#validationErrors').empty();
            loadDVDs();
        }).error(function (data, status) {
            $('#validationErrors').empty();
            $.each(data.responseJSON.fieldErrors, function (index, validationError) {
                var errorDiv = $('#validationErrors');
                errorDiv.append(validationError.message).append($('<br>'));
            });
        });
    });

    $('#edit-button').click(function (event) {
        event.preventDefault();
        // update our dvd via AJAX
        $.ajax({
            type: 'PUT',
            url: 'dvd/' + $('#edit-dvd-id').val(),
            data: JSON.stringify({
                dvdId: $('#edit-dvd-id').val(), ////////here
                title: $('#edit-title').val(),
                releaseDate: $('#edit-release-date').val(),
                mpaa: $('#edit-mpaa').val(),
                director: $('#edit-director').val(),
                studio: $('#edit-studio').val(),
                note: $('#edit-note').val()
            }),
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },
            'dataType': 'json'
        }).success(function () {
            loadDVDs();
        });
    });

    $("#search-button").click(function (event) {
        event.preventDefault();
        $.ajax({
            type: 'POST',
            url: 'search/dvds',
            dataType: 'json',
            data: JSON.stringify({
                title: $('#search-title').val(),
                releaseDate: $('#search-release-date').val(),
                mpaa: $('#search-mpaa').val(),
                director: $('#search-director').val(),
                studio: $('#search-studio').val(),
                note: $('#search-note').val()
            }),
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            }
        }).success(function (searchResults, status) {
            $('#search-title').val('');
            $('#search-release-date').val('');
            $('#search-mpaa').val('');
            $('#search-director').val('');
            $('#search-studio').val('');
            $('#search-note').val('');


            fillDVDTable(searchResults, status);
        });
    });
});

// Load dvds into our summary table
function loadDVDs() {
    //  Get our JSON object from the endpoint
    $.ajax({
        url: 'dvds'
    }).success(function (data, status) {
        fillDVDTable(data, status);
    });
}

// used with Search button and loadDVD function
function fillDVDTable(dvdLibrary, status) {
    // clear the previous list
    clearDVDTable();

    // store our tbody in a variable 
    var summaryTable = $('#contentRows');

    $.each(dvdLibrary, function (arrayPosition, myDVD) {
        summaryTable.append($('<tr>')
                .append($('<td>')
                        .append($('<a>')
                                .attr({
                                    'data-dvd-id': myDVD.dvdid,/////////here
                                    'data-toggle': 'modal',
                                    'data-target': '#detailsModal'
                                })
                                .text(myDVD.title + ' (' + myDVD.releaseDate+')')))
                .append($('<td>').text(myDVD.director))
                .append($('<td>')
                        .append($('<a>')
                                .attr({
                                    'data-dvd-id': myDVD.dvdid,/////////here
                                    'data-toggle': 'modal',
                                    'data-target': '#editModal'
                                })
                                .text('Edit')))
                .append($('<td>')
                        .append($('<a>')
                                .attr({
                                    'onClick': 'deleteDVD(' + myDVD.dvdid + ')'//////here
                                })
                                .text('Delete'))));
    });
}

// Clear all the rows from our summary table
function clearDVDTable() {
    $('#contentRows').empty();
}

// Run in response to the show.bs.modal event
// It gets the dvd data and renders it to the dialog
$('#detailsModal').on('show.bs.modal', function (event) {
    var element = $(event.relatedTarget);
    var dvdId = element.data('dvd-id');
    var modal = $(this);
    // Get our object via AJAX
    $.ajax({
        type: 'GET',
        url: 'dvd/' + dvdId
    }).success(function (sampleDVD) {
        modal.find('#dvd-id').text(sampleDVD.dvdId);
        modal.find('#dvd-title').text(sampleDVD.title);
        modal.find('#dvd-releaseDate').text(sampleDVD.releaseDate);
        modal.find('#dvd-mpaa').text(sampleDVD.mpaa);
        modal.find('#dvd-director').text(sampleDVD.director);
        modal.find('#dvd-studio').text(sampleDVD.studio);
        modal.find('#dvd-note').text(sampleDVD.note);

    });
});

$('#editModal').on('show.bs.modal', function (event) {
    var element = $(event.relatedTarget);
    var dvdId = element.data('dvd-id');
    var modal = $(this);

    // get our object via AJAX
    $.ajax({
        type: 'GET',
        url: 'dvd/' + dvdId
    }).success(function (sampleEditDVD) {
        modal.find('#edit-dvd-id').val(sampleEditDVD.dvdid);
        modal.find('#edit-title').val(sampleEditDVD.title);
        modal.find('#edit-release-date').val(sampleEditDVD.releaseDate);
        modal.find('#edit-mpaa').val(sampleEditDVD.mpaa);
        modal.find('#edit-director').val(sampleEditDVD.director);
        modal.find('#edit-studio').val(sampleEditDVD.studio);
        modal.find('#edit-note').val(sampleEditDVD.note);

    });
});

function deleteDVD(id) {
    var answer = confirm('Do you really want to delete this DVD?');

    if (answer === true) {
        $.ajax({
            type: 'DELETE',
            url: 'dvd/' + id
        }).success(function () {
            // reload summary
            loadDVDs();
        });
    }
}        