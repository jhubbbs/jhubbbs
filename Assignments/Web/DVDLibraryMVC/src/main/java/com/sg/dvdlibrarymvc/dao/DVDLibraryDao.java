/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sg.dvdlibrarymvc.dao;

import com.sg.dvdlibrarymvc.model.DVD;
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Scanner;
import java.util.function.Predicate;
import java.util.stream.Collectors;
import org.springframework.core.io.ClassPathResource;

/**
 *
 * @author parallels
 */
public class DVDLibraryDao {

    private Map<Integer, DVD> dvdMap = new HashMap<>();
    private final String DVD_FILE = "dvds.txt";
    private final String DELIMITER = "::";

    // We need to track our IDs.  This will simulate auto-incrementing.
    private static int dvdIdCounter = 0;

    public DVD addDVD(DVD dvd) {
        // We need to keep track of the ID manually right now
        // In our DB implementation, the database will assign the ID
        dvd.setDVDId(dvdIdCounter);
        // Once we assign the ID, we need to increment the counter for the next dvd
        dvdIdCounter++;
        // add our dvd to the map
        dvdMap.put(dvd.getDVDId(), dvd);
        save();
        // We need to return the dvd, per the interface
        return dvd;
    }

    public DVD getDVDById(int dvdId) {
        if (dvdMap.isEmpty()) {
            // Load the dvds from the file
            load();
        }
        // Get the dvd from the hashmap which uses the ID as its key
        return dvdMap.get(dvdId);
    }

    public List<DVD> getAllDVDsByName(String nameToSearch) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public List<DVD> getAllDVDs() {
        // Load the dvds from the file
        load();
        // We want all of our dvds, but do we really need the hashmap?
        // Not really - we just need the list of dvds.
        // The ID is still on the DVD object, so values() would have what we need
        List<DVD> allDVDs = new ArrayList<>(dvdMap.values());
        return allDVDs;
    }

    public void updateDVD(DVD dvd) {
        // Updating a record on a hashmap is as simple as put with the Key and Value
        dvdMap.put(dvd.getDVDId(), dvd);
        save();
    }

    public void removeDVD(int dvdId) {
        // remove the dvd from the hashmap
        dvdMap.remove(dvdId);
        save();
    }

    public List<DVD> searchDVDs(Map<SearchTerm, String> criteria) {
        // Get all the things we're searching for into individual variables
        String titleCriteria = criteria.get(SearchTerm.TITLE);
        String releaseDateCriteria = criteria.get(SearchTerm.RELEASE_DATE);
        String mpaaCriteria = criteria.get(SearchTerm.MPAA);
        String directorCriteria = criteria.get(SearchTerm.DIRECTOR);
        String studioCriteria = criteria.get(SearchTerm.STUDIO);
        String noteCriteria = criteria.get(SearchTerm.NOTE);

        // Declare predicate conditions - to be used by our filters
        Predicate<DVD> titleMatches;
        Predicate<DVD> releaseDateMatches;
        Predicate<DVD> mpaaMatches;
        Predicate<DVD> directorMatches;
        Predicate<DVD> studioMatches;
        Predicate<DVD> noteMatches;

        // Default predicate
        Predicate<DVD> truePredicate = (c) -> {
            return true;
        };

        // Assign values to predicates
        // If a given search term is empty, assign the default Predicate
        // Otherwise, assign the predicate that filters for that term
        // ? expression = ternary expression
        // boolean expression ? true value : false value
        // (Do we have nothing in first name criteria?  Use default : Filter on firstNameCriteria
        titleMatches = (titleCriteria == null || titleCriteria.isEmpty())
                ? truePredicate
                : (c) -> c.getTitle().equalsIgnoreCase(titleCriteria);

        releaseDateMatches = (releaseDateCriteria == null || releaseDateCriteria.isEmpty())
                ? truePredicate
                : (c) -> c.getReleaseDate().equalsIgnoreCase(releaseDateCriteria);

        mpaaMatches = (mpaaCriteria == null || mpaaCriteria.isEmpty())
                ? truePredicate
                : (c) -> c.getMpaa().equalsIgnoreCase(mpaaCriteria);

        directorMatches = (directorCriteria == null || directorCriteria.isEmpty())
                ? truePredicate
                : (c) -> c.getDirector().equalsIgnoreCase(directorCriteria);

        studioMatches = (studioCriteria == null || studioCriteria.isEmpty())
                ? truePredicate
                : (c) -> c.getStudio().equalsIgnoreCase(studioCriteria);
        
        noteMatches = (noteCriteria == null || noteCriteria.isEmpty())
                ? truePredicate
                : (c) -> c.getNote().equalsIgnoreCase(noteCriteria);

        // Return the list of dvds that match the given criteria
        // We will use filters and join them with AND
        return dvdMap.values().stream()
                .filter(titleMatches
                        .and(releaseDateMatches)
                        .and(mpaaMatches)
                        .and(directorMatches)
                        .and(studioMatches)
                        .and(noteMatches))
                .collect(Collectors.toList());
    }

    private void load() {
        try {
            loadFromFile();
        } catch (IOException io) {
            // do nothing
        }
    }

    private void save() {
        try {
            saveToFile();
        } catch (IOException io) {
            // do nothing
            System.out.println(io.getMessage());
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }

    private void loadFromFile() throws IOException {

        try {
            //           Scanner dvdScanner = new Scanner(new BufferedReader(new FileReader(DVD_FILE)));
            Scanner dvdScanner = new Scanner(new BufferedReader(new FileReader(new ClassPathResource(DVD_FILE).getFile())));

            // While there are still lines in the file to read - keep reading them!
            while (dvdScanner.hasNextLine()) {
                String dvdLine = dvdScanner.nextLine();

                String[] dvdProperties = dvdLine.split(DELIMITER);

                if (dvdProperties.length != 7) {
                    continue;
                }

                DVD myDVD = new DVD();
                
                myDVD.setTitle(dvdProperties[1]);
                myDVD.setReleaseDate(dvdProperties[2]);
                myDVD.setMpaa(dvdProperties[3]);
                myDVD.setDirector(dvdProperties[4]);
                myDVD.setStudio(dvdProperties[5]);
                myDVD.setNote(dvdProperties[6]);

                

                try {
                    myDVD.setDVDId(Integer.parseInt(dvdProperties[0]));

                } catch (NumberFormatException e) {
                    continue;
                }

                dvdMap.put(myDVD.getDVDId(), myDVD);
            }

            dvdScanner.close();

        } catch (FileNotFoundException ex) {
            new FileWriter(new ClassPathResource(DVD_FILE).getFile());
        }

    }

    private void saveToFile() throws IOException {
        try {
//        PrintWriter writer = new PrintWriter(new FileWriter(DVD_FILE));
            PrintWriter writer = new PrintWriter(new FileWriter(new ClassPathResource(DVD_FILE).getFile()));

           
            for (DVD d : dvdMap.values()) {
                writer.println(d.getDVDId() + DELIMITER
                        + d.getTitle() + DELIMITER
                        + d.getReleaseDate() + DELIMITER
                        + d.getMpaa() + DELIMITER
                        + d.getDirector() + DELIMITER
                        + d.getStudio() + DELIMITER
                        + d.getNote());
            }

            writer.flush();
            writer.close();
        } catch (IOException io) {

        }
    }

    private void createFile() {

    }
}
