/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sg.addressbookmvc.controller;

import com.sg.addressbookmvc.dao.AddressBookDao;
import com.sg.addressbookmvc.model.Address;
import java.util.List;
import javax.inject.Inject;
import javax.validation.Valid;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 *
 * @author parallels
 */
@Controller
public class HomeController {
        // Our DAO for the controller - will be wired up for dependency injection
    private AddressBookDao dao;

   @Inject
    public HomeController(AddressBookDao dao) {
        this.dao = dao;
    }
    
    @RequestMapping(value = {"/", "/home"}, method = RequestMethod.GET)
    public String displayHomePage() {
        return "home";
    }

// - Retrieve a address (GET)
//        - /address/{addressId}
//        - Response Body: Address in JSON  
    @RequestMapping(value = "/address/{id}", method = RequestMethod.GET)
    @ResponseBody
    public Address getAddress(@PathVariable("id") int id) {
        return dao.getAddressById(id);
    }

//- Create an address (POST)
//      - /address
//       - RequestBody - JSON object of our Address
    // - ResponseBody - JSON object of our Address
    @RequestMapping(value = "/address", method = RequestMethod.POST)
    @ResponseStatus(HttpStatus.CREATED)
    @ResponseBody
    public Address createAddress(@Valid @RequestBody Address address) {
        dao.addAddress(address);
        return address;
    }

//- Delete a address (DELETE)
//        - address/{addressId}
//        - Note: No RequestBody, no ResponseBody
    @RequestMapping(value = "/address/{id}", method = RequestMethod.DELETE)
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void deleteAddress(@PathVariable("id") int id) {
        dao.removeAddress(id);
    }

//- Update a address (PUT)
//        - address/{addressId}
//        - RequestBody: JSON object of our Address, with the addressId
    @RequestMapping(value = "/address/{id}", method = RequestMethod.PUT)
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void putAddress(@PathVariable("id") int id, @Valid @RequestBody Address address) {
        address.setAddressId(id);
        dao.updateAddress(address);
    }
    
//- Retrieve ALL addresss (GET)
//        - /Addresses
//        -ResponseBody: Array of Addresses, in JSON form
    @RequestMapping(value="/addresses", method=RequestMethod.GET)
    @ResponseBody public List<Address> getAllAddresses(){
        return dao.getAllAddresses();
    }

}
