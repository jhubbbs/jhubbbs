/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sg.dvdlibrarydb.service;

import com.sg.dvdlibrarydb.dao.DVDLibraryDaoInterface;
import com.sg.dvdlibrarydb.model.DVD;
import java.util.List;
import javax.inject.Inject;
import javax.validation.Valid;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 *
 * @author apprentice
 */
@Controller
public class DVDLibraryDaoAPI {
    private DVDLibraryDaoInterface dao;

    @Inject
    public DVDLibraryDaoAPI (DVDLibraryDaoInterface dao) {
        this.dao = dao;
    }
    
    @RequestMapping(value = {"/", "/home"}, method = RequestMethod.GET)
    public String displayHomePage() {
        return "home";
    }
    
    @RequestMapping(value = {"/search"}, method = RequestMethod.GET)
    public String displaySearchPage() {
        return "search";
    }
    
    @RequestMapping(value="/dvds", method=RequestMethod.GET)
    @ResponseBody public List<DVD> getAllDVDs(){
        return dao.getAllDVDs();
    }
    
    @RequestMapping(value="/search/{searchTerm}", method=RequestMethod.GET)
    @ResponseBody
    public List<DVD> getDVDsBySearch(@PathVariable("searchTerm") String searchTerm){
        
        if(searchTerm.equals("")||searchTerm.isEmpty()){
            return dao.getAllDVDs();
        }else{
            return dao.getDVDsBySearch(searchTerm);
        }
        
    }
    
    @RequestMapping(value = "/dvd/{id}", method = RequestMethod.PUT)
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void updateDVD(@PathVariable("id") int id, @Valid @RequestBody DVD dvd) {
        dvd.setDVDId(id);
        dao.updateDVD(dvd);
    }
    
    
    @RequestMapping(value = "/dvd/{id}", method = RequestMethod.DELETE)
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void deleteDVD(@PathVariable("id") int id) {
        dao.deleteDVD(id);
    }
    
    @RequestMapping(value = "/dvd/{id}", method = RequestMethod.GET)
    @ResponseBody
    public DVD getDVD(@PathVariable("id") int id) {
        return dao.getDVDById(id);
    }
    
    @RequestMapping(value = "/dvd", method = RequestMethod.POST)
    @ResponseStatus(HttpStatus.CREATED)
    @ResponseBody
    public DVD createDVD(@Valid @RequestBody DVD dvd) {
        dao.addDVD(dvd);
        return dvd;
    }
}
