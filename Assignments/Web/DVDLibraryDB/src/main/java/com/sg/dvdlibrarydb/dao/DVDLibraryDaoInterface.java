/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sg.dvdlibrarydb.dao;

import com.sg.dvdlibrarydb.model.DVD;
import java.util.List;

/**
 *
 * @author apprentice
 */
public interface DVDLibraryDaoInterface {
    
    public void addDVD(DVD dvd);

    public void deleteDVD(int dvdId);

    public void updateDVD(DVD dvd);

    public DVD getDVDById(int id);

    public List<DVD> getAllDVDs();
    
    public List<DVD> getDVDsBySearch(String searchTerm);
}
