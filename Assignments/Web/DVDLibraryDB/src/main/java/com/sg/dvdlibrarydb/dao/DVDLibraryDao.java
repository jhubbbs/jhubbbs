/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sg.dvdlibrarydb.dao;

import com.sg.dvdlibrarydb.model.DVD;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author parallels
 */
public class DVDLibraryDao implements DVDLibraryDaoInterface {

    private JdbcTemplate jdbcTemplate;
    
    public void setJdbcTemplate(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    private static final String SQL_INSERT_DVD = "insert into dvds (title, releaseDate, mpaa, director, studio, note) values (?, ?, ?, ?, ?, ?)";
    private static final String SQL_DELETE_DVD = "delete from dvds where dvdId = ?";
    private static final String SQL_UPDATE_DVD = "update dvds set title = ?, releaseDate = ?, mpaa = ?, director = ?, studio = ?, note = ? where dvdId = ?";
    private static final String SQL_SELECT_DVD = "select * from dvds where dvdId = ?";
    private static final String SQL_SELECT_ALL_DVDS = "select * from dvds";
    private static final String SQL_SELECT_DVDS_SEARCH = "select * from dvds where title like ? "
            + "or releaseDate like ? or mpaa like ? or director like ? "
            + "or studio like ?";


    @Transactional(propagation = Propagation.REQUIRED, readOnly = false)
    @Override
    public void addDVD(DVD dvd) {
        jdbcTemplate.update(SQL_INSERT_DVD,
                dvd.getTitle(),
                dvd.getReleaseDate(),
                dvd.getMpaa(),
                dvd.getDirector(),
                dvd.getStudio(),
                dvd.getNote());
        dvd.setDVDId(jdbcTemplate.queryForObject("select LAST_INSERT_ID()", Integer.class));
    }

    @Override
    public void deleteDVD(int dvdId) {
        jdbcTemplate.update(SQL_DELETE_DVD, dvdId);
    }

    @Override
    public void updateDVD(DVD dvd) {
        jdbcTemplate.update(SQL_UPDATE_DVD,
                dvd.getTitle(),
                dvd.getReleaseDate(),
                dvd.getMpaa(),
                dvd.getDirector(),
                dvd.getStudio(),
                dvd.getNote(),
                dvd.getDVDId());
    }

    @Override
    public DVD getDVDById(int dvdId) {
        try {
            return jdbcTemplate.queryForObject(SQL_SELECT_DVD, new DVDMapper(), dvdId);
        } catch (EmptyResultDataAccessException ex) {
            return null;
        }
    }
    
    @Override
    public List<DVD> getDVDsBySearch(String searchTerm) { 
        try {
            
            List<DVD> list = jdbcTemplate.query(SQL_SELECT_DVDS_SEARCH, new DVDMapper(), "%"+searchTerm+"%", "%"+searchTerm+"%", "%"+searchTerm+"%", "%"+searchTerm+"%", "%"+searchTerm+"%");
            return list;
        } catch (EmptyResultDataAccessException ex) {
            return null;
        }
    }

    @Override
    public List<DVD> getAllDVDs() {
        return jdbcTemplate.query(SQL_SELECT_ALL_DVDS, new DVDMapper());
    }

    private static final class DVDMapper implements RowMapper<DVD> {

        @Override
        public DVD mapRow(ResultSet rs, int i) throws SQLException {
            DVD dvd = new DVD();
            dvd.setTitle(rs.getString("title"));
            dvd.setReleaseDate(rs.getString("releaseDate"));
            dvd.setMpaa(rs.getString("mpaa"));
            dvd.setDirector(rs.getString("director"));
            dvd.setStudio(rs.getString("studio"));
            dvd.setNote(rs.getString("note"));
            dvd.setDVDId(rs.getInt("dvdId"));
            return dvd;
        }
    }

}
