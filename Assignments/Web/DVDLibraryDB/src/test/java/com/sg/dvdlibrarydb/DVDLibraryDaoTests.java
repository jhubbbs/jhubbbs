/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sg.dvdlibrarydb;

import com.sg.dvdlibrarydb.dao.DVDLibraryDaoInterface;
import com.sg.dvdlibrarydb.model.DVD;
import java.util.List;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Test;
import static org.junit.Assert.*;
import org.junit.Before;
import org.junit.BeforeClass;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.jdbc.core.JdbcTemplate;

/**
 *
 * @author apprentice
 */
public class DVDLibraryDaoTests {
    
    DVDLibraryDaoInterface dao;
    
    public DVDLibraryDaoTests() {
    }


    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
        // Ask Spring for my DAO
        ApplicationContext ctx = new ClassPathXmlApplicationContext("test-applicationContext.xml");
        dao = (DVDLibraryDaoInterface) ctx.getBean("dvdLibraryDao");

        // Grab a JdbcTemplate to use for cleaning up
        JdbcTemplate cleaner = (JdbcTemplate) ctx.getBean("jdbcTemplate");
        
        cleaner.execute("delete from dvds");
    }

    @After
    public void tearDown() {
    }

    @Test
    public void addGetUpdateDeleteDVDTest() {

        // Create and Read tests
        DVD dvd = new DVD();
        dvd.setTitle("James");
        dvd.setReleaseDate("Joyce");
        dvd.setMpaa("123 Main");
        dvd.setDirector("Hometown");
        dvd.setStudio("OH");
        dvd.setNote("12345");

        dao.addDVD(dvd);

        DVD fromDb = dao.getDVDById(dvd.getDVDId());

        assertTrue(areEqual(dvd, fromDb));

        // Update Test
        dvd.setTitle("JamesZZ");
        dvd.setReleaseDate("JoyceZZ");
        dvd.setMpaa("123 MainZZ");
        dvd.setDirector("HometownZZ");
        dvd.setStudio("OHZZ");
        dvd.setNote("12345ZZ");

        dao.updateDVD(dvd);
        fromDb = dao.getDVDById(dvd.getDVDId());

        assertTrue(areEqual(dvd, fromDb));

        // Delete Test
        dao.deleteDVD(dvd.getDVDId());

        fromDb = dao.getDVDById(dvd.getDVDId());

        assertNull(fromDb);

    }

    @Test
    public void getAllDVDsTest() {
        DVD dvd = new DVD();
        
        dvd.setTitle("James");
        dvd.setReleaseDate("Joyce");
        dvd.setMpaa("123 Main");
        dvd.setDirector("Hometown");
        dvd.setStudio("OH");
        dvd.setNote("12345");
        dao.addDVD(dvd);

        DVD dvd1 = new DVD();
        dvd1.setTitle("JamesAA");
        dvd1.setReleaseDate("JoyceAA");
        dvd1.setMpaa("123 MainAA");
        dvd1.setDirector("HometownAA");
        dvd1.setStudio("OHAA");
        dvd1.setNote("12345AA");
        dao.addDVD(dvd1);

        DVD dvd2 = new DVD();
        dvd2.setTitle("JamesBB");
        dvd2.setReleaseDate("JoyceBB");
        dvd2.setMpaa("123 MainBB");
        dvd2.setDirector("HometownBB");
        dvd2.setStudio("OHBB");
        dvd2.setNote("12345BB");
        dao.addDVD(dvd2);

        List<DVD> list = dao.getAllDVDs();

        assertEquals(3, list.size());

        DVD fromDb = dao.getDVDById(dvd.getDVDId());
        DVD fromDb1 = dao.getDVDById(dvd1.getDVDId());
        DVD fromDb2 = dao.getDVDById(dvd2.getDVDId());

        assertTrue(areEqual(dvd, fromDb));
        assertTrue(areEqual(dvd1, fromDb1));
        assertTrue(areEqual(dvd2, fromDb2));
    }

    private boolean areEqual(DVD dvd1, DVD dvd2) {
        return dvd1.getDVDId() == dvd2.getDVDId()
                && dvd1.getTitle().equals(dvd2.getTitle())
                && dvd1.getReleaseDate().equals(dvd2.getReleaseDate())
                && dvd1.getMpaa().equals(dvd2.getMpaa())
                && dvd1.getDirector().equals(dvd2.getDirector())
                && dvd1.getStudio().equals(dvd2.getStudio())
                && dvd1.getNote().equals(dvd2.getNote());
    }

    
}
