CREATE DATABASE IF NOT EXISTS `dvdlibrary_test`;

USE `dvdlibrary_test`;

CREATE TABLE IF NOT EXISTS `dvds` (
 `dvdId` int(20) NOT NULL AUTO_INCREMENT,
 `title` varchar(20) NOT NULL,
 `releaseDate` varchar(20) NOT NULL,
 `mpaa` varchar(20) NOT NULL,
 `director` varchar(20) NOT NULL,
 `studio` varchar(20) NOT NULL,
 `note` varchar(50) NOT NULL,
 PRIMARY KEY (`dvdId`)
);

