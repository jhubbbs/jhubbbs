<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Factor Results</title>
    </head>
    <body>
        <h1>Factor Results</h1>


        Your Number: 
        <c:out value="${resultsList.get(0)}"/><br>

        <c:if test="${resultsList.get(2)==true}">Prime Number! </c:if>
        <c:if test="${resultsList.get(2)==false}">Not a Prime Number! </c:if> 
            <br>  

        <c:if test="${resultsList.get(3)==true}">Perfect Number!</c:if>
        <c:if test="${resultsList.get(3)==false}">Not a Perfect Number!</c:if>  
            <br>

        Factors: 
        <c:forEach var="factor" items="${resultsList.get(1)}">

            <c:out value="${factor}"/> 

        </c:forEach>


        <form action="FactorizerServlet" method="GET">

            <input type="submit" value="Again!">
        </form>

    </body>
</html>
