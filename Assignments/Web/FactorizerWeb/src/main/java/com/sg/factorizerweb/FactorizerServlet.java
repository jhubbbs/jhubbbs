/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sg.factorizerweb;

import java.io.IOException;
import java.util.ArrayList;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author apprentice
 */
@WebServlet(name = "FactorizerServlet", urlPatterns = {"/FactorizerServlet"})
public class FactorizerServlet extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        
        RequestDispatcher rd = request.getRequestDispatcher("index.jsp");
        rd.forward(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        int number = Integer.parseInt(request.getParameter("number"));

        int sum = 0;
        ArrayList factors = new ArrayList<>();

        boolean isPrime = false;
        boolean isPerfect = false;

        for (int x = 1; x < number; x++) {
            if (number % x == 0) {
                factors.add(x);
                sum = sum + x;
            }
        }
        
        if (sum == number) {
            isPerfect = true;
        }

        if (factors.size() == 1) {
            isPrime = true;
        }

        ArrayList resultsList = new ArrayList<>();

        resultsList.add(number);
        resultsList.add(factors);
        resultsList.add(isPrime);
        resultsList.add(isPerfect);

        request.setAttribute("resultsList", resultsList);
        RequestDispatcher rd = request.getRequestDispatcher("factorResults.jsp");
        rd.forward(request, response);
    }

    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
