
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Factorizer</title>
    </head>
    <body>
        <h1>Factorizer</h1>
        
        
        <form action="FactorizerServlet" method="POST">
            
            Number to factorize: <input type="number" name="number" min="1" required><br>
            <br>
            <input type="submit" value="Factorize!">
            
        </form>
        
        
    </body>
</html>
