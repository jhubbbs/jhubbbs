/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sg.junitdrillspart1.logic;

/**
 *
 * @author apprentice
 */
public class AlarmClock {

    public String alarmClock(int day, boolean vacation) {
        
        /*
        Given a day of the week encoded as 0=Sun, 1=Mon, 2=Tue, ...6=Sat, 
        and a booleanean indicating if we are on vacation, return a String of 
        the form "7:00" indicating when the alarm clock should ring. Weekdays, 
        the alarm should be "7:00" and on the weekend it should be "10:00". 
        Unless we are on vacation -- then on weekdays it should be "10:00" and 
        weekends it should be "off".
        */
        
        if(vacation == true){
            if(day == 0 || day ==6){
                return "off";
            }else{
                return "10:00";
            }
        }else{
            if(day ==0 || day ==6){
                return "10:00";
            }else{
                return "7:00";
            }
        }
        
        
    }

}
