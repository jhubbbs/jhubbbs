/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sg.junitdrillspart1.logic;

/**
 *
 * @author apprentice
 */
public class InRange {

    public boolean inRange(int n, boolean outsideMode) {

        /*
        Given a number n, return true if n is in the range 1..10, inclusive.
        Unless "outsideMode" is true, in which case return true if the number 
        is less or equal to 1, or greater or equal to 10. 
         */
        if (outsideMode == true) {
            if (n <= 1 || n >= 10) {
                return true;

            }

        } else if (n >= 1 && n <= 10) {
            return true;
        }

        return false;

    }
}