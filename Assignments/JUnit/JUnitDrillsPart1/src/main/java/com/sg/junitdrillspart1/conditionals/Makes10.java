/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sg.junitdrillspart1.conditionals;

/**
 *
 * @author apprentice
 */
public class Makes10 {
    public boolean makes10(int a, int b) {

        /*
        Given two ints, a and b, return true if one if them is 10 or if their sum is 10.
         */
        return a==10||b==10||a+b==10;   
}
}
