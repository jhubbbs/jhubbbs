/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sg.junitdrillspart1.logic;

/**
 *
 * @author apprentice
 */
public class AreInOrder {
    public boolean areInOrder(int a, int b, int c, boolean bOk) {
        
        /*
        Given three ints, a b c, return true if b is greater than a, and 
        c is greater than b. However, with the exception that if "bOk" is true, 
        b does not need to be greater than a. 
        */
        
        if(bOk==true && c>b){
            return true;
        }else if(b > a && c > b){
            return true;
        }
        
        return false;
    }
}
