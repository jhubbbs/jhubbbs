/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sg.junitdrillspart1.logic;

/**
 *
 * @author apprentice
 */
public class AnswerCell {

    public boolean answerCell(boolean isMorning, boolean isMom, boolean isAsleep) {

        /*
        Your cell phone rings. Return true if you should answer it. 
        Normally you answer, except in the morning you only answer if it is your
        mom calling. In all cases, if you are asleep, you do not answer. 
         */
        if (isAsleep == true) {
            return false;
        } else if (isMorning == true && isMom == true) {
            return true;
        } else if (isMorning == true) {
            return false;
        } else {
            return true;
        }

    }

}
