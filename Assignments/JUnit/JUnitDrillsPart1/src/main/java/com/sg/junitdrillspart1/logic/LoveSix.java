/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sg.junitdrillspart1.logic;

/**
 *
 * @author apprentice
 */
public class LoveSix {
    public boolean loveSix(int a, int b) {
        
        /*
        The number 6 is a truly great number. Given two int values, 
        a and b, return true if either one is 6. 
        Or if their sum or difference is 6
        */
        
        if((a==6)||(b==6)||(a+b==6)||(a-b==6)||(b-a==6)){
            return true;
        }else{
            return false;
        }
        
    }
}
