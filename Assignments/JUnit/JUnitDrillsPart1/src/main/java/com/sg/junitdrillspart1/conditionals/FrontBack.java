/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sg.junitdrillspart1.conditionals;

/**
 *
 * @author apprentice
 */
public class FrontBack {

    public String frontBack(String str) {
        /*
    Given a String, return a new String where the first and 
    last chars have been exchanged. 
         */

        char[] n = str.toCharArray();
        
        char first = n[0];
        char last = n[n.length-1];
        n[0]= last;
        n[n.length-1] = first;

        str =new String(n);
        
        return str;
    }
}
