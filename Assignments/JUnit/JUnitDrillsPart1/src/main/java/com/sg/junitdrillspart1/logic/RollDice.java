/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sg.junitdrillspart1.logic;

/**
 *
 * @author apprentice
 */
public class RollDice {
    public int rollDice(int die1, int die2, boolean noDoubles) {
        /*
        Return the sum of two 6-sided dice rolls, each in the range 1..6. 
        However, if noDoubles is true, if the two dice show the same value, 
        increment one die to the next value, wrapping around to 1 if its 
        value was 6. 
        */
        
        if(!noDoubles){
            return die1+die2;
        }else if (noDoubles && die1==die2){
            return die1+die2+1;
        }        
        return die1+die2;
    }
}
