/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sg.junitdrillspart1.logic;

/**
 *
 * @author apprentice
 */
public class TwoIsOne {
    public boolean twoIsOne(int a, int b, int c) {
        
        /*
        Given three ints, a b c, return true if it is possible to add 
        two of the ints to get the third. 
        */
        
        if(a+b==c || b+c==a || a+c==b){
            return true;
        }
        return false;
    }
}
