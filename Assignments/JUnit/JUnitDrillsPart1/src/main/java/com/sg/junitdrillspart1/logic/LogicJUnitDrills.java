/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sg.junitdrillspart1.logic;

/**
 *
 * @author apprentice
 */
public class LogicJUnitDrills {

    public boolean greatParty(int cigars, boolean isWeekend) {
        //ciagrs and weekends
        boolean isGreatParty = false;

        //if cigars is between 40 and 60 inclusive duing the week - great party
        if ((cigars >= 40 && cigars <= 60) && !isWeekend) {
            isGreatParty = true;
        } else if (isWeekend && cigars >= 40) {
            isGreatParty = true;
        }
        return isGreatParty;
        //if it's weekend, we need at least 40 cigars, no upper

    }

    public int canHazTable(int yourStyle, int dateStyle) {
        /*You and your date are trying to get a table at a restaurant. The parameter "you" is the stylishness 
        of your clothes, in the range 0..10, and "date" is the stylishness of your date's clothes. The result 
        getting the table is encoded as an int value with 0=no, 1=maybe, 2=yes. If either of you is very stylish, 
        8 or more, then the result is 2 (yes). With the exception that if either of you has style of 2 or less, 
        then the result is 0 (no). Otherwise the result is 1 (maybe). 

        canHazTable(5, 10) → 2
        canHazTable(5, 2) → 0
        canHazTable(5, 5) → 1
         */
        int result;

        if (yourStyle <= 2 || dateStyle <= 2) {
            result = 0;
        } else if (yourStyle >= 8 || dateStyle >= 8) {
            result = 2;
        } else {
            result = 1;
        }

        return result;
    }

    public boolean playOutside(int temp, boolean isSummer) {
        /*
        The children in Cleveland spend most of the day playing outside. 
        In particular, they play if the temperature is between 60 and 90 (inclusive). 
        Unless it is summer, then the upper limit is 100 instead of 90. 
        Given an int temperature and a boolean isSummer, return true if the children play 
        and false otherwise. 

        playOutside(70, false) → true
        playOutside(95, false) → false
        playOutside(95, true) → true
         */
        boolean result = false;
        
        if(temp >= 60 && temp <= 90){
            result = true;
        }else if((isSummer == true) && (temp >=60 && temp <=100)){
            result = true;
        }
        
        return result;
    }

}
