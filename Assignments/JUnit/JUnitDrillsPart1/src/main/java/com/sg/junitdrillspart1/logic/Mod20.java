/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sg.junitdrillspart1.logic;

/**
 *
 * @author apprentice
 */
public class Mod20 {
    public boolean mod20(int n) {
        /*
        Return true if the given non-negative number is 1 or 2 more than
        a multiple of 20. See also: Introduction to Mod 
         */ 
        
        return n % 20 ==1 || n % 20 ==2;
    }
}
