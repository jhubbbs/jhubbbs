/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sg.junitdrillspart1.logic;

/**
 *
 * @author apprentice
 */
public class Mod35 {
    public boolean mod35(int n) {
        
        /*
        Return true if the given non-negative number is a multiple of 3 or 5, 
        but not both. Use the % "mod" operator
        */
        
        if(n % 3 ==0 ||n%5==0){
            if(n%3==0&&n%5==0){
                return false;
            }else{
                return true;
            }
        }
        return false;
        
    }
}
