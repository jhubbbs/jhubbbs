/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sg.junitdrillspart1.conditionals;

/**
 *
 * @author apprentice
 */
public class SumDouble {
    public int sumDouble(int a, int b) {
  
        /*Given two int values, return their sum. However, if the two values 
are the same, then return double their sum. 
        */
        
        if(a==b){
            return (a+b)*2;
        }else{
            return a+b;
        }
        
        
}
    
    
}
