/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sg.junitdrillspart1.conditionals;

/**
 *
 * @author apprentice
 */
public class NotString {

    public String notString(String s) {
        /*
        Given a String, return a new String where "not " has been added to the
        front. However, if the String already begins with "not", return the String unchanged.
         */

        if (s.length() >= 3) {

            if (s.substring(0, 3).equals("not")) {
                return s;
            }
        } 
            return "not " + s;
        

    }

}
