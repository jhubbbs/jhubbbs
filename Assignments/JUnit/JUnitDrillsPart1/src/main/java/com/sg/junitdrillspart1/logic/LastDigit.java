/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sg.junitdrillspart1.logic;

/**
 *
 * @author apprentice
 */
public class LastDigit {
    public boolean lastDigit(int a, int b, int c){
        
        /*
        Given three ints, a b c, return true if two or more of them have the
        same rightmost digit. The ints are non-negative. 
        */
        
        if(a %10 == b%10 || a%10 ==c%10 || b%10==c%10){
            return true;
        }
        
        return false;
    }
}
