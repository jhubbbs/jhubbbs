/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sg.junitdrillspart1.logic.tests;

import com.sg.junitdrillspart1.logic.LoveSix;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author apprentice
 */
public class LoveSixTests {
    
    public LoveSixTests() {
    }

    // TODO add test methods here.
    // The methods must be annotated with annotation @Test. For example:
    //
    // @Test
    // public void hello() {}
    
    @Test
    public void testLoveSix(){
        LoveSix myLove = new LoveSix();
        
        //loveSix(6, 4) → true
        assertTrue(myLove.loveSix(6,4));
        //loveSix(4, 5) → false
        assertFalse(myLove.loveSix(4, 5));
        //loveSix(1, 5) → true
        assertTrue(myLove.loveSix(1, 5));
        
        
    }
}
