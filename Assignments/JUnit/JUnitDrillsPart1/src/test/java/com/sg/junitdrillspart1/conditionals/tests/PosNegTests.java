/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sg.junitdrillspart1.conditionals.tests;

import com.sg.junitdrillspart1.conditionals.PosNeg;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author apprentice
 */
public class PosNegTests {
    
    public PosNegTests() {
    }

    // TODO add test methods here.
    // The methods must be annotated with annotation @Test. For example:
    //
    // @Test
    // public void hello() {}
    @Test
    public void testPosNeg(){
        PosNeg pn = new PosNeg();
        
        /*
        posNeg(1, -1, false) -> true
posNeg(-1, 1, false) -> true
posNeg(-4, -5, true) -> true
        */
        
        
        assertTrue(pn.posNeg(1, -1, false));
        assertTrue(pn.posNeg(-1, 1, false));
        assertTrue(pn.posNeg(-4,-5,true));
        
        
}
}
