/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sg.junitdrillspart1.logic.tests;

import com.sg.junitdrillspart1.logic.SpecialEleven;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author apprentice
 */
public class SpecialElevenTests {
    
    public SpecialElevenTests() {
    }

    // TODO add test methods here.
    // The methods must be annotated with annotation @Test. For example:
    //
    // @Test
    // public void hello() {}
    @Test
    public void testSpecialEleven(){
        SpecialEleven mySpecial = new SpecialEleven();
        
        //specialEleven(22) → true
        assertTrue(mySpecial.specialEleven(22));
        
        //specialEleven(23) → true
        assertTrue(mySpecial.specialEleven(23));
        
        //specialEleven(24) → false
        assertFalse(mySpecial.specialEleven(24));
        
    }
}
