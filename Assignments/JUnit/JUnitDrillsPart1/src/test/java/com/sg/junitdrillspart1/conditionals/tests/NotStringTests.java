/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sg.junitdrillspart1.conditionals.tests;

import com.sg.junitdrillspart1.conditionals.NotString;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author apprentice
 */
public class NotStringTests {
    
    public NotStringTests() {
    }

    // TODO add test methods here.
    // The methods must be annotated with annotation @Test. For example:
    //
    // @Test
    // public void hello() {}
    @Test
    public void testNotString(){
        NotString ns = new NotString();
        /*
        notString("candy") -> "not candy"
notString("x") -> "not x"
notString("not bad") -> "not bad"
        */
        
        assertEquals("not candy", ns.notString("candy"));
        assertEquals("not x", ns.notString("x"));
        assertEquals("not bad", ns.notString("not bad"));
        
        
        
        
        
        
        
        
    }
}
