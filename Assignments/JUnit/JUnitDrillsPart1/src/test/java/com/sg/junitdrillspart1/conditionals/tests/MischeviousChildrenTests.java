/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sg.junitdrillspart1.conditionals.tests;

import com.sg.junitdrillspart1.conditionals.MischeviousChildren;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author apprentice
 */
public class MischeviousChildrenTests {

    public MischeviousChildrenTests() {
    }

    @Test
    public void testMischeviousChildren() {

        MischeviousChildren mc = new MischeviousChildren();

        /*
        
        areWeInTrouble(true, true) -> true
areWeInTrouble(false, false) -> true
areWeInTrouble(true, false) -> false

         */
        
        
        assertTrue(mc.areWeInTrouble(true, true));
        assertTrue(mc.areWeInTrouble(false, false));
        assertFalse(mc.areWeInTrouble(true, false));
        
        
        
    }
    // TODO add test methods here.
    // The methods must be annotated with annotation @Test. For example:
    //
    // @Test
    // public void hello() {}
}
