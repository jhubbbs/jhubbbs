/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sg.junitdrillspart1.logic.tests;

import com.sg.junitdrillspart1.logic.LastDigit;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author apprentice
 */
public class LastDigitTests {
    
    public LastDigitTests() {
    }

    // TODO add test methods here.
    // The methods must be annotated with annotation @Test. For example:
    //
    // @Test
    // public void hello() {}
    @Test
    public void testLastDigit(){
        LastDigit myLast = new LastDigit();
        
        //lastDigit(23, 19, 13) → true
        assertTrue(myLast.lastDigit(23, 19, 13));
        //lastDigit(23, 19, 12) → false
        assertFalse(myLast.lastDigit(23, 19, 12));
        //lastDigit(23, 19, 3) → true
        assertTrue(myLast.lastDigit(23,19,3));
        
    }
}
