/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sg.junitdrillspart1.logic.tests;

import com.sg.junitdrillspart1.logic.Mod35;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author apprentice
 */
public class Mod35Tests {
    
    public Mod35Tests() {
    }

    // TODO add test methods here.
    // The methods must be annotated with annotation @Test. For example:
    //
    // @Test
    // public void hello() {}
    @Test
    public void testMod35(){
        Mod35 myMod = new Mod35();
        //mod35(3) → true
        assertTrue(myMod.mod35(3));
        //mod35(10) → true
        assertTrue(myMod.mod35(10));
        //mod35(15) → false
        assertFalse(myMod.mod35(15));
        
    }
    
    
}
