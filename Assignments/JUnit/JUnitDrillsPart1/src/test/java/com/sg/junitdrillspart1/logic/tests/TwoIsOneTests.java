/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sg.junitdrillspart1.logic.tests;

import com.sg.junitdrillspart1.logic.TwoIsOne;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author apprentice
 */
public class TwoIsOneTests {
    
    public TwoIsOneTests() {
    }

    // TODO add test methods here.
    // The methods must be annotated with annotation @Test. For example:
    //
    // @Test
    // public void hello() {}
    @Test
    public void testTwoIsOne(){
        TwoIsOne myTwo = new TwoIsOne();
        
        //twoIsOne(1, 2, 3) → true
        assertTrue(myTwo.twoIsOne(1, 2, 3));
        //twoIsOne(3, 1, 2) → true
        assertTrue(myTwo.twoIsOne(3, 1, 2));
        //twoIsOne(3, 2, 2) → false
        assertFalse(myTwo.twoIsOne(3, 2, 2));


    }
}
