/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sg.junitdrillspart1.conditionals.tests;

import com.sg.junitdrillspart1.conditionals.ParrotTrouble;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author apprentice
 */
public class ParrotTroubleTest {
    
    public ParrotTroubleTest() {
    }

    // TODO add test methods here.
    // The methods must be annotated with annotation @Test. For example:
    //
    // @Test
    // public void hello() {}
    
    @Test
    public void testParrotTrouble(){
        
        /*
        parrotTrouble(true, 6) -> true
parrotTrouble(true, 7) -> false
parrotTrouble(false, 6) -> false
        */
        ParrotTrouble pt = new ParrotTrouble();
            
            
            assertTrue(pt.parrotTrouble(true, 6));
            assertFalse(pt.parrotTrouble(true, 7));
            assertFalse(pt.parrotTrouble(false, 6));
            
        }
        
        
    }

    
    
