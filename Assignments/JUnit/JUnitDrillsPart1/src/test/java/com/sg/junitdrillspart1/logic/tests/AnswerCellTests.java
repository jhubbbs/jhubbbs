/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sg.junitdrillspart1.logic.tests;

import com.sg.junitdrillspart1.logic.AnswerCell;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author apprentice
 */
public class AnswerCellTests {
    
    public AnswerCellTests() {
    }

    // TODO add test methods here.
    // The methods must be annotated with annotation @Test. For example:
    //
    // @Test
    // public void hello() {}
    @Test
    public void testAnswerCall(){
        AnswerCell myAnswer = new AnswerCell();
        
        //answerCell(false, false, false) → true
        assertTrue(myAnswer.answerCell(false, false, false));
        
        //answerCell(false, false, true) → false
        assertFalse(myAnswer.answerCell(false, false, true));
        
        //answerCell(true, false, false) → false
        assertFalse(myAnswer.answerCell(true, false, false));

        
        
    }
}
