/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sg.junitdrillspart1.conditionals.tests;

import com.sg.junitdrillspart1.conditionals.NearHundred;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author apprentice
 */
public class NearHundredTests {
    
    public NearHundredTests() {
    }

    // TODO add test methods here.
    // The methods must be annotated with annotation @Test. For example:
    //
    // @Test
    // public void hello() {}
    @Test
    public void testNearHundred(){
        /*
        nearHundred(103) -> true
nearHundred(90) -> true
nearHundred(89) -> false

        */
        NearHundred nh = new NearHundred();
        assertTrue(nh.nearHundred(103));
        assertTrue(nh.nearHundred(90));
        assertFalse(nh.nearHundred(89));
        
        
        
        
    }
}
