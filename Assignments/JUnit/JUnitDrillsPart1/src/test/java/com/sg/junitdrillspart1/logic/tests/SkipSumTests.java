/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sg.junitdrillspart1.logic.tests;

import com.sg.junitdrillspart1.logic.SkipSum;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author apprentice
 */
public class SkipSumTests {

    public SkipSumTests() {
    }

    // TODO add test methods here.
    // The methods must be annotated with annotation @Test. For example:
    //
    // @Test
    // public void hello() {}
    @Test
    public void testSkipSum() {
        


        SkipSum mySkip = new SkipSum();
        
        //skipSum(3, 4) → 7
        assertEquals(mySkip.skipSum(3, 4), 7);
        
        //skipSum(9, 4) → 20
        assertEquals(mySkip.skipSum(9, 4), 20);
        
        //skipSum(10, 11) → 21
        assertEquals(mySkip.skipSum(10, 11), 21);
    }

}
