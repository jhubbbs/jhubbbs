/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sg.junitdrillspart1.logic.tests;

import com.sg.junitdrillspart1.logic.RollDice;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author apprentice
 */
public class RollDiceTests {
    
    public RollDiceTests() {
    }

    // TODO add test methods here.
    // The methods must be annotated with annotation @Test. For example:
    //
    // @Test
    // public void hello() {}
    @Test
    public void testRollDice(){
        RollDice myRoll = new RollDice();
        
        //rollDice(2, 3, true) → 5
        assertEquals(myRoll.rollDice(2, 3, true), 5);
        
        //rollDice(3, 3, true) → 7
        assertEquals(myRoll.rollDice(3, 3, true), 7);
        //rollDice(3, 3, false) → 6
        assertEquals(myRoll.rollDice(3, 3, false), 6);
        
        
        
    }
}
