/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sg.junitdrillspart1.logic.tests;

import com.sg.junitdrillspart1.logic.InRange;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author apprentice
 */
public class InRangeTests {
    
    public InRangeTests() {
    }

    // TODO add test methods here.
    // The methods must be annotated with annotation @Test. For example:
    //
    // @Test
    // public void hello() {}
    @Test
    public void testInRange(){
        
        InRange myIn = new InRange();
        
        //inRange(5, false) → true
        assertTrue(myIn.inRange(5, false));
        //inRange(11, false) → false
        assertFalse(myIn.inRange(11, false));
        //inRange(11, true) → true
        assertTrue(myIn.inRange(11, true));
    }
}
