/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sg.junitdrillspart1.logic.tests;

import com.sg.junitdrillspart1.logic.AreInOrder;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author apprentice
 */
public class AreInOrderTests {
    
    public AreInOrderTests() {
    }

    // TODO add test methods here.
    // The methods must be annotated with annotation @Test. For example:
    //
    // @Test
    // public void hello() {}
    @Test
    public void testAreInOrder(){
        AreInOrder myAre = new AreInOrder();
        
        //areInOrder(1, 2, 4, false) → true
        assertTrue(myAre.areInOrder(1, 2, 4, false));
        
        //areInOrder(1, 2, 1, false) → false
        assertFalse(myAre.areInOrder(1,2,1,false));
        
        //areInOrder(1, 1, 2, true) → true
        assertTrue(myAre.areInOrder(1, 1, 2, true));
        
        
        
    }
}
