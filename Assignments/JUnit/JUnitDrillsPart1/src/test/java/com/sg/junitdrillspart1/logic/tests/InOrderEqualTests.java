/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sg.junitdrillspart1.logic.tests;

import com.sg.junitdrillspart1.logic.InOrderEqual;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author apprentice
 */
public class InOrderEqualTests {
    
    public InOrderEqualTests() {
    }

    // TODO add test methods here.
    // The methods must be annotated with annotation @Test. For example:
    //
    // @Test
    // public void hello() {}
    @Test
    public void testInOrderEqual(){
        InOrderEqual myIn = new InOrderEqual();
        
        //inOrderEqual(2, 5, 11, false) → true
        assertTrue(myIn.inOrderEqual(2, 5, 11, false));
        //inOrderEqual(5, 7, 6, false) → false
        assertFalse(myIn.inOrderEqual(5, 7, 6, false));
        //inOrderEqual(5, 5, 7, true) → true
        assertTrue(myIn.inOrderEqual(5, 5, 7, true));
    }
}
