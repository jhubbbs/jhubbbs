/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sg.junitdrillspart1.conditionals.tests;

import com.sg.junitdrillspart1.conditionals.SleepingIn;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author apprentice
 */
public class SleepingInTests {
    
    public SleepingInTests() {
    }

    // TODO add test methods here.
    // The methods must be annotated with annotation @Test. For example:
    //
    // @Test
    // public void hello() {}
    @Test
    public void testSleepingIn(){
        /*anSleepIn(false, false) -> true
canSleepIn(true, false) -> false
canSleepIn(false, true) -> true*/
        
        SleepingIn si = new SleepingIn();
        
        assertTrue(si.canSleepIn(false, false));
        assertFalse(si.canSleepIn(true, false));
        assertTrue(si.canSleepIn(false, true));
        
    }
}
