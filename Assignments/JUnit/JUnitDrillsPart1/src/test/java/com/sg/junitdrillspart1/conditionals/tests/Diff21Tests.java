/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sg.junitdrillspart1.conditionals.tests;

import com.sg.junitdrillspart1.conditionals.Diff21;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author apprentice
 */
public class Diff21Tests {
    
    public Diff21Tests() {
    }
    
    @Test
    public void testDiff21(){
        
        Diff21 d = new Diff21();
        
        /*
        diff21(23) -> 4
diff21(10) -> 11
diff21(21) -> 0
        */
        
        assertEquals(4, d.diff21(23));
        assertEquals(11, d.diff21(10));
        assertEquals(0, d.diff21(21));
        
    }

    // TODO add test methods here.
    // The methods must be annotated with annotation @Test. For example:
    //
    // @Test
    // public void hello() {}
}
