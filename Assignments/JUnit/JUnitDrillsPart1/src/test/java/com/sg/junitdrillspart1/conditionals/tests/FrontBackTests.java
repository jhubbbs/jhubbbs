/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sg.junitdrillspart1.conditionals.tests;

import com.sg.junitdrillspart1.conditionals.FrontBack;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author apprentice
 */
public class FrontBackTests {
    
    public FrontBackTests() {
    }

    // TODO add test methods here.
    // The methods must be annotated with annotation @Test. For example:
    //
    // @Test
    // public void hello() {}
    @Test
    public void testFrontBack(){
        
        /*
        
frontBack("code") -> "eodc"
frontBack("a") -> "a"
frontBack("ab") -> "ba"

        */
        FrontBack fb = new FrontBack();
        
        assertEquals("eodc", fb.frontBack("code"));
        assertEquals("a", fb.frontBack("a"));
        assertEquals("ba", fb.frontBack("ab"));
        
        
    }
}
