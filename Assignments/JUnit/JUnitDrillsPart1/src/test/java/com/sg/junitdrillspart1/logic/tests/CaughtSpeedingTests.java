/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sg.junitdrillspart1.logic.tests;

import com.sg.junitdrillspart1.logic.CaughtSpeeding;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author apprentice
 */
public class CaughtSpeedingTests {
    
    public CaughtSpeedingTests() {
    }

    // TODO add test methods here.
    // The methods must be annotated with annotation @Test. For example:
    //
    // @Test
    // public void hello() {}
    
    @Test
    public void testCaughtSpeeding(){
        /*
        
        
        
        */
        
        CaughtSpeeding myCaught = new CaughtSpeeding();
        
        //caughtSpeeding(60, false) → 0
        assertEquals(myCaught.caughtSpeeding(60, false), 0);
        
        //caughtSpeeding(65, false) → 1
        assertEquals(myCaught.caughtSpeeding(65, false), 1);
        
        //caughtSpeeding(65, true) → 0
        assertEquals(myCaught.caughtSpeeding(65, true), 0);
    }
    
    
}
