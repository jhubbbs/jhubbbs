/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sg.junitdrillspart1.logic.tests;

import com.sg.junitdrillspart1.logic.Mod20;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author apprentice
 */
public class Mod20Tests {
    
    public Mod20Tests() {
    }

    // TODO add test methods here.
    // The methods must be annotated with annotation @Test. For example:
    //
    // @Test
    // public void hello() {}
    @Test
    public void testMod20(){
        Mod20 myMod = new Mod20();
        
        //mod20(20) → false
        assertFalse(myMod.mod20(20));
        //mod20(21) → true
        assertTrue(myMod.mod20(21));
        //mod20(22) → true
        assertTrue(myMod.mod20(22));
        
    }
}
