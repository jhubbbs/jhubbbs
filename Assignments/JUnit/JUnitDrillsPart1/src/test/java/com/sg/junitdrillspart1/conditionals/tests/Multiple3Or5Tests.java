/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sg.junitdrillspart1.conditionals.tests;

import org.junit.Test;
import static org.junit.Assert.*;
import com.sg.junitdrillspart1.conditionals.Multiple3or5;

/**
 *
 * @author apprentice
 */
public class Multiple3Or5Tests {

    public Multiple3Or5Tests() {
    }
    @Test
    public void testMultiple3Or5() {
        
        Multiple3or5 myMultiples = new Multiple3or5();
        
        
        //multiple3or5(3) -> true
        assertTrue(myMultiples.multiple3or5(3));
        //multiple3or5(10) -> true
        assertTrue(myMultiples.multiple3or5(10));
        //multiple3or5(8) -> false
        assertFalse(myMultiples.multiple3or5(8));
    }

}
