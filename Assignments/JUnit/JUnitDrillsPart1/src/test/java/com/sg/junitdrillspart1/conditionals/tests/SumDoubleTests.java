/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sg.junitdrillspart1.conditionals.tests;

import com.sg.junitdrillspart1.conditionals.SumDouble;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author apprentice
 */
public class SumDoubleTests {
    
    public SumDoubleTests() {
    }

    // TODO add test methods here.
    // The methods must be annotated with annotation @Test. For example:
    //
    // @Test
    // public void hello() {}
    
    @Test
    public void testSumDouble(){
        /*
        sumDouble(1, 2) -> 3
sumDouble(3, 2) -> 5
sumDouble(2, 2) -> 8
        */
        
        SumDouble sd = new SumDouble();
        
        
        assertEquals(3, sd.sumDouble(1, 2));
        assertEquals(5,sd.sumDouble(3,2));
        assertEquals(8, sd.sumDouble(2, 2));
        
        
    }
}
