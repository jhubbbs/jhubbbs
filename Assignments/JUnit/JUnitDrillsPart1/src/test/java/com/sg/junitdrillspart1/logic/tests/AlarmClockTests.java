/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sg.junitdrillspart1.logic.tests;

import com.sg.junitdrillspart1.logic.AlarmClock;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author apprentice
 */
public class AlarmClockTests {
    
    public AlarmClockTests() {
    }

    // TODO add test methods here.
    // The methods must be annotated with annotation @Test. For example:
    //
    // @Test
    // public void hello() {}
    
        @Test
        public void testAlarmClock(){
            

            AlarmClock myAlarm = new AlarmClock();
            
            //alarmClock(1, false) → "7:00"
            
            assertEquals(myAlarm.alarmClock(1, false),"7:00" );
            
            //alarmClock(5, false) → "7:00"
            assertEquals(myAlarm.alarmClock(5, false), "7:00");
            
            //alarmClock(0, false) → "10:00"
            assertEquals(myAlarm.alarmClock(0, false), "10:00");
            
            
        }
    
    
    
}
