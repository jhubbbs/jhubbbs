/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sg.junitdrillspart1.logic.tests;

import com.sg.junitdrillspart1.logic.LogicJUnitDrills;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author apprentice
 */
public class LogicJUnitDrillsTests {

    public LogicJUnitDrillsTests() {
    }
    LogicJUnitDrills myParty = new LogicJUnitDrills();
    LogicJUnitDrills myTable = new LogicJUnitDrills();

    //step 1 - add the tests
    // right click the Source Packages
    // new ... other
    // Category: unit tests, file: junit tests
    //in dropdown, make sure test packages is selected
    @Test
    public void testGreatParty() {
        //Step2 - copy the test cases from the Drills file
        /*greatParty(30, false) → false
        greatParty(50, false) → true
        greatParty(70, true) → true
         */
        //Step 3 - do the Arrange - Act - Assert for the first case

        // Arrange - set conditions
        int numOfCigars = 30;
        boolean isWeekend = false;
        boolean expectedResult = false;

        //Act - get the actual result
        boolean actualResult = myParty.greatParty(numOfCigars, isWeekend);

        //Assert - make sure actual and expected match
        assertEquals(expectedResult, actualResult);

    }

    @Test
    public void testGreatParty2() {

        boolean result;
        result = myParty.greatParty(50, false);
        //greatParty(50, false) → true
        assertTrue(result);

        result = myParty.greatParty(70, true);
        assertTrue(result);
        //greatParty(70, true) → true

        assertFalse(myParty.greatParty(30, false));
        //greatParty(30, false) → false
    }

    @Test
    public void testCanHazTable() {
        /*
        canHazTable(5, 10) → 2
        canHazTable(5, 2) → 0
        canHazTable(5, 5) → 1
         */
        int result;
        int expected;

        //canHazTable(5, 10) → 2
        result = myTable.canHazTable(5, 10);
        expected = 2;
        assertEquals(expected, result);

        //canHazTable(5, 2) → 0
        result = myTable.canHazTable(5, 2);
        expected = 0;
        assertEquals(expected, result);

        //canHazTable(5, 5) → 1
        result = myTable.canHazTable(5, 5);
        expected = 1;
        assertEquals(expected, result);
    }
    
    @Test
    public void testPlayOutside(){
        LogicJUnitDrills myOutside = new LogicJUnitDrills();
        
        /*
        playOutside(70, false) → true
        playOutside(95, false) → false
        playOutside(95, true) → true
        */
        boolean result;
        
        
        result = myOutside.playOutside(70, false);
        assertTrue(result);
        
        result = myOutside.playOutside(95, false);
        assertFalse(result);
        
        result = myOutside.playOutside(95, true);
        assertTrue(result);
        
    }

}
