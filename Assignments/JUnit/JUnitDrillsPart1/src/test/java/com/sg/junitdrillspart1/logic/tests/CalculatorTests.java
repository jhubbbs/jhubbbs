/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sg.junitdrillspart1.logic.tests;

import com.sg.junitdrillspart1.logic.Calculator;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author apprentice
 */
public class CalculatorTests {
    
    public CalculatorTests() {
    }

    // TODO add test methods here.
    // The methods must be annotated with annotation @Test. For example:
    //
    // @Test
    // public void hello() {}
    @Test
    public void testAdd(){
        //arrange act assert
        
        int num1 = 2;
        int num2 = 2;
        int expected = 4;
        
        int actual = Calculator.add(num1, num2);
        
        assertEquals(expected, actual);
    }
}
