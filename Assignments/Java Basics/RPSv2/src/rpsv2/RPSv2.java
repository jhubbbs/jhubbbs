/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package rpsv2;

import java.util.Random;
import java.util.Scanner;

/**
 *
 * @author apprentice
 */
public class RPSv2 {

    
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        String stringPick;
        int pick;
        int chosenRounds;
        String stringChosenRounds;
        
        do{
            System.out.println("Select a number of rounds to play (1-10): ");
            stringChosenRounds = sc.nextLine();
            chosenRounds = Integer.parseInt(stringChosenRounds);
        }while(chosenRounds < 1 || chosenRounds > 10);
        
        int roundCount = 0;
        
        do{
            do{
                roundCount++;
                System.out.println("Round #"+roundCount);
                System.out.println("Enter: \n1 for Rock\n2 for Paper\n3 for Scissors");
                stringPick = sc.nextLine();
                pick = Integer.parseInt(stringPick);
            }while((pick != 1)&&(pick != 2) && (pick != 3));

            Random rGen = new Random();
            int compPick = rGen.nextInt(2)+1;
            String stringCompPick;

            if(compPick == 1){
                stringCompPick = "Rock";
            } else if (compPick == 2){
                stringCompPick = "Paper";
            } else{
                stringCompPick = "Scissors";
            }

            System.out.println("The computer chose: "+stringCompPick);

            if(pick ==1){
                if(compPick==1){
                    System.out.println("Tie");
                } else if(compPick==2){
                    System.out.println("Lose");
                } else{
                    System.out.println("Win");
                }
            } else if(pick == 2){
                if(compPick==1){
                    System.out.println("Win");
                } else if(compPick==2){
                    System.out.println("Tie");
                } else{
                    System.out.println("Lose");
                }
            }else{
                if(compPick==1){
                    System.out.println("Lose");
                } else if(compPick==2){
                    System.out.println("Win");
                } else{
                    System.out.println("Tie");
                }
            }
            
        }while(roundCount < chosenRounds);
    }
    
}
