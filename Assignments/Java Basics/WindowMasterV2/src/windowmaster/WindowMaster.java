/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package windowmaster;

import java.util.Scanner;

/**
 *
 * @author apprentice
 */
public class WindowMaster {
    
    public static void main(String[] args) {
        
        Scanner sc = new Scanner(System.in);
        
        String length;
        String width;
        
        System.out.println("Please enter the width:");
        width = sc.nextLine();
        float floatWidth = Float.parseFloat(width);
        
        System.out.println("Please enter the length");
        length = sc.nextLine();
        float floatLength = Float.parseFloat(length);
        
        System.out.println("Please enter the glass cost per square inch:");
        String glass = sc.nextLine();
        float glassRate = Float.parseFloat(glass);
        
        System.out.println("Please enter the trim cost per inch:");
        String trim = sc.nextLine();
        float trimRate = Float.parseFloat(trim);
        
        
        float area = floatWidth * floatLength;
        float perimeter = (floatLength * 2) + (floatWidth * 2);
        
        //use f on numbers to tell compiler to treat them as floats
       
        float glassCost = glassRate * area;
        float trimCost = trimRate * perimeter;
        
        System.out.println("Window height = " +length);
        System.out.println("Window width = " + width);
        System.out.println("Window area = " +area);
        System.out.println("Window perimeter = " + perimeter);
        System.out.println("Glass cost = " + glassCost);
        System.out.println("Trim cost = " + trimCost);
        System.out.println("Total cost = " +(glassCost + trimCost));
        
        
    }
    
}
