/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package luckysevens;

import java.util.Random;
import java.util.Scanner;

/**
 *
 * @author apprentice
 */
public class LuckySevens {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        
        Scanner sc = new Scanner(System.in);
        String stringStarting;
        int starting;
        
        do{
            System.out.println("Starting bet? Must be > 0");
            stringStarting = sc.nextLine();
            starting = Integer.parseInt(stringStarting);
        } while(starting <= 0);
        
        int balance = starting;
        int rollCount = 0;
        int max = starting;
        int maxRoll = 1;
        
        Random rGen = new Random();
        int d1;
        int d2;
        
        do{
            rollCount++;
            
            d1 = rGen.nextInt(5)+1;
            d2 = rGen.nextInt(5)+1;
            
            if(d1+d2==7){
                balance = balance + 4;
            } else{
                balance--;
            }
            
            if(balance > max){
                max = balance;
                maxRoll = rollCount;
            }
            
        } while(balance > 0);
        
        System.out.println("Number of rolls: "+rollCount);
        System.out.println("Highest balance: "+max);
        System.out.println("# of rolls at highest balance: "+maxRoll);
        
    }
    
}
