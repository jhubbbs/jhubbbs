/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package interest.calculator;

import java.util.Scanner;

/**
 *
 * @author apprentice
 */
public class InterestCalculator {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        
        System.out.println("Starting balance?");
        String stringStarting = sc.nextLine();
        float starting = Integer.parseInt(stringStarting);
        
        System.out.println("Annual rate?");
        String stringAnnual = sc.nextLine();
        float annual = Integer.parseInt(stringAnnual);
        
        System.out.println("Number of years?");
        String stringYears = sc.nextLine();
        float years = Integer.parseInt(stringYears);
        
        float balance = starting;
        float quarterly = annual / 4;
        float interest;
        float tempBalance = balance;
        
        for(int currentYear = 1; currentYear <= years; currentYear++){
            
            System.out.println("Year: "+currentYear);
            System.out.println("Starting balance: "+balance);
            
            tempBalance = tempBalance * (1+ (quarterly / 100));
            tempBalance = tempBalance * (1+ (quarterly / 100));
            tempBalance = tempBalance * (1+ (quarterly / 100));
            tempBalance = tempBalance * (1+ (quarterly / 100));
            
            interest = tempBalance - balance;
            balance = tempBalance;
            
            System.out.println("Interest earned: "+interest);
            System.out.println("Ending balance :"+balance);
            
            
        }
        
        
    }
    
}
