/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package rpsv3;

import java.util.Random;
import java.util.Scanner;

/**
 *
 * @author apprentice
 */
public class RPSv3 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        String stringPick;
        int pick;
        int chosenRounds;
        String stringChosenRounds;
        int winCount = 0;
        int loseCount = 0;
        int tieCount = 0;
        int roundCount = 0;
        
        
        do{
            System.out.println("Select a number of rounds to play (1-10): ");
            stringChosenRounds = sc.nextLine();
            chosenRounds = Integer.parseInt(stringChosenRounds);
        }while(chosenRounds < 1 || chosenRounds > 10);
        
        
        
        do{
            roundCount++;
            do{
                
                System.out.println("Round #"+roundCount);
                System.out.println("Enter: \n1 for Rock\n2 for Paper\n3 for Scissors");
                stringPick = sc.nextLine();
                
            }while(!(stringPick.equals("1"))&&!(stringPick.equals("2")) && !(stringPick.equals("3")));
            
            pick = Integer.parseInt(stringPick);
            Random rGen = new Random();
            int compPick = rGen.nextInt(2)+1;
            String stringCompPick;

            if(compPick == 1){
                stringCompPick = "Rock";
            } else if (compPick == 2){
                stringCompPick = "Paper";
            } else{
                stringCompPick = "Scissors";
            }

            System.out.println("The computer chose: "+stringCompPick);

            if(pick ==1){
                if(compPick==1){
                    System.out.println("Tie");
                    tieCount++;
                } else if(compPick==2){
                    System.out.println("Lose");
                    loseCount++;
                } else{
                    System.out.println("Win");
                    winCount++;
                }
            } else if(pick == 2){
                if(compPick==1){
                    System.out.println("Win");
                    winCount++;
                } else if(compPick==2){
                    System.out.println("Tie");
                    tieCount++;
                } else{
                    System.out.println("Lose");
                    loseCount++;
                }
            }else{
                if(compPick==1){
                    System.out.println("Lose");
                    loseCount++;
                } else if(compPick==2){
                    System.out.println("Win");
                    winCount++;
                } else{
                    System.out.println("Tie");
                    tieCount++;
                }
            }
            
        }while(roundCount < chosenRounds);
        System.out.println("Total wins: "+winCount);
        System.out.println("Total ties: "+tieCount);
        System.out.println("Total losses: "+loseCount);
        
        if(winCount > loseCount){
            System.out.println("You're the winner!");
        } else if(loseCount > winCount){
            System.out.println("The computer wins!");
        } else{
            System.out.println("It's a tie!");
        }
    }
    
}
