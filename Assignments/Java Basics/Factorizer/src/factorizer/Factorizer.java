/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package factorizer;

import java.util.Scanner;

/**
 *
 * @author apprentice
 */
public class Factorizer {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        
        System.out.println("What number would you like to investigate?");
        String stringNumber= sc.nextLine();
        int number = Integer.parseInt(stringNumber);
        
        
        
        int sum = 0;
        int factorCount = 0;
        
        System.out.println("The factors of "+number+" are:");
        for(int x = 1; x < number; x++){
            if (number % x == 0){
                System.out.println(x);
                sum = sum + x;
                factorCount++;
            }
        }
        if(sum == number){
            System.out.println(number+" is a perfect number!");
        } else{
            System.out.println(number+" is not a perfect number...");
        }
        if(factorCount==1){
            System.out.println(number+" is a prime number!");
        } else{
            System.out.println(number+" is not a prime number...");
        }
    }
    
}
