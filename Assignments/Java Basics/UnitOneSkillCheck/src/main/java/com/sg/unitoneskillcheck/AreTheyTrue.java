/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sg.unitoneskillcheck;

/**
 *
 * @author apprentice
 */
public class AreTheyTrue {
    public static void main(String[] args) {
        
        AreTheyTrue test = new AreTheyTrue();
        
        System.out.println(test.howTrue(true, true));
        System.out.println(test.howTrue(false, false));
        System.out.println(test.howTrue(true, false));
        System.out.println(test.howTrue(false, true));
    }
    private String howTrue(boolean one, boolean two){
          
        if(one==true && two==true){
            return "BOTH";
        } else if(one==true || two==true){
            return "ONLY ONE";
        }else{
            return "NEITHER";
        }
        
        
    }
}
