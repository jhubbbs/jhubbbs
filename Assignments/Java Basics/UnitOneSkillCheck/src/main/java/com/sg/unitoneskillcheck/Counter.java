/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sg.unitoneskillcheck;

/**
 *
 * @author apprentice
 */
public class Counter {
    public static void main(String[] args) {
        toTen();
        
        toN(3);
        toN(8);
        toN(200);
        
    }
    public static void toTen(){
        for(int x=1;x<=10; x++){
            System.out.println(x);
        }
    }
    public static void toN(int n){
        for(int x = 1; x <= n; x++){
            System.out.println(x);
        }
    }
}
