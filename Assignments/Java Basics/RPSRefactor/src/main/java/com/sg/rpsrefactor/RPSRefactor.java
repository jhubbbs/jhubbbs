/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sg.rpsrefactor;

import java.util.Random;
import java.util.Scanner;

/**
 *
 * @author apprentice
 */
public class RPSRefactor {

    public static void main(String[] args) {
        String playAgain;
        Scanner sc = new Scanner(System.in);

        do {
            playSet();
            System.out.println("Enter Y to play again or anything else to exit");
            playAgain = sc.nextLine();
        } while (playAgain.equals("Y") || playAgain.equals("y"));

        System.out.println("Thanks for playing!");

    }

    public static void playSet() {
        int winCount = 0;
        int loseCount = 0;
        int tieCount = 0;
        int roundCount = 0;
        String stringChosenRounds;
        int chosenRounds;
        Scanner sc = new Scanner(System.in);
        String roundResult;

        do {
            System.out.println("Select a number of rounds to play (1-10): ");
            stringChosenRounds = sc.nextLine();
            chosenRounds = Integer.parseInt(stringChosenRounds);
        } while (chosenRounds < 1 || chosenRounds > 10);

        do {
            roundCount++;
            roundResult = playRound(roundCount);
            if (roundResult.equals("won")) {
                winCount++;
            } else if (roundResult.equals("tied")) {
                tieCount++;
            } else {
                loseCount++;
            }
        } while (roundCount < chosenRounds);

        postResults(winCount, tieCount, loseCount);

    }

    public static String playRound(int roundCount) {
        Scanner sc = new Scanner(System.in);
        Random rGen = new Random();
        String stringPick;
        int pick;

        do {

            System.out.println("Round #" + roundCount);
            System.out.println("Enter: \n1 for Rock\n2 for Paper\n3 for Scissors");
            stringPick = sc.nextLine();

        } while (!(stringPick.equals("1")) && !(stringPick.equals("2")) && !(stringPick.equals("3")));

        pick = Integer.parseInt(stringPick);

        int compPick = rGen.nextInt(3) + 1;
        String stringCompPick;

        if (compPick == 1) {
            stringCompPick = "Rock";
        } else if (compPick == 2) {
            stringCompPick = "Paper";
        } else {
            stringCompPick = "Scissors";
        }

        System.out.println("The computer chose: " + stringCompPick);

        if (pick == 1) {
            if (compPick == 1) {
                System.out.println("Tie");
                return "tied";
            } else if (compPick == 2) {
                System.out.println("Lose");
                return "lost";
            } else {
                System.out.println("Win");
                return "won";
            }
        } else if (pick == 2) {
            if (compPick == 1) {
                System.out.println("Win");
                return "won";
            } else if (compPick == 2) {
                System.out.println("Tie");
                return "tied";
            } else {
                System.out.println("Lose");
                return "lost";
            }
        } else if (compPick == 1) {
            System.out.println("Lose");
            return "lost";
        } else if (compPick == 2) {
            System.out.println("Win");
            return "won";
        } else {
            System.out.println("Tie");
            return "tied";
        }
    }

    public static void postResults(int winCount, int tieCount, int loseCount) {
        System.out.println("Total wins: " + winCount);
        System.out.println("Total ties: " + tieCount);
        System.out.println("Total losses: " + loseCount);

        if (winCount > loseCount) {
            System.out.println("You're the winner of this set of rounds!");
        } else if (loseCount > winCount) {
            System.out.println("The computer wins this set of rounds!");
        } else {
            System.out.println("It's a tie for this set of rounds!");
        }
    }
}
