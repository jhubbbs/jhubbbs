/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sg.statecapitals_v2;

/**
 *
 * @author apprentice
 */
public class Capital {
    
    String name;
    String population;
    String sqmi;
    
    public Capital(){
        
    }
    public Capital(String name, String population, String sqmi){
        this.name = name;
        this.population = population;
        this.sqmi = sqmi;
    }
    
    public String getName(){
        return this.name;
    }
    public String getPop(){
        return this.population;
    }
    public String getSq(){
        return this.sqmi;
    }
    
    
    @Override
    public String toString(){
        return "Capital: "+name +" | Population: "+population+" | Square Miles: "+sqmi;
    }
    
}
