/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sg.statecapitals_v2;


import java.util.HashMap;
import java.util.Scanner;


/**
 *
 * @author apprentice
 */
public class StateHash {

    public void run() {
        Scanner sc = new Scanner(System.in);
        HashMap<String, Capital> pairs = new HashMap<>();
        pairs.put("Alabama", new Capital("Montgomery", "205000", "156"));
        pairs.put("Alaska", new Capital("Juneau", "31000", "3255"));
        pairs.put("Arizona", new Capital("Phoenix", "1445000", "517"));
        pairs.put("Arkansas", new Capital("Little Rock", "193000", "116"));


        System.out.println("A list of states/capitals:");
        for (String k : pairs.keySet()) {
            System.out.println(k + " | " + pairs.get(k));
        }
        
        
        System.out.println("Please enter lower limit for capital population: ");
        int min = sc.nextInt();
        sc.nextLine();
        int cityPop;
        String stringCityPop;
        boolean success = false;
        
        
        System.out.println("LISTING CAPITALS WITH POPULATIONS GREATER THAN "+min+":");
        for (String k : pairs.keySet()) {
            stringCityPop = pairs.get(k).getPop();
            cityPop = Integer.parseInt(stringCityPop);
            
            if(cityPop >= min){
                System.out.println(k + " | "+ pairs.get(k));
                success = true;
            }
        }
        if(success == false){
            System.out.println("No capitals fit that criteria!");
        }
        
        
        
        
    }
}
