/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sg.statecapitals;


import java.util.HashMap;
import java.util.Scanner;


/**
 *
 * @author apprentice
 */
public class StateHash {

    public void run() {

        HashMap<String, String> capitals = new HashMap<>();
        capitals.put("Alabama", "Montgomery");
        capitals.put("Alaska", "Juneau");
        capitals.put("Arizona", "Phoenix");
        capitals.put("Arkansas", "Little Rock");
        
        System.out.println("A list of states:");
        for (String k : capitals.keySet()) {
            System.out.println(k);
        }
        
        System.out.println("A list of capitals:");
        for (String v : capitals.values()) {
            System.out.println(v);
        }

        System.out.println("A list of states/capitls:");
        for (String k : capitals.keySet()) {
            System.out.println(k + " / " + capitals.get(k));
        }

        String state;
        String again;
        
        Scanner sc = new Scanner(System.in);
        do {
            System.out.println("Enter state to search its capital: ");
            state = sc.nextLine();
            /*if(capitals.get(state).equals("null")){//BROKEN
                System.out.println("Sorry, we could not find that state!");
            }else{
            */   
                System.out.println(capitals.get(state));
            
            

            System.out.println("Enter Y to search again or anything else to quit: ");
            again = sc.nextLine();
        } while (again.equals("Y") || again.equals("y"));
        
        boolean capHit = false;
        do {
            System.out.println("Enter capital to search: ");
            String enteredCap = sc.nextLine();
            for (String s : capitals.keySet()) {
                if (capitals.get(s).equals(enteredCap)) {
                    System.out.println(s);
                    capHit=true;
                }

            }
            if(capHit==false){
                System.out.println("Sorry, we could not find a state with that capital!");
            }
            System.out.println("Enter Y to search again or anything else to quit: ");
            again = sc.nextLine();
        } while (again.equals("Y") || again.equals("y"));
        
        System.out.println("Bye");
    }
}
