/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sg.addressbook.dao;

import com.sg.addressbook.dto.Address;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Scanner;

/**
 *
 * @author apprentice
 */
public class AddressBook {

    private String OUTPUT_FILE = "SavedData/OutFile.txt";
    private ArrayList<Address> aBook = new ArrayList<Address>();

    private void writeAddressToFile(Address addy) {

        try {

            PrintWriter outPrinter = new PrintWriter(
                    new FileOutputStream(new File(OUTPUT_FILE), true));
            outPrinter.println(addy.encode());
            outPrinter.flush();

            outPrinter.close();

        } catch (FileNotFoundException fnf) {
            System.out.println("We cannot find the file: " + OUTPUT_FILE);
            System.out.println(fnf.getMessage());
        } catch (IOException io) {
            System.out.println("Problem writing our sample file");
            System.out.println(io.getMessage());
        }
    }

    public void writeAddressBookToFile() {
        try {
            PrintWriter out = new PrintWriter(new FileWriter(OUTPUT_FILE));
            for (int x = 0; x < aBook.size(); x++) {
                writeAddressToFile(aBook.get(x));
            }
        } catch (FileNotFoundException fnf) {
            System.out.println("We cannot find the file: " + OUTPUT_FILE);
            System.out.println(fnf.getMessage());
        } catch (IOException io) {
            System.out.println("Problem writing our sample file");
            System.out.println(io.getMessage());
        }

    }

    public void readAddressBookFromFile() {

        try {
            Scanner sc = new Scanner(new BufferedReader(new FileReader(OUTPUT_FILE)));
            // go through the file line-by-line
            // using Scanner's iterator
            // While there are things in the Scanner's buffer
            while (sc.hasNextLine()) {
                // Read in the next line
                String stuffToReadIn = sc.nextLine();
                // We need to decode this into a student object:
                Address myAddress = new Address();
                myAddress = myAddress.decode(stuffToReadIn);
                // Write out the object
                aBook.add(myAddress);
            }
        } catch (FileNotFoundException fnf) {
            // tell the user we can't read a file with the location
            System.out.println("Problem reading from file: " + OUTPUT_FILE);
            // show a message for our sanity
            System.out.println(fnf.getMessage());
        }
    }

    public void addAddress(String firstName, String lastName, String street,
            String city, String state, int zip) {

        aBook.add(new Address());
        int id = aBook.size() - 1;

        aBook.get(id).setId(id);
        aBook.get(id).setFirstName(firstName);
        aBook.get(id).setLastName(lastName);
        aBook.get(id).setStreet(street);
        aBook.get(id).setCity(city);
        aBook.get(id).setState(state);
        aBook.get(id).setZip(zip);

    }

    public void removeAddress(int id) {
        int arrayId;

        for (int x = 0; x < aBook.size(); x++) {
            if (aBook.get(x).getId() == id) {
                arrayId = x;
                aBook.remove(arrayId);
            }
        }

    }

    public void modifyAddress(int id, int field, String info) {

        if (field == 1) {
            aBook.get(id).setLastName(info);
        } else if (field == 2) {
            aBook.get(id).setFirstName(info);
        } else if (field == 3) {
            aBook.get(id).setStreet(info);
        } else if (field == 4) {
            aBook.get(id).setCity(info);
        } else if (field == 5) {
            aBook.get(id).setState(info);
        } else {
            int zip = Integer.parseInt(info);
            aBook.get(id).setZip(zip);
        }

    }

    public ArrayList<Address> getAddressById(int id) {

        ArrayList<Address> highlander = new ArrayList<Address>();
        highlander.add(aBook.get(id));

        return highlander;
    }

    public int getAddressCount() {

        return aBook.size();
    }

    public ArrayList<Address> getAddressBook() {

        return aBook;
    }

    public ArrayList<Address> getAddressByLastName(String lastName) {

        ArrayList<Address> smallList = new ArrayList<Address>();

        for (int x = 0; x < aBook.size(); x++) {

            if (aBook.get(x).getLastName().equals(lastName)) {
                smallList.add(aBook.get(x));
            }

        }
        return smallList;
    }

}
