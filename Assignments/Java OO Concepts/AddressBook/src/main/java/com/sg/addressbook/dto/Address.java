/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sg.addressbook.dto;

/**
 *
 * @author apprentice
 */
public class Address {

    private final String DELIMITER = "&&";

    private int id;
    private String lastName;
    private String firstName;
    private String street;
    private String city;
    private String state;
    private int zip;

    public String encode() {
        return id + DELIMITER + lastName + DELIMITER + firstName + DELIMITER
                + street + DELIMITER + city + DELIMITER + state + DELIMITER
                + zip;

    }

    public Address decode(String stringToDecode) {

        Address myAddress = new Address();

        String[] addressProperties = stringToDecode.split(DELIMITER);

        myAddress.id = Integer.parseInt(addressProperties[0]);
        myAddress.lastName = addressProperties[1];
        myAddress.firstName = addressProperties[2];
        myAddress.street = addressProperties[3];
        myAddress.city = addressProperties[4];
        myAddress.state = addressProperties[5];
        myAddress.zip = Integer.parseInt(addressProperties[6]);

        return myAddress;

    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public int getZip() {
        return zip;
    }

    public void setZip(int zip) {
        this.zip = zip;
    }

}
