/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sg.dvdlibraryv2.dao;

import com.sg.dvdlibraryv2.dto.DVD;
import java.util.ArrayList;

/**
 *
 * @author apprentice
 */
public interface DVDLibraryInterface {

    public void writeDVDToFile(DVD myDVD);

    public void writeDVDLibraryToFile();

    public void readDVDLibraryFromFile();

    public void addDVD(String title, int releaseDate, String mpaa,
            String director, String studio, String note);

    public void removeDVD(int id);

    public void modifyDVD(int id, int field, String info);

    public ArrayList<DVD> getDVDById(int id);

    public int getDVDCount();

    public ArrayList<DVD> getDVDLibrary();

    public ArrayList<DVD> getDVDByTitle(String title);

    public ArrayList<DVD> getDVDByYear(int year);

    public ArrayList<DVD> getDVDByRating(String rating);

    public ArrayList<DVD> getDVDByDirector(String director);

    public ArrayList<DVD> getDVDByStudio(String studio);

    public double getAverageYear();

    public ArrayList<DVD> getNewest();

    public ArrayList<DVD> getOldest();

}
