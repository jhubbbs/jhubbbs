/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sg.dvdlibraryv2.dao;

import com.sg.dvdlibraryv2.dto.DVD;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Scanner;

/**
 *
 * @author apprentice
 */
public class DVDLibrary implements DVDLibraryInterface {

    private String OUTPUT_FILE = "SavedData/OutFile.txt";
    private ArrayList<DVD> lib = new ArrayList<DVD>();

    @Override
    public void writeDVDToFile(DVD myDVD) {

        try {

            PrintWriter outPrinter = new PrintWriter(
                    new FileOutputStream(new File(OUTPUT_FILE), true));
            outPrinter.println(myDVD.encode());
            outPrinter.flush();

            outPrinter.close();

        } catch (FileNotFoundException fnf) {
            System.out.println("We cannot find the file: " + OUTPUT_FILE);
            System.out.println(fnf.getMessage());
        } catch (IOException io) {
            System.out.println("Problem writing our sample file");
            System.out.println(io.getMessage());
        }
    }

    @Override
    public void writeDVDLibraryToFile() {
        try {
            PrintWriter out = new PrintWriter(new FileWriter(OUTPUT_FILE));
            for (int x = 0; x < lib.size(); x++) {
                writeDVDToFile(lib.get(x));
            }
        } catch (FileNotFoundException fnf) {
            System.out.println("We cannot find the file: " + OUTPUT_FILE);
            System.out.println(fnf.getMessage());
        } catch (IOException io) {
            System.out.println("Problem writing our sample file");
            System.out.println(io.getMessage());
        }

    }

    @Override
    public void readDVDLibraryFromFile() {

        try {
            Scanner sc = new Scanner(new BufferedReader(new FileReader(OUTPUT_FILE)));
            // go through the file line-by-line
            // using Scanner's iterator
            // While there are things in the Scanner's buffer
            while (sc.hasNextLine()) {
                // Read in the next line
                String stuffToReadIn = sc.nextLine();

                DVD myDVD = new DVD();
                myDVD = myDVD.decode(stuffToReadIn);
                // Write out the object
                lib.add(myDVD);
            }
        } catch (FileNotFoundException fnf) {
            // tell the user we can't read a file with the location
            System.out.println("Problem reading from file: " + OUTPUT_FILE);
            // show a message for our sanity
            System.out.println(fnf.getMessage());
        }
    }

    @Override
    public void addDVD(String title, int releaseDate, String mpaa,
            String director, String studio, String note) {

        lib.add(new DVD());
        int id = lib.size() - 1;

        lib.get(id).setId(id);
        lib.get(id).setTitle(title);
        lib.get(id).setReleaseDate(releaseDate);
        lib.get(id).setMpaa(mpaa);
        lib.get(id).setDirector(director);
        lib.get(id).setStudio(studio);
        lib.get(id).setNote(note);

    }

    @Override
    public void removeDVD(int id) {
        int arrayId;

        for (int x = 0; x < lib.size(); x++) {
            if (lib.get(x).getId() == id) {
                arrayId = x;
                lib.remove(arrayId);
            }
        }

    }

    @Override
    public void modifyDVD(int id, int field, String info) {

        for (int x = 0; x < lib.size(); x++) {
            if (lib.get(x).getId() == id) {

                switch (field) {
                    case 1:
                        lib.get(x).setTitle(info);
                        break;
                    case 2:
                        lib.get(x).setReleaseDate(Integer.parseInt(info));
                        break;
                    case 3:
                        lib.get(x).setMpaa(info);
                        break;
                    case 4:
                        lib.get(x).setDirector(info);
                        break;
                    case 5:
                        lib.get(x).setStudio(info);
                        break;
                    default:
                        lib.get(x).setNote(info);
                        break;
                }

            }
        }

    }

    @Override
    public ArrayList<DVD> getDVDById(int id) {

        ArrayList<DVD> highlander = new ArrayList<DVD>();

        int checkIt;
        for (int x = 0; x < lib.size(); x++) {
            checkIt = lib.get(x).getId();

            if (id == checkIt) {
                highlander.add(lib.get(x));
            }
        }

        return highlander;
    }

    @Override
    public int getDVDCount() {

        return lib.size();
    }

    @Override
    public ArrayList<DVD> getDVDLibrary() {

        return lib;
    }

    @Override
    public ArrayList<DVD> getDVDByTitle(String title) {

        ArrayList<DVD> smallList = new ArrayList<DVD>();

        for (int x = 0; x < lib.size(); x++) {

            if (lib.get(x).getTitle().contains(title)) {
                smallList.add(lib.get(x));
            }

        }
        return smallList;
    }
    @Override
    public ArrayList<DVD> getDVDByYear(int year) {
        ArrayList<DVD> smallList = new ArrayList<DVD>();
        lib.stream()
                .filter(dvd -> dvd.getReleaseDate()==year)
                .forEach(dvd -> smallList.add(dvd));
        return smallList;
    }
    @Override
    public ArrayList<DVD> getDVDByRating(String rating) {
        ArrayList<DVD> smallList = new ArrayList<DVD>();
        lib.stream()
                .filter(dvd -> dvd.getMpaa().equals(rating))
                .forEach(dvd -> smallList.add(dvd));
        return smallList;
    }
    @Override
    public ArrayList<DVD> getDVDByDirector(String director) {
        ArrayList<DVD> smallList = new ArrayList<DVD>();
        lib.stream()
                .filter(dvd -> dvd.getDirector().contains(director))
                .forEach(dvd -> smallList.add(dvd));
        return smallList;
    }
    @Override
    public ArrayList<DVD> getDVDByStudio(String studio) {
        ArrayList<DVD> smallList = new ArrayList<>();
        lib.stream()
                .filter(dvd -> dvd.getStudio().contains(studio))
                .forEach(dvd -> smallList.add(dvd));
        return smallList;
    }
    @Override
    public double getAverageYear() {
        double averageYear = lib
                .stream()
                .mapToInt(DVD::getReleaseDate)
                .average()
                .getAsDouble();
               
        return averageYear;
    }
    
    @Override
    public ArrayList<DVD> getNewest(){
        /*int newestYear=0;
        int newestIndex=0;
        for(int x=0; x < lib.size();x++){
            if(lib.get(x).getReleaseDate() > newestYear){
                newestYear = lib.get(x).getReleaseDate();
                newestIndex = x;
            }
        }
        ArrayList<DVD> smallList = new ArrayList<>();
        smallList.add(lib.get(newestIndex));
        return smallList;
        */
        ArrayList<DVD> smallList = new ArrayList<>();
        int newest = lib
                .stream()
                .mapToInt(DVD::getReleaseDate)
                .max()
                .getAsInt();
        
        lib
                .stream()
                .filter(dvd -> dvd.getReleaseDate()==newest)
                .forEach(dvd -> smallList.add(dvd));

        return smallList;
    }
    
    @Override
    public ArrayList<DVD> getOldest(){
        
        /*int oldestYear=9999;
        int oldestIndex=0;
        
        for(int x=0; x < lib.size();x++){
            if(lib.get(x).getReleaseDate() < oldestYear){
                oldestYear = lib.get(x).getReleaseDate();
                oldestIndex = x;
            }
        }
        ArrayList<DVD> smallList = new ArrayList<>();
        smallList.add(lib.get(oldestIndex));
        return smallList;
        */
        ArrayList<DVD> smallList = new ArrayList<>();
        int oldest = lib
                .stream()
                .mapToInt(DVD::getReleaseDate)
                .min()
                .getAsInt();
        
        lib
                .stream()
                .filter(dvd -> dvd.getReleaseDate()==oldest)
                .forEach(dvd -> smallList.add(dvd));

        return smallList;
    }
    
    
}
