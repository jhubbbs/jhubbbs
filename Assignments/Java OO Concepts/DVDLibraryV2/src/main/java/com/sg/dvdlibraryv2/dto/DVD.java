/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sg.dvdlibraryv2.dto;

/**
 *
 * @author apprentice
 */
public class DVD {
    
   private final String DELIMITER = "&&";

   private int id;
   private String title;
   private int releaseDate;
   private String mpaa;
   private String director;
   private String studio;
   private String note;
   
   public String encode() {
        return id+ DELIMITER + title + DELIMITER + releaseDate + DELIMITER + mpaa + DELIMITER
                + director + DELIMITER + studio + DELIMITER + note;

    }

    public DVD decode(String stringToDecode) {

        DVD myDVD = new DVD();

        String[] DVDProperties = stringToDecode.split(DELIMITER);
        myDVD.id = Integer.parseInt(DVDProperties[0]);
        myDVD.title = DVDProperties[1];
        myDVD.releaseDate = Integer.parseInt(DVDProperties[2]);
        myDVD.mpaa = DVDProperties[3];
        myDVD.director = DVDProperties[4];
        myDVD.studio = DVDProperties[5];
        myDVD.note = DVDProperties[6];
        
        return myDVD;

    }

    /**
     * @return the title
     */
    public String getTitle() {
        return title;
    }

    /**
     * @param title the title to set
     */
    public void setTitle(String title) {
        this.title = title;
    }

    /**
     * @return the releaseDate
     */
    public int getReleaseDate() {
        return releaseDate;
    }

    /**
     * @param releaseDate the releaseDate to set
     */
    public void setReleaseDate(int releaseDate) {
        this.releaseDate = releaseDate;
    }

    /**
     * @return the mpaa
     */
    public String getMpaa() {
        return mpaa;
    }

    /**
     * @param mpaa the mpaa to set
     */
    public void setMpaa(String mpaa) {
        this.mpaa = mpaa;
    }

    /**
     * @return the director
     */
    public String getDirector() {
        return director;
    }

    /**
     * @param director the director to set
     */
    public void setDirector(String director) {
        this.director = director;
    }

    /**
     * @return the studio
     */
    public String getStudio() {
        return studio;
    }

    /**
     * @param studio the studio to set
     */
    public void setStudio(String studio) {
        this.studio = studio;
    }

    /**
     * @return the id
     */
    public int getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(int id) {
        this.id = id;
    }

    /**
     * @return the note
     */
    public String getNote() {
        return note;
    }

    /**
     * @param note the note to set
     */
    public void setNote(String note) {
        this.note = note;
    }
}
