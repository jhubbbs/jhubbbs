/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sg.studentquizgrades;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Scanner;

/**
 *
 * @author apprentice
 */
public class StudentHash {

    Scanner sc = new Scanner(System.in);
    ConsoleIO cio = new ConsoleIO();

    public void run() {

        HashMap<String, ArrayList> pairs = new HashMap<>();
        boolean more = true;
        
        int selection;

        do {

            do {
                selection = cio.getInt("Please make a selection:\n"
                        + "1. Add a student\n"
                        + "2. Remove a student\n"
                        + "3. List a student's scores\n"
                        + "4. Show a student's average score\n"
                        + "5. Exit program\n");
            } while (selection < 1 || selection > 5);
            
            if(selection == 1){
                addStudent(pairs);
            }else if(selection == 2){
                removeStudent(pairs);
            }else if(selection ==3){
                listScores(pairs);
            }else if(selection == 4){
                listAverage(pairs);
            }else{
                more = false;
            }
            
        } while (more == true);

        System.out.println("\nGoodbye!");

    }

    public HashMap<String, ArrayList> addStudent(HashMap<String, ArrayList> pairs) {
        System.out.println("\n--ADD STUDENT--");
        String name = cio.getString("Please enter the student's name: ");
        pairs.put(name, new ArrayList());
        boolean done = false;
        int count = 0;

        do {
            count++;
            double score = cio.getDoubleMinMax("\nPlease enter quiz score #" + count, 0, 100);

            pairs.get(name).add(score);

            String stringDone = cio.getString(
                    "\nDo you have another quiz score to enter?\nEnter Y for yes or anything else to stop");

            if (!(stringDone.equals("Y") || stringDone.equals("y"))) {
                done = true;
            }
        } while (done == false);
        System.out.println("\n");
        return pairs;
    }

    public HashMap<String, ArrayList> removeStudent(HashMap<String, ArrayList> pairs) {
        System.out.println("\n--REMOVE STUDENT--");
        String name = cio.getString("Please enter the student's name: ");
        pairs.remove(name);
        System.out.println("\n"+name+" has been removed\n");
        return pairs;
    }

    public void listStudents(HashMap<String, ArrayList> pairs) {
        System.out.println("Student list:");
        for (String k : pairs.keySet()) {
            System.out.println("\n"+k+"\n");
        }
    }

    public void listScores(HashMap<String, ArrayList> pairs) {
        System.out.println("\n--VIEW SCORES--");
        String name = cio.getString("Which student?");
        System.out.println("\n"+name + ": " + pairs.get(name)+"\n");
    }

    public void listAverage(HashMap<String, ArrayList> pairs) {
        System.out.println("\n--VIEW AVERAGE SCORE--");
        String name = cio.getString("Which student?");
        int size = pairs.get(name).size();
        double sum = 0;
        double count = 0;
        double current;
        for (int x = 0; x < size; x++) {
            current = Double.parseDouble(pairs.get(name).get(x).toString());
            sum = sum + current;
            count++;
        }
        System.out.println("\n"+name + "'s average is:" + sum / count+"\n");
    }

}
