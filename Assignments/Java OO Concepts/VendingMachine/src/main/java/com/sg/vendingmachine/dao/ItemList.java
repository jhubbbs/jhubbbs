/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sg.vendingmachine.dao;

import com.sg.vendingmachine.dto.Item;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.Scanner;

/**
 *
 * @author apprentice
 */
public class ItemList {

    private String OUTPUT_FILE = "SavedData/OutFile.txt";
    private HashMap<String, Item> map = new HashMap<String, Item>();

    public HashMap<String, Item> getList() {

        return map;
    }
    
    public int getListSize(){
        
        return map.size();
    }

    public double vendItem(String chosen, double cash) {
        //returning double to controller to give it the cost
        //it can then call makeChange() and pass it cost and cash

        if (cash >= map.get(chosen).getCost()) {

            int q = map.get(chosen).getQuantity();
            q--;
            map.get(chosen).setQuantity(q);

            return map.get(chosen).getCost();
        } else {
            return 0;//if can't afford, returning cost of zero
        }

    }

    public String getNameById(int choice) {

        String name = "false";

        for (String k : map.keySet()) {
            if (map.get(k).getId() == choice) {
                name = map.get(k).getName();
            }
        }

        return name;
    }

    private void writeItemToFile(Item myItem) {

        try {

            PrintWriter outPrinter = new PrintWriter(
                    new FileOutputStream(new File(OUTPUT_FILE), true));
            outPrinter.println(myItem.encode());
            outPrinter.flush();

            outPrinter.close();

        } catch (FileNotFoundException fnf) {
            System.out.println("We cannot find the file: " + OUTPUT_FILE);
            System.out.println(fnf.getMessage());
        } catch (IOException io) {
            System.out.println("Problem writing our sample file");
            System.out.println(io.getMessage());
        }
    }

    public void writeItemListToFile() {
        try {
            PrintWriter out = new PrintWriter(new FileWriter(OUTPUT_FILE));

            for (String k : map.keySet()) {
                writeItemToFile(map.get(k));
            }

        } catch (FileNotFoundException fnf) {
            System.out.println("We cannot find the file: " + OUTPUT_FILE);
            System.out.println(fnf.getMessage());
        } catch (IOException io) {
            System.out.println("Problem writing our sample file");
            System.out.println(io.getMessage());
        }

    }

    public void readItemListFromFile() {

        try {
            Scanner sc = new Scanner(new BufferedReader(new FileReader(OUTPUT_FILE)));
            // go through the file line-by-line
            // using Scanner's iterator
            // While there are things in the Scanner's buffer
            while (sc.hasNextLine()) {

                String stuffToReadIn = sc.nextLine();

                Item myItem = new Item();
                myItem = myItem.decode(stuffToReadIn);

                map.put(myItem.getName(), myItem);
            }
        } catch (FileNotFoundException fnf) {
            // tell the user we can't read a file with the location
            System.out.println("Problem reading from file: " + OUTPUT_FILE);
            System.out.println(fnf.getMessage());
        }
    }

}
