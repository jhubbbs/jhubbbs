/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sg.vendingmachine.controller;

import com.sg.vendingmachine.dao.ItemList;
import com.sg.vendingmachine.dto.Item;
import com.sg.vendingmachine.ui.ConsoleIO;
import java.util.HashMap;

/**
 *
 * @author apprentice
 */
public class ItemListController {

    ConsoleIO cio = new ConsoleIO();
    ItemList controlled = new ItemList();
    
    
    
    public void run() {
        

        double cash = 0;
        boolean done = false;
        int selection;
        String chosen;
        double cost;

        controlled.readItemListFromFile();
        
        
        while (done == false) {
            
            displayMenu(cash);

            selection = cio.getIntMinMax("Please select an option: ", -1, controlled.getListSize());
            if(selection==-1){
                done = true;
            }
            else if(selection==0){
                cash = addCash(cash);
            }else{
                
                chosen = controlled.getNameById(selection);
                cost = controlled.vendItem(chosen, cash);
                if(cost==0){
                    cio.sout("NOT ENOUGH $$$$$$$$$");
                }else{
                    cio.sout("VENDED\nPLEASE TAKE YOUR ITEM\n");
                    makeChange(cost, cash);
                    cash = 0;
                    String another = cio.getString("Another go? Enter N to quit...");
                    if(another.equals("N")|| another.equals("n")){
                        done = true;
                        
                    }
                }
            }
            
        }
        controlled.writeItemListToFile();
        cio.sout("GOODBYE!");

    }

    public void makeChange(double cost, double cash) {
        cash = cash - cost;

        int q = 0;
        int d = 0;
        int n = 0;
        int p = 0;

        while (cash >= 0.01) {
            if (cash - .25 >= 0) {
                q++;
                cash = cash - .25;
            } else if (cash - .1 >= 0) {
                d++;
                cash = cash - .1;
            } else if (cash - .05 >= 0) {
                n++;
                cash = cash - .05;
            } else if (cash - .01 >= 0) {
                p++;
                cash = cash - .01;
            }
        }

        cio.sout("Your change is:\n"
                + q + " quarter\n"
                + d + " dimes\n"
                + n + " nickles\n"
                + p + " pennies\n");
    }

    private void displayMenu(double cash) {
        
        HashMap<String, Item> muhList = controlled.getList();
        int count = 1;
        
        cio.sout("\nBalance: "+cash+"\n");
        cio.sout("0: Insert Cash");
        
        while(count <= muhList.size()){
            for (String k : muhList.keySet()) {
                if(muhList.get(k).getId() == count){
                    if(muhList.get(k).getQuantity()>0){
                        cio.sout(muhList.get(k).toString());
                        
                    }                    
                    count++;
                }
            }
        }
        cio.sout("-1: Quit");
        
    }

    private double addCash(double cash){
        
        Double moMoney = cio.getDoubleMinMax("Please enter an amount: ", 0, 100);
        
        return cash + moMoney;
    }

}
