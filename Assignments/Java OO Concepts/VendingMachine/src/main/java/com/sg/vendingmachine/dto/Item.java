/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sg.vendingmachine.dto;

/**
 *
 * @author apprentice
 */
public class Item {

    private int id;
    private String name;
    private double cost;
    private int quantity;

    private final String DELIMITER = "&&";

    
    @Override
    public String toString(){
        
        return id+": "+name+" $"+cost+" Q:"+quantity;
    }
    
    
    public String encode() {
        return id+ DELIMITER + name + DELIMITER + cost + DELIMITER + quantity;

    }

    public Item decode(String stringToDecode) {

        Item myItem = new Item();

        String[] ItemProperties = stringToDecode.split(DELIMITER);

        myItem.setId(Integer.parseInt(ItemProperties[0]));
        myItem.setName(ItemProperties[1]);
        myItem.setCost(Double.parseDouble(ItemProperties[2]));
        myItem.setQuantity(Integer.parseInt(ItemProperties[3]));

        return myItem;

    }

    /**
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * @param name the name to set
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * @return the cost
     */
    public double getCost() {
        return cost;
    }

    /**
     * @param cost the cost to set
     */
    public void setCost(double cost) {
        this.cost = cost;
    }

    /**
     * @return the quantity
     */
    public int getQuantity() {
        return quantity;
    }

    /**
     * @param quantity the quantity to set
     */
    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    /**
     * @return the id
     */
    public int getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(int id) {
        this.id = id;
    }
}
