/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sg.flooringmastery.dao;

import com.sg.flooringmastery.dto.Product;
import java.util.ArrayList;

/**
 *
 * @author apprentice
 */
public interface ProductListInterface {

    public void writeProductToFile(Product myProduct);

    public void writeProductListToFile();

    public void readProductListFromFile();

    public ArrayList<Product> getProducts();
}
