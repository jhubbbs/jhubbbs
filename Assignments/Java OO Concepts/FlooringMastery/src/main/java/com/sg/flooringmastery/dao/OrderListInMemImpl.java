/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sg.flooringmastery.dao;

import com.sg.flooringmastery.dto.Order;

import java.util.ArrayList;

/**
 *
 * @author apprentice
 */
public class OrderListInMemImpl implements OrderList {

    private ArrayList<Order> orders = new ArrayList<>();
    private String date;

    @Override
    public ArrayList<Order> getOrders() {

        return orders;
    }

    @Override
    public void clearOrders() {

        ArrayList<Order> blank = new ArrayList<>();
        orders = blank;
    }

    @Override
    public void sortOrders() {

        for (int x = 0; x < orders.size(); x++) {

            orders.get(x).setOrderNumber(x + 1);

        }

    }

    @Override
    public void addOrder(Order order) {

        orders.add(order);
    }

    @Override
    public void modifyOrder(int x, Order newOrder) {
        orders.set(x, newOrder);
    }

    @Override
    public void removeOrder(int x) {
        orders.remove(x);
    }

    @Override
    public Order calculateOrder(Order newOrder) {

        newOrder.setMaterialCost(newOrder.getArea() * newOrder.getMaterialCostPerSqFt());
        newOrder.setLaborCost(newOrder.getArea() * newOrder.getLaborCostPerSqFt());

        double subtotal = newOrder.getLaborCost() + newOrder.getMaterialCost();

        newOrder.setTax(newOrder.getTaxRate() * subtotal);
        newOrder.setTotalCost(newOrder.getTax() + subtotal);

        return newOrder;
    }

    @Override
    public void writeOrderToFile(Order myOrder) {
        //empty

    }

    @Override
    public void writeOrderListToFile() {
        //empty
    }

    @Override
    public void readOrderListFromFile() {
        //empty
    }

    @Override
    public String getDate() {
        return date;
    }

    @Override
    public void setDate(String date) {
        this.date = date;
    }

}
