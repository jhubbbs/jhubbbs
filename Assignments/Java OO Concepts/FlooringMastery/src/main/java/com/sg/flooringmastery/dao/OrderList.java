/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sg.flooringmastery.dao;

import com.sg.flooringmastery.dto.Order;
import java.util.ArrayList;

public interface OrderList {

    public ArrayList<Order> getOrders();

    public void clearOrders();

    public void sortOrders();

    public void addOrder(Order order);

    public void modifyOrder(int x, Order newOrder);

    public void removeOrder(int x);

    public Order calculateOrder(Order newOrder);

    public void writeOrderToFile(Order myOrder);

    public void writeOrderListToFile();

    public void readOrderListFromFile();

    public String getDate();

    public void setDate(String date);
}
