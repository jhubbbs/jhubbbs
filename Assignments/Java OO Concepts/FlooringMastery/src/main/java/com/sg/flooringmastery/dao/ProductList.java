/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sg.flooringmastery.dao;

import com.sg.flooringmastery.dto.Product;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Scanner;

/**
 *
 * @author apprentice
 */
public class ProductList implements ProductListInterface {

    private ArrayList<Product> products = new ArrayList<>();
    private final String PRODUCTS_FILE = "SavedData/Products.txt";

    @Override
    public void writeProductToFile(Product myProduct) {

        try {

            PrintWriter outPrinter = new PrintWriter(
                    new FileOutputStream(new File(PRODUCTS_FILE), true));
            outPrinter.println(myProduct.encode());
            outPrinter.flush();

            outPrinter.close();

        } catch (FileNotFoundException fnf) {
            System.out.println("We cannot find the file: " + PRODUCTS_FILE);
            System.out.println(fnf.getMessage());
        } catch (IOException io) {
            System.out.println("Problem writing our sample file");
            System.out.println(io.getMessage());
        }
    }

    @Override
    public void writeProductListToFile() {

        try {
            PrintWriter out = new PrintWriter(new FileWriter(PRODUCTS_FILE));
            for (int x = 0; x < products.size(); x++) {
                writeProductToFile(products.get(x));
            }
        } catch (FileNotFoundException fnf) {
            System.out.println("We cannot find the file: " + PRODUCTS_FILE);
            System.out.println(fnf.getMessage());
        } catch (IOException io) {
            System.out.println("Problem writing our sample file");
            System.out.println(io.getMessage());
        }

    }

    @Override
    public void readProductListFromFile() {

        try {
            Scanner sc = new Scanner(new BufferedReader(new FileReader(PRODUCTS_FILE)));

            while (sc.hasNextLine()) {

                String stuffToReadIn = sc.nextLine();

                Product myProduct = new Product();
                myProduct = myProduct.decode(stuffToReadIn);

                products.add(myProduct);
            }
        } catch (FileNotFoundException fnf) {
            System.out.println("Problem reading from file: " + PRODUCTS_FILE);
            System.out.println(fnf.getMessage());
        }
    }

    @Override
    public ArrayList<Product> getProducts() {

        return products;
    }
}
