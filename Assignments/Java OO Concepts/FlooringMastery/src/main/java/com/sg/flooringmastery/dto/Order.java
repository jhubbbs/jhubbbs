/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sg.flooringmastery.dto;

/**
 *
 * @author apprentice
 */
public class Order {

    private int orderNumber;
    private String customerName;
    private String state;
    private double taxRate;
    private String productType;
    private double area;
    private double materialCostPerSqFt;
    private double laborCostPerSqFt;
    private double materialCost;
    private double laborCost;
    private double tax;
    private double totalCost;

    private final String DELIMITER = "&&";

    public Order() {

    }

    public Order(int num, String name, String state, double taxRate, String productType,
            double area, double matCostPer, double labCostPer) {

        this.orderNumber = num;
        this.customerName = name;
        this.state = state;
        this.taxRate = taxRate;
        this.productType = productType;
        this.area = area;
        this.materialCostPerSqFt = matCostPer;
        this.laborCostPerSqFt = labCostPer;

    }

    @Override
    public String toString() {

        return "----------------------------------\nOrderNo: " + orderNumber + "\tCustomer: "
                + customerName + "\tState: "
                + state + "\nTax%: " + (taxRate*100) + "\tProduct: " + productType + "\n"
                + "Area: " + area + "\tMCPSF: " + materialCostPerSqFt + "\tLCPSF: "
                + laborCostPerSqFt + "\nMat Cost: " + materialCost + "\tLab Cost: "
                + laborCost + "\tTax: " + tax + "\n\nTOTAL COST: " + totalCost
                + "\n----------------------------------";
    }

    public String encode() {
        return orderNumber + DELIMITER + customerName + DELIMITER + state + DELIMITER
                + taxRate + DELIMITER + productType + DELIMITER + area + DELIMITER + materialCostPerSqFt
                + DELIMITER + laborCostPerSqFt + DELIMITER + materialCost + DELIMITER + laborCost + DELIMITER
                + tax + DELIMITER + totalCost;

    }

    public Order decode(String stringToDecode) {

        Order myOrder = new Order();

        String[] orderProperties = stringToDecode.split(DELIMITER);

        myOrder.orderNumber = Integer.parseInt(orderProperties[0]);
        myOrder.customerName = orderProperties[1];
        myOrder.state = orderProperties[2];
        myOrder.taxRate = Double.parseDouble(orderProperties[3]);
        myOrder.productType = orderProperties[4];
        myOrder.area = Double.parseDouble(orderProperties[5]);
        myOrder.materialCostPerSqFt = Double.parseDouble(orderProperties[6]);
        myOrder.laborCostPerSqFt = Double.parseDouble(orderProperties[7]);
        myOrder.materialCost = Double.parseDouble(orderProperties[8]);
        myOrder.laborCost = Double.parseDouble(orderProperties[9]);
        myOrder.tax = Double.parseDouble(orderProperties[10]);
        myOrder.totalCost = Double.parseDouble(orderProperties[11]);

        return myOrder;

    }

    /**
     * @return the orderNumber
     */
    public int getOrderNumber() {
        return orderNumber;
    }

    /**
     * @param orderNumber the orderNumber to set
     */
    public void setOrderNumber(int orderNumber) {
        this.orderNumber = orderNumber;
    }

    /**
     * @return the customerName
     */
    public String getCustomerName() {
        return customerName;
    }

    /**
     * @param customerName the customerName to set
     */
    public void setCustomerName(String customerName) {
        this.customerName = customerName;
    }

    /**
     * @return the state
     */
    public String getState() {
        return state;
    }

    /**
     * @param state the state to set
     */
    public void setState(String state) {
        this.state = state;
    }

    /**
     * @return the taxRate
     */
    public double getTaxRate() {
        return taxRate;
    }

    /**
     * @param taxRate the taxRate to set
     */
    public void setTaxRate(double taxRate) {
        this.taxRate = taxRate;
    }

    /**
     * @return the productType
     */
    public String getProductType() {
        return productType;
    }

    /**
     * @param productType the productType to set
     */
    public void setProductType(String productType) {
        this.productType = productType;
    }

    /**
     * @return the area
     */
    public double getArea() {
        return area;
    }

    /**
     * @param area the area to set
     */
    public void setArea(double area) {
        this.area = area;
    }

    /**
     * @return the materialCostPerSqFt
     */
    public double getMaterialCostPerSqFt() {
        return materialCostPerSqFt;
    }

    /**
     * @param materialCostPerSqFt the materialCostPerSqFt to set
     */
    public void setMaterialCostPerSqFt(double materialCostPerSqFt) {
        this.materialCostPerSqFt = materialCostPerSqFt;
    }

    /**
     * @return the laborCostPerSqFt
     */
    public double getLaborCostPerSqFt() {
        return laborCostPerSqFt;
    }

    /**
     * @param laborCostPerSqFt the laborCostPerSqFt to set
     */
    public void setLaborCostPerSqFt(double laborCostPerSqFt) {
        this.laborCostPerSqFt = laborCostPerSqFt;
    }

    /**
     * @return the materialCost
     */
    public double getMaterialCost() {
        return materialCost;
    }

    /**
     * @param materialCost the materialCost to set
     */
    public void setMaterialCost(double materialCost) {
        this.materialCost = materialCost;
    }

    /**
     * @return the laborCost
     */
    public double getLaborCost() {
        return laborCost;
    }

    /**
     * @param laborCost the laborCost to set
     */
    public void setLaborCost(double laborCost) {
        this.laborCost = laborCost;
    }

    /**
     * @return the tax
     */
    public double getTax() {
        return tax;
    }

    /**
     * @param tax the tax to set
     */
    public void setTax(double tax) {
        this.tax = tax;
    }

    /**
     * @return the totalCost
     */
    public double getTotalCost() {
        return totalCost;
    }

    /**
     * @param totalCost the totalCost to set
     */
    public void setTotalCost(double totalCost) {
        this.totalCost = totalCost;
    }
}
