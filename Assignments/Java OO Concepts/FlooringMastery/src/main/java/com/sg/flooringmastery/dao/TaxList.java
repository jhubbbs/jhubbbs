/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sg.flooringmastery.dao;

import com.sg.flooringmastery.dto.Tax;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Scanner;

/**
 *
 * @author apprentice
 */
public class TaxList implements TaxListInterface {

    ArrayList<Tax> taxes = new ArrayList<>();

    private final String TAXES_FILE = "SavedData/Taxes.txt";

    @Override
    public void writeTaxToFile(Tax myTax) {

        try {

            PrintWriter outPrinter = new PrintWriter(
                    new FileOutputStream(new File(TAXES_FILE), true));
            outPrinter.println(myTax.encode());
            outPrinter.flush();

            outPrinter.close();

        } catch (FileNotFoundException fnf) {
            System.out.println("We cannot find the file: " + TAXES_FILE);
            System.out.println(fnf.getMessage());
        } catch (IOException io) {
            System.out.println("Problem writing our sample file");
            System.out.println(io.getMessage());
        }
    }

    @Override
    public void writeTaxListToFile() {

        try {
            PrintWriter out = new PrintWriter(new FileWriter(TAXES_FILE));
            for (int x = 0; x < taxes.size(); x++) {
                writeTaxToFile(taxes.get(x));
            }
        } catch (FileNotFoundException fnf) {
            System.out.println("We cannot find the file: " + TAXES_FILE);
            System.out.println(fnf.getMessage());
        } catch (IOException io) {
            System.out.println("Problem writing our sample file");
            System.out.println(io.getMessage());
        }

    }

    @Override
    public void readTaxListFromFile() {

        try {
            Scanner sc = new Scanner(new BufferedReader(new FileReader(TAXES_FILE)));

            while (sc.hasNextLine()) {

                String stuffToReadIn = sc.nextLine();

                Tax myTax = new Tax();
                myTax = myTax.decode(stuffToReadIn);

                taxes.add(myTax);
            }
        } catch (FileNotFoundException fnf) {
            System.out.println("Problem reading from file: " + TAXES_FILE);
            System.out.println(fnf.getMessage());
        }
    }

    @Override
    public ArrayList<Tax> getTaxes() {
        return taxes;
    }
}
