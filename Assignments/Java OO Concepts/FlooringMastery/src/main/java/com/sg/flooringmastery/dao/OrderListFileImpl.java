/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sg.flooringmastery.dao;

import com.sg.flooringmastery.dto.Order;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Scanner;

/**
 *
 * @author apprentice
 */
public class OrderListFileImpl implements OrderList {

    private ArrayList<Order> orders = new ArrayList<>();
    private String date;

    @Override
    public ArrayList<Order> getOrders() {

        return orders;
    }

    @Override
    public void clearOrders() {

        ArrayList<Order> blank = new ArrayList<>();
        orders = blank;
    }

    @Override
    public void sortOrders() {

        for (int x = 0; x < orders.size(); x++) {

            orders.get(x).setOrderNumber(x + 1);

        }

    }

    @Override
    public void addOrder(Order order) {

        orders.add(order);
    }

    @Override
    public void modifyOrder(int x, Order newOrder) {
        orders.set(x, newOrder);
    }

    @Override
    public void removeOrder(int x) {
        orders.remove(x);
    }

    @Override
    public Order calculateOrder(Order newOrder) {

        newOrder.setMaterialCost(newOrder.getArea() * newOrder.getMaterialCostPerSqFt());
        newOrder.setLaborCost(newOrder.getArea() * newOrder.getLaborCostPerSqFt());

        double subtotal = newOrder.getLaborCost() + newOrder.getMaterialCost();

        newOrder.setTax(newOrder.getTaxRate() * subtotal);
        newOrder.setTotalCost(newOrder.getTax() + subtotal);

        return newOrder;
    }

    @Override
    public void writeOrderToFile(Order myOrder) {
        String file = "SavedData/Orders_" + getDate() + ".txt";

        try {

            PrintWriter outPrinter = new PrintWriter(
                    new FileOutputStream(new File(file), true));
            outPrinter.println(myOrder.encode());
            outPrinter.flush();

            outPrinter.close();

        } catch (FileNotFoundException fnf) {
            System.out.println("We cannot find the file: " + file);
            System.out.println(fnf.getMessage());
        } catch (IOException io) {
            System.out.println("Problem writing our sample file");
            System.out.println(io.getMessage());
        }
    }

    @Override
    public void writeOrderListToFile() {
        String file = "SavedData/Orders_" + getDate() + ".txt";

        try {
            PrintWriter out = new PrintWriter(new FileWriter(file));
            for (int x = 0; x < orders.size(); x++) {
                writeOrderToFile(orders.get(x));
            }
        } catch (FileNotFoundException fnf) {
            System.out.println("We cannot find the file: " + file);
            System.out.println(fnf.getMessage());
        } catch (IOException io) {
            System.out.println("Problem writing our sample file");
            System.out.println(io.getMessage());
        }

    }

    @Override
    public void readOrderListFromFile() {
        String file = "SavedData/Orders_" + getDate() + ".txt";

        try {
            Scanner sc = new Scanner(new BufferedReader(new FileReader(file)));

            while (sc.hasNextLine()) {

                String stuffToReadIn = sc.nextLine();

                Order myOrder = new Order();
                myOrder = myOrder.decode(stuffToReadIn);

                orders.add(myOrder);
            }
        } catch (FileNotFoundException fnf) {
            System.out.println("No file currently exists for this date.");

        }
    }

    @Override
    public String getDate() {
        return date;
    }

    @Override
    public void setDate(String date) {
        this.date = date;
    }

}
