/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sg.flooringmastery.dto;

/**
 *
 * @author apprentice
 */
public class Tax {

    private String state;
    private double taxRate;

    private final String DELIMITER = "&&";

    public String encode() {
        return state + DELIMITER + taxRate;

    }

    public Tax decode(String stringToDecode) {

        Tax myTax = new Tax();

        String[] taxProperties = stringToDecode.split(DELIMITER);

        myTax.state = taxProperties[0];
        myTax.taxRate = Double.parseDouble(taxProperties[1]);

        return myTax;

    }

    /**
     * @return the state
     */
    public String getState() {
        return state;
    }

    /**
     * @param state the state to set
     */
    public void setState(String state) {
        this.state = state;
    }

    /**
     * @return the taxRate
     */
    public double getTaxRate() {
        return taxRate;
    }

    /**
     * @param taxRate the taxRate to set
     */
    public void setTaxRate(double taxRate) {
        this.taxRate = taxRate;
    }
}
