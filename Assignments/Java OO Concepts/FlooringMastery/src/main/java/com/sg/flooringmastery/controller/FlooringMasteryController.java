/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sg.flooringmastery.controller;

import com.sg.flooringmastery.dao.OrderList;
import com.sg.flooringmastery.dao.ProductList;
import com.sg.flooringmastery.dao.TaxList;
import com.sg.flooringmastery.dto.Order;
import com.sg.flooringmastery.dto.Product;
import com.sg.flooringmastery.dto.Tax;
import com.sg.flooringmastery.ui.ConsoleIO;
import com.sg.flooringmastery.ui.UI;
import java.util.ArrayList;

/**
 *
 * @author apprentice
 *
 */
public class FlooringMasteryController {
    
    ConsoleIO cio = new ConsoleIO();
    UI ui = new UI();
    String date;
    
    ProductList myProducts = new ProductList();
    TaxList myTaxes = new TaxList();
    OrderList myOrders;
    
    public FlooringMasteryController(OrderList dao) {
        this.myOrders = dao;
    }
    
    public void run() {
        
        myProducts.readProductListFromFile();
        myTaxes.readTaxListFromFile();
        
        date = "start";
        cio.sout("\n");
        changeDate();
        boolean done = false;
        
        while (done == false) {
            int selection = ui.mainMenuPrompt(date);
            switch (selection) {
                case 1:
                    changeDate();
                    break;
                case 2:
                    add();
                    break;
                case 3:
                    modify();
                    break;
                case 4:
                    remove();
                    break;
                case 5:
                    displayOrders();
                    break;
                case 6:
                    cio.sout("-----SAVE-----");
                    save();
                    break;
                case 7:
                    done = exit();
                    break;
            }
        }
    }
    
    public void changeDate() {
        if (!(date.equals("start"))) {
            Boolean saveMe = ui.promptSave();
            if (saveMe == true) {
                myOrders.writeOrderListToFile();
            }
        }
        
        myOrders.clearOrders();
        
        date = ui.promptForDate();
        myOrders.setDate(date);
        myOrders.readOrderListFromFile();
    }
    
    public void add() {
        ArrayList<Order> orders = myOrders.getOrders();
        ArrayList<Product> products = myProducts.getProducts();
        ArrayList<Tax> taxes = myTaxes.getTaxes();
        
        Order newOrder = ui.promptAddInfo(products, taxes, orders);
        
        newOrder = myOrders.calculateOrder(newOrder);
        
        int confirm = ui.confirmOrder(newOrder);
        
        if (confirm == 1) {
            myOrders.addOrder(newOrder);
            cio.sout("ADDED");
        } else {
            cio.sout("CANCELLED");
        }
        
    }
    
    public void modify() {
        
        cio.sout("-----MODIFY ORDER-----");
        
        ArrayList<Order> orders = myOrders.getOrders();
        ArrayList<Product> products = myProducts.getProducts();
        ArrayList<Tax> taxes = myTaxes.getTaxes();
        
        int num = cio.getIntMinMax("Order No: ", 0, 1000000);
        boolean found = false;
        
        for (int x = 0; x < orders.size(); x++) {
            if (orders.get(x).getOrderNumber() == num) {
                found = true;
                int confirm = ui.confirmOrder(orders.get(x));
                if (confirm == 1) {
                    
                    Order newOrder = orders.get(x);
                    newOrder = ui.promptModifyInfo(products, taxes, orders, newOrder);
                    
                    newOrder = myOrders.calculateOrder(newOrder);
                    
                    myOrders.modifyOrder(x, newOrder);
                    
                    cio.sout("MODIFIED");
                    
                    cio.sout(newOrder.toString());
                } else {
                    cio.sout("CANCELLED");
                }
            }
        }
        if (found == false) {
            cio.sout("We could not find that order number...");
        }
        
    }
    
    public void remove() {
        
        cio.sout("-----REMOVE ORDER-----");
        
        ArrayList<Order> orders = myOrders.getOrders();
        
        int num = cio.getIntMinMax("Order No: ", 0, 1000000);
        boolean found = false;
        
        for (int x = 0; x < orders.size(); x++) {
            if (orders.get(x).getOrderNumber() == num) {
                found = true;
                int confirm = ui.confirmOrder(orders.get(x));
                if (confirm == 1) {
                    
                    myOrders.removeOrder(x);
                    myOrders.sortOrders();
                    cio.sout("REMOVED");
                } else {
                    cio.sout("CANCELLED");
                }
            }
        }
        if (found == false) {
            cio.sout("We could not find that order number...");
        }
    }
    
    public void displayOrders() {
        
        ArrayList<Order> theseOrders = myOrders.getOrders();
        
        if (theseOrders.isEmpty()) {
            cio.sout("No orders found...");
        } else {
            ui.printOrders(theseOrders, date);
        }
    }
    
    public boolean exit() {
        
        cio.sout("-----EXITING-----");
        save();
        cio.sout("GOODBYE!");
        return true;
    }
    
    public void save() {
        Boolean saveMe = ui.promptSave();
        if (saveMe == true) {
            myOrders.writeOrderListToFile();
        }
    }
    
}
