/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sg.flooringmastery.ui;

import com.sg.flooringmastery.dto.Order;
import com.sg.flooringmastery.dto.Product;
import com.sg.flooringmastery.dto.Tax;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;

/**
 *
 * @author apprentice
 */
public class UI {

    ConsoleIO cio = new ConsoleIO();

    public void printOrders(ArrayList<Order> orders, String date) {

        cio.sout("-----DISPLAYING ORDERS-----");
        displayDate(date);
        orders.forEach(order -> cio.sout(order.toString()));

    }

    public String promptForDate() {
        String date = "";
        boolean success = false;
        do {
            try {
                while (!(date.length() == 8)) {

                    date = cio.getString("Please enter a date in the MMDDYYYY format");

                }

                SimpleDateFormat sdf = new SimpleDateFormat("MMddyyyy");
                sdf.setLenient(false);
                sdf.parse(date);
                success = true;
            } catch (ParseException pe) {
                System.out.println("Not a real date...");
                date = "";
            }
        } while (success == false);
        return date;
    }

    public Boolean promptSave() {

        boolean saveMe = false;
        int choice = cio.getIntMinMax("Save changes?\n"
                + "1. Yes\n"
                + "2. No", 1, 2);

        if (choice == 1) {
            saveMe = true;
        }

        return saveMe;
    }

    public void displayDate(String date) {

        String[] splitDate;
        splitDate = date.split("");

        String month = splitDate[0] + splitDate[1];
        String day = splitDate[2] + splitDate[3];
        String year = splitDate[4] + splitDate[5] + splitDate[6] + splitDate[7];

        cio.sout("\nACTIVE DATE: " + month + "/" + day + "/" + year);

    }

    public int mainMenuPrompt(String date) {

        displayDate(date);
        int selection = cio.getIntMinMax("Please selection an action: \n"
                + "1. Switch active date\n"
                + "2. Add an order\n"
                + "3. Modify an order\n"
                + "4. Remove an order\n"
                + "5. Display today's orders\n"
                + "6. Save\n"
                + "7. Exit", 1, 7);
        return selection;
    }

    public Order promptAddInfo(ArrayList<Product> myProducts, ArrayList<Tax> myTaxes, ArrayList<Order> myOrders) {
        cio.sout("-----ADD ORDER-----");
        Order order = new Order();

        order.setOrderNumber(myOrders.size() + 1);

        order.setCustomerName(cio.getString("Customer Name: "));

        boolean validState = false;
        String state;
        while (validState == false) {
            state = cio.getString("State code (ex- OH): ");

            for (Tax myTax : myTaxes) {
                if (myTax.getState().equalsIgnoreCase(state)) {

                    validState = true;
                    order.setState(myTax.getState());
                    order.setTaxRate(myTax.getTaxRate());
                }
            }
            if (validState == false) {
                cio.sout("Invalid entry...");
            }
        }

        boolean validProduct = false;
        String productType;
        while (validProduct == false) {
            productType = cio.getString("Product Type: ");

            for (Product myProduct : myProducts) {
                if (myProduct.getProductType().equalsIgnoreCase(productType)) {
                    validProduct = true;
                    order.setProductType(myProduct.getProductType());
                    order.setMaterialCostPerSqFt(myProduct.getMaterialCostPerSqFt());
                    order.setLaborCostPerSqFt(myProduct.getLaborCostPerSqFt());
                }
            }
            if (validProduct == false) {
                cio.sout("Invalid entry...");
            }
        }
        order.setArea(cio.getDoubleMinMax("Area: ", .1, 1000000));
        return order;
    }

    public int confirmOrder(Order newOrder) {
        cio.sout("\nThis order?\n" + newOrder.toString());
        int input = cio.getIntMinMax("\n1. Confirm\n2. Cancel", 1, 2);

        return input;
    }

    public Order promptModifyInfo(ArrayList<Product> products, ArrayList<Tax> taxes, ArrayList<Order> orders, Order order) {

        //MODIFY NAME
        String newName = cio.getString("Customer Name (" + order.getCustomerName() + "): ");
        if (!(newName.isEmpty())) {
            order.setCustomerName(newName);
        }

        //MODIFY STATE
        boolean validState = false;
        String state;
        while (validState == false) {
            state = cio.getString("State code (" + order.getState() + "): ");

            if (state.isEmpty()) {
                validState = true;
            } else {

                for (Tax myTax : taxes) {
                    if (myTax.getState().equalsIgnoreCase(state)) {

                        validState = true;
                        order.setState(myTax.getState());
                        order.setTaxRate(myTax.getTaxRate());
                    }
                }
                if (validState == false) {
                    cio.sout("Invalid entry...");
                }
            }
        }

        //MODIFY PRODUCT
        boolean validProduct = false;
        String productType;
        while (validProduct == false) {
            productType = cio.getString("Product Type (" + order.getProductType() + "): ");

            if (productType.isEmpty()) {
                validProduct = true;
            } else {

                for (Product myProduct : products) {
                    if (myProduct.getProductType().equalsIgnoreCase(productType)) {
                        validProduct = true;
                        order.setProductType(myProduct.getProductType());
                        order.setMaterialCostPerSqFt(myProduct.getMaterialCostPerSqFt());
                        order.setLaborCostPerSqFt(myProduct.getLaborCostPerSqFt());
                    }
                }
                if (validProduct == false) {
                    cio.sout("Invalid entry...");
                }
            }
        }

        //MODIFY AREA
        boolean areaSuccess = false;
        String stringArea;
        double area;
        do {

            stringArea = cio.getString("Area (" + order.getArea() + "): ");

            if (stringArea.isEmpty()) {
                areaSuccess = true;
            } else {

                area = Double.parseDouble(stringArea);

                if (area >= .1 && area <= 1000000) {
                    areaSuccess = true;
                    order.setArea(area);
                } else {
                    System.out.println("That is not within range.");
                }
            }
        } while (areaSuccess == false);

        return order;
    }
}
