/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sg.flooringmastery.dao;

import com.sg.flooringmastery.dto.Order;
import java.util.ArrayList;
import org.junit.Test;
import static org.junit.Assert.*;
import org.junit.Before;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 *
 * @author apprentice
 */
public class OrderListTests {

    OrderList testOL;

    public OrderListTests() {
    }

    @Before
    public void setUp() {
        ApplicationContext ctx = new ClassPathXmlApplicationContext("applicationContext.xml");
        testOL = (OrderList) ctx.getBean("daoInMemImpl");
    }

    @Test
    public void testCalculateOrder() {
        Order myOrder = new Order(10, "Bob", "OH", .08, "Wood",
                100, 12, 8);

        testOL.calculateOrder(myOrder);

        assertEquals(800, myOrder.getLaborCost(), .001);
        assertEquals(1200, myOrder.getMaterialCost(), .001);
        assertEquals(160, myOrder.getTax(), .001);
        assertEquals(2160, myOrder.getTotalCost(), .001);

    }

    @Test
    public void testSortOrders() {
        testOL.clearOrders();

        Order myOrder = new Order(1, "Bob", "OH", .08, "Wood",
                100, 12, 8);
        myOrder = testOL.calculateOrder(myOrder);
        testOL.addOrder(myOrder);

        Order myOrder2 = new Order(2, "Robert", "PA", .1, "Laminate", 100,
                10, 8);
        myOrder2 = testOL.calculateOrder(myOrder2);
        testOL.addOrder(myOrder2);

        Order myOrder3 = new Order(3, "Ringo", "TX", .1, "Laminate", 100,
                10, 8);
        myOrder3 = testOL.calculateOrder(myOrder3);
        testOL.addOrder(myOrder3);

        testOL.removeOrder(0);//remove first order
        testOL.sortOrders();

        assertEquals(1, testOL.getOrders().get(0).getOrderNumber());
        assertEquals(2, testOL.getOrders().get(1).getOrderNumber());
        assertEquals("Robert", testOL.getOrders().get(0).getCustomerName());
        assertEquals("Ringo", testOL.getOrders().get(1).getCustomerName());

    }

    @Test
    public void testAddOrder() {
        testOL.clearOrders();

        Order myOrder = new Order();

        testOL.addOrder(myOrder);

        ArrayList<Order> list = testOL.getOrders();

        assertEquals(1, list.size());

        Order dos = new Order();

        testOL.addOrder(dos);

        list = testOL.getOrders();

        assertEquals(2, list.size());

    }

    @Test
    public void testSetAndGetDate() {
        String date = "01012016";
        testOL.setDate(date);

        assertEquals(date, testOL.getDate());

    }

    @Test
    public void testClearOrders() {
        Order myOrder = new Order();

        testOL.addOrder(myOrder);

        testOL.clearOrders();

        assertEquals(0, testOL.getOrders().size());

    }

    @Test
    public void testRemoveOrder() {

        Order myOrder = new Order();
        testOL.addOrder(myOrder);

        int startSize = testOL.getOrders().size();

        testOL.removeOrder(0);

        assertEquals(startSize - 1, testOL.getOrders().size());

    }

    @Test
    public void testModifyOrder() {

        testOL.clearOrders();

        Order myOrder = new Order(10, "Bob", "OH", .08, "Wood",
                100, 12, 8);
        myOrder = testOL.calculateOrder(myOrder);
        testOL.addOrder(myOrder);

        Order modifiedOrder = new Order(10, "Robert", "PA", .1, "Laminate", 100,
                10, 8);
        modifiedOrder = testOL.calculateOrder(modifiedOrder);

        testOL.modifyOrder(0, modifiedOrder);

        assertEquals("Robert", testOL.getOrders().get(0).getCustomerName());
        assertEquals("PA", testOL.getOrders().get(0).getState());
        assertEquals(.1, testOL.getOrders().get(0).getTaxRate(), .001);
        assertEquals("Laminate", testOL.getOrders().get(0).getProductType());
        assertEquals(10, testOL.getOrders().get(0).getMaterialCostPerSqFt(), .001);
        assertEquals(1000, testOL.getOrders().get(0).getMaterialCost(), .001);

    }

}
