/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sg.shapesandperimeters;

/**
 *
 * @author apprentice
 */
public class SquareShape extends Shape{
    
    private double length;
    
    @Override
    public void area(){
        
        double a = getLength()*getLength();
        System.out.println("Area: "+ a);
    }
    
    
    @Override
    public void perimeter(){
        double p = getLength()*4;
        System.out.println("Perimeter: "+ p);
    }

    /**
     * @return the length
     */
    public double getLength() {
        return length;
    }

    /**
     * @param length the length to set
     */
    public void setLength(double length) {
        this.length = length;
    }
    
    
}
