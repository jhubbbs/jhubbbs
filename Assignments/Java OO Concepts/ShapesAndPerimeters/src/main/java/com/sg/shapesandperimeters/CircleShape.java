/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sg.shapesandperimeters;

/**
 *
 * @author apprentice
 */
public class CircleShape extends Shape{
    private double r;
    
    @Override
    public void area(){
        double a = 3.14*(getR()*getR());
        System.out.println("A: "+a);
    }
    @Override
    public void perimeter(){
        double p = 2*3.14*getR();
        System.out.println("P: "+ p);
    }

    /**
     * @return the r
     */
    public double getR() {
        return r;
    }

    /**
     * @param r the r to set
     */
    public void setR(double r) {
        this.r = r;
    }
}
