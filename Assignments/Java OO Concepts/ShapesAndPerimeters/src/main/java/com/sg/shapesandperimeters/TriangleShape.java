/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sg.shapesandperimeters;

/**
 *
 * @author apprentice
 */
public class TriangleShape extends Shape {
    private double one;
    private double two;
    private double three;
    private double oneHeight;
    
    @Override
    public void area(){
        double a = (getOne()*getOneHeight())/2;
        System.out.println("A: "+a);
    }
    @Override
    public void perimeter(){
        double p = getOne() + getTwo() + getThree();
        System.out.println("P: "+p);
    }

    /**
     * @return the one
     */
    public double getOne() {
        return one;
    }

    /**
     * @param one the one to set
     */
    public void setOne(double one) {
        this.one = one;
    }

    /**
     * @return the two
     */
    public double getTwo() {
        return two;
    }

    /**
     * @param two the two to set
     */
    public void setTwo(double two) {
        this.two = two;
    }

    /**
     * @return the three
     */
    public double getThree() {
        return three;
    }

    /**
     * @param three the three to set
     */
    public void setThree(double three) {
        this.three = three;
    }

    /**
     * @return the oneHeight
     */
    public double getOneHeight() {
        return oneHeight;
    }

    /**
     * @param oneHeight the oneHeight to set
     */
    public void setOneHeight(double oneHeight) {
        this.oneHeight = oneHeight;
    }
}
