/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sg.shapesandperimeters;

/**
 *
 * @author apprentice
 */
public class RectangleShape extends Shape{
    
    private double l;
    private double w;
    
    @Override
    public void area(){
        double a = getL()*getW();
        System.out.println("A: "+a);
    }
    @Override
    public void perimeter(){
        double p = (2*getL()) + (2*getW());
        System.out.println("P: "+p);
    }

    /**
     * @return the l
     */
    public double getL() {
        return l;
    }

    /**
     * @param l the l to set
     */
    public void setL(double l) {
        this.l = l;
    }

    /**
     * @return the w
     */
    public double getW() {
        return w;
    }

    /**
     * @param w the w to set
     */
    public void setW(double w) {
        this.w = w;
    }
    
}
