/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import com.sg.dvdlibrary.dao.DVDLibrary;
import com.sg.dvdlibrary.dto.DVD;
import java.util.ArrayList;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author apprentice
 */
public class DVDLibraryTests {
    
    public DVDLibraryTests() {
    }

    // TODO add test methods here.
    // The methods must be annotated with annotation @Test. For example:
    //
    // @Test
    // public void hello() {}
    @Test
    public void testGetDVDByID(){
        DVDLibrary myLib = new DVDLibrary();
        
        myLib.addDVD("Persona", "1933", "R", "Bergman", "Janus", "no note");
        myLib.addDVD("Band of Outsiders", "1965", "pg13", "Godard", "Frenchh", "this note");
        myLib.addDVD("Faust", "1926", "G", "Murnau", "Deutche", "mephisto");
        
        ArrayList<DVD> newLib = myLib.getDVDById(0);
        assertEquals(newLib.get(0).getTitle(), "Persona");
        
        ArrayList<DVD> extraLib = myLib.getDVDById(1);
        assertEquals(extraLib.get(0).getDirector(), "Godard");
        
        ArrayList<DVD> finalLib = myLib.getDVDById(2);
        assertEquals(finalLib.get(0).getReleaseDate(), "1926");
        
    }
    
}
