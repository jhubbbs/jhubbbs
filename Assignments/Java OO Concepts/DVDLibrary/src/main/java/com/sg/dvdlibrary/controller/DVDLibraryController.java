/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sg.dvdlibrary.controller;

import com.sg.dvdlibrary.dto.DVD;
import com.sg.dvdlibrary.dao.DVDLibrary;
import com.sg.dvdlibrary.ui.ConsoleIO;
import java.util.ArrayList;

/**
 *
 * @author apprentice
 */
public class DVDLibraryController {

    ConsoleIO cio = new ConsoleIO();
    DVDLibrary controlled = new DVDLibrary();
    
    String title;
    String releaseDate;
    String mpaa;
    String director;
    String studio;
    String note;

    public void run() {
        
        boolean done = false;

        controlled.readDVDLibraryFromFile();
        
        int selection;

        while (done == false) {

            selection = cio.getIntMinMax("\nSelect an option: \n"
                    + "1. Add DVD\n"
                    + "2. Remove DVD\n"
                    + "3. Modify DVD\n"
                    + "4. List All DVDs\n"
                    + "5. Give Size of Library\n"
                    + "6. Search DVD by Title\n"
                    + "7. Exit\n"
                    + "", 1, 7);

            switch (selection) {
                case 1:
                    add();
                    break;
                case 2:
                    remove();
                    break;
                case 3:
                    modify();
                    break;
                case 4:
                    printAll();
                    break;
                case 5:
                    cio.sout("\n---COUNT DVDS---\n");
                    cio.sout("Number of DVDs: " + controlled.getDVDCount());
                    break;
                case 6:
                    searchByTitle();
                    break;
                default:
                    //EXIT
                    done = true;
                    controlled.writeDVDLibraryToFile();
                    cio.sout("\n---GOODBYE!---\n");
                    break;
            }

        }

    }

    public void add() {
        cio.sout("\n---ENTER NEW DVD---\n");
        title = cio.getString("Enter the DVD title: ");
        releaseDate = cio.getString("Enter the release date: ");
        mpaa = cio.getString("Enter the MPAA Rating: ");
        director = cio.getString("Enter the director: ");
        studio = cio.getString("Enter the studio: ");
        note = cio.getString("Enter a personal note: ");

        controlled.addDVD(title, releaseDate, mpaa, director, studio, note);
    }

    public void remove() {
        int id;
        cio.sout("\n---REMOVE A DVD---\n");
        id = selectAmongTitle();
        if (id >= 0) {
            controlled.removeDVD(id);
            cio.sout("\nDVD removed!\n");
        }
    }

    public void modify() { //BROKEN, pics weird ones to mod

        cio.sout("\n---MODIFY A DVD---\n");
        int id = selectAmongTitle();
        ArrayList<DVD> smallList = new ArrayList<DVD>();
        if (id >= 0) {

            smallList = controlled.getDVDById(id); //HERE BROKE ME THINKS

            printDVDLibrary(smallList);

            int field = cio.getIntMinMax("\nSelect a field: \n"
                    + "1. Title\n"
                    + "2. Release Date\n"
                    + "3. MPAA Rating\n"
                    + "4. Director\n"
                    + "5. Studio\n"
                    + "6. Note\n"
                    + "", 1, 6);
            String info = cio.getString("Change it to what?");
            controlled.modifyDVD(id, field, info);
            cio.sout("\nDVD modified to:\n");
            printDVDLibrary(smallList);
        }
    }

    public void printAll() {

        ArrayList<DVD> libCopy = new ArrayList<DVD>();

        cio.sout("\n---LIST ALL DVDS---\n");
        libCopy = controlled.getDVDLibrary();
        printDVDLibrary(libCopy);
        cio.sout("\n");
    }

    public void searchByTitle() {
        ArrayList<DVD> smallList = new ArrayList<DVD>();
        cio.sout("\n---DVDS WITH TITLE---\n");
        title = cio.getString("Enter the title: ");
        smallList = controlled.getDVDByTitle(title);
        cio.sout("DVD(s) with that title:\n");
        printDVDLibrary(smallList);

    }

    public void printDVDLibrary(ArrayList<DVD> lib) {
        for (int x = 0; x < lib.size(); x++) {
            cio.sout((x + 1) + ": ");
            cio.sout(lib.get(x).getTitle() + " (" + lib.get(x).getReleaseDate() + ")");

            cio.sout("by " + lib.get(x).getDirector() + " via " + lib.get(x).getStudio());
            cio.sout(lib.get(x).getMpaa());
            cio.sout("Note - " + lib.get(x).getNote() + "\n");
        }
    }

    public int selectAmongTitle() {

        int selection;
        int id = -1;
        ArrayList<DVD> smallList = new ArrayList<DVD>();
        title = cio.getString("Enter the title: ");
        smallList = controlled.getDVDByTitle(title);
        cio.sout("\n");

        switch (smallList.size()) {
            case 0:
                System.out.println("No records found.\n");
                break;
            case 1:
                id = smallList.get(0).getId();
                break;
            default:
                System.out.println("Which one did you mean?\n");
                printDVDLibrary(smallList);
                selection = cio.getIntMinMax("Which DVD number?\n", 1, smallList.size()) - 1;
                id = smallList.get(selection).getId();
                break;
        }

        return id;

    }

}
