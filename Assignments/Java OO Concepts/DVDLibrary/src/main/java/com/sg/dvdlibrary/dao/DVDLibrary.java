/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sg.dvdlibrary.dao;

import com.sg.dvdlibrary.dto.DVD;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Scanner;

/**
 *
 * @author apprentice
 */
public class DVDLibrary {

    private String OUTPUT_FILE = "SavedData/OutFile.txt";
    private ArrayList<DVD> lib = new ArrayList<DVD>();

    private void writeDVDToFile(DVD myDVD) {

        try {

            PrintWriter outPrinter = new PrintWriter(
                    new FileOutputStream(new File(OUTPUT_FILE), true));
            outPrinter.println(myDVD.encode());
            outPrinter.flush();

            outPrinter.close();

        } catch (FileNotFoundException fnf) {
            System.out.println("We cannot find the file: " + OUTPUT_FILE);
            System.out.println(fnf.getMessage());
        } catch (IOException io) {
            System.out.println("Problem writing our sample file");
            System.out.println(io.getMessage());
        }
    }

    public void writeDVDLibraryToFile() {
        try {
            PrintWriter out = new PrintWriter(new FileWriter(OUTPUT_FILE));
            for (int x = 0; x < lib.size(); x++) {
                writeDVDToFile(lib.get(x));
            }
        } catch (FileNotFoundException fnf) {
            System.out.println("We cannot find the file: " + OUTPUT_FILE);
            System.out.println(fnf.getMessage());
        } catch (IOException io) {
            System.out.println("Problem writing our sample file");
            System.out.println(io.getMessage());
        }

    }

    public void readDVDLibraryFromFile() {

        try {
            Scanner sc = new Scanner(new BufferedReader(new FileReader(OUTPUT_FILE)));
            // go through the file line-by-line
            // using Scanner's iterator
            // While there are things in the Scanner's buffer
            while (sc.hasNextLine()) {
                // Read in the next line
                String stuffToReadIn = sc.nextLine();

                DVD myDVD = new DVD();
                myDVD = myDVD.decode(stuffToReadIn);
                // Write out the object
                lib.add(myDVD);
            }
        } catch (FileNotFoundException fnf) {
            // tell the user we can't read a file with the location
            System.out.println("Problem reading from file: " + OUTPUT_FILE);
            // show a message for our sanity
            System.out.println(fnf.getMessage());
        }
    }

    public void addDVD(String title, String releaseDate, String mpaa,
            String director, String studio, String note) {

        lib.add(new DVD());
        int id = lib.size() - 1;

        lib.get(id).setId(id);
        lib.get(id).setTitle(title);
        lib.get(id).setReleaseDate(releaseDate);
        lib.get(id).setMpaa(mpaa);
        lib.get(id).setDirector(director);
        lib.get(id).setStudio(studio);
        lib.get(id).setNote(note);

    }

    public void removeDVD(int id) {
        int arrayId;

        for (int x = 0; x < lib.size(); x++) {
            if (lib.get(x).getId() == id) {
                arrayId = x;
                lib.remove(arrayId);
            }
        }

    }

    public void modifyDVD(int id, int field, String info) {

        for (int x = 0; x < lib.size(); x++) {
            if (lib.get(x).getId() == id) {

                switch (field) {
                    case 1:
                        lib.get(x).setTitle(info);
                        break;
                    case 2:
                        lib.get(x).setReleaseDate(info);
                        break;
                    case 3:
                        lib.get(x).setMpaa(info);
                        break;
                    case 4:
                        lib.get(x).setDirector(info);
                        break;
                    case 5:
                        lib.get(x).setStudio(info);
                        break;
                    default:
                        lib.get(x).setNote(info);
                        break;
                }

            }
        }

    }

    public ArrayList<DVD> getDVDById(int id) {

        ArrayList<DVD> highlander = new ArrayList<DVD>();

        int checkIt;
        for (int x = 0; x < lib.size(); x++) {
            checkIt = lib.get(x).getId();

            if (id == checkIt) {
                highlander.add(lib.get(x));
            }
        }

        return highlander;
    }

    public int getDVDCount() {

        return lib.size();
    }

    public ArrayList<DVD> getDVDLibrary() {

        return lib;
    }

    public ArrayList<DVD> getDVDByTitle(String title) {

        ArrayList<DVD> smallList = new ArrayList<DVD>();

        for (int x = 0; x < lib.size(); x++) {

            if (lib.get(x).getTitle().contains(title)) {
                smallList.add(lib.get(x));
            }

        }
        return smallList;
    }
}
