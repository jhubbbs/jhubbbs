/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sg.dvdlibrary.ui;

import java.util.Scanner;

/**
 *
 * @author apprentice
 */
public class ConsoleIO {

    Scanner sc = new Scanner(System.in);

    public int getInt(String prompt) {
        System.out.println(prompt);
        String stringInput = sc.nextLine();
        int input = Integer.parseInt(stringInput);
        
        return input;

    }
    
    public void sout(String out){
        System.out.println(out);
    }

    public int getIntMinMax(String prompt, int min, int max) {
        boolean success = false;
        int input;
        do {
            System.out.println(prompt);
            input = sc.nextInt();
            sc.nextLine();
            if (input >= min && input <= max) {
                success = true;
            } else {
                System.out.println("That is not within range.");
            }
        } while (success == false);
        return input;
    }

    public String getString(String prompt) {
        System.out.println(prompt);
        return sc.nextLine();
    }

    public float getFloat(String prompt) {
        System.out.println(prompt);
        float input = sc.nextFloat();
        sc.nextLine();
        return input;
    }

    public float getFloatMinMax(String prompt, float min, float max) {
        boolean success = false;
        float input;
        do {
            System.out.println(prompt);
            input = sc.nextFloat();
            sc.nextLine();
            if (input >= min && input <= max) {
                success = true;
            } else {
                System.out.println("That is not within range.");
            }
        } while (success == false);
        return input;
    }

    public double getDouble(String prompt) {
        System.out.println(prompt);
        double input = sc.nextDouble();
        sc.nextLine();
        return input;
    }

    public double getDoubleMinMax(String prompt, double min, double max) {
        boolean success = false;
        double input;
        do {
            System.out.println(prompt);
            input = sc.nextDouble();
            sc.nextLine();
            if (input >= min && input <= max) {
                success = true;
            } else {
                System.out.println("That is not within range.");
            }
        } while (success == false);
        return input;
    }

    public void printString(String prompt) {
        System.out.println(prompt);
        String input = sc.nextLine();
        System.out.println(input);

    }

}
