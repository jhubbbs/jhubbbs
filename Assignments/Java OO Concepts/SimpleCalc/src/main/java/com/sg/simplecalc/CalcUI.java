/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sg.simplecalc;

import java.util.Scanner;

/**
 *
 * @author apprentice
 */
public class CalcUI {

    Scanner sc = new Scanner(System.in);
    ConsoleIO cio = new ConsoleIO();
    SimpleCalc calc = new SimpleCalc();

    public void run() {

        boolean finished = false;
        double num1, num2;
        String operator;
        do {
            operator = cio.getString("Please enter +, -, *, or /. Or, enter anything else to exit.");
            if (operator.equals("+")) {

                num1 = cio.getDouble("Please enter the first number");
                num2 = cio.getDouble("Please enter the second number");

                System.out.println(calc.add(num1, num2));

            } else if (operator.equals("-")) {
                num1 = cio.getDouble("Please enter the first number");
                num2 = cio.getDouble("Please enter the second number");

                System.out.println(calc.subtract(num1, num2));

            } else if (operator.equals("*")) {
                num1 = cio.getDouble("Please enter the first number");
                num2 = cio.getDouble("Please enter the second number");

                System.out.println(calc.multiply(num1, num2));

            } else if (operator.equals("/")) {
                num1 = cio.getDouble("Please enter the first number");
                num2 = cio.getDouble("Please enter the second number");

                System.out.println(calc.divide(num1, num2));

            } else {
                finished = true;
            }

        } while (finished == false);
        System.out.println("Thanks for using SimpleCalc^tm");

    }
}
