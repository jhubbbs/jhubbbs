/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sg.addressbookv2.controller;

import com.sg.addressbookv2.dto.Address;
import com.sg.addressbookv2.dao.AddressBook;
import com.sg.addressbookv2.ui.ConsoleIO;
import java.util.ArrayList;

/**
 *
 * @author apprentice
 */
public class AddressBookController {

    ConsoleIO cio = new ConsoleIO();
    AddressBook controlled = new AddressBook();

    public void run() {

        boolean done = false;
        final int MIN = 1;
        final int MAX = 7;
        int selection;

        String firstName;
        String lastName;
        String street;
        int zip;
        String city;
        String state;
        ArrayList<Address> smallList = new ArrayList<Address>();
        ArrayList<Address> aBookCopy = new ArrayList<Address>();
        int id;

        controlled.readAddressBookFromFile();

        while (done == false) {

            selection = cio.getIntMinMax("\nSelect an option: \n"
                    + "1. Add Address\n"
                    + "2. Remove Address\n"
                    + "3. Modify Address\n"
                    + "4. List All Addresses\n"
                    + "5. Give Number of Addresses\n"
                    + "6. Search Addresses\n"
                    + "7. Exit\n"
                    + "", MIN, MAX);

            if (selection == 1) {
                System.out.println("\n---ENTER NEW ADDRESS---\n");
                lastName = cio.getString("Enter the last name: ");
                firstName = cio.getString("Enter the first name: ");
                street = cio.getString("Enter the street: ");
                city = cio.getString("Enter the city: ");
                state = cio.getString("Enter the state: ");
                zip = cio.getInt("Enter the ZIP: ");

                controlled.addAddress(firstName, lastName, street, city, state, zip);

            } else if (selection == 2) {
                System.out.println("\n---REMOVE AN ADDRESS---\n");
                id = selectAmongLastName();
                if (id >= 0) {
                    controlled.removeAddress(id);
                    System.out.println("\nAddress removed!\n");
                }

            } else if (selection == 3) {
                System.out.println("\n---MODIFY AN ADDRESS---\n");
                id = selectAmongLastName();

                if (id >= 0) {

                    smallList = controlled.getAddressById(id);

                    printAddressBook(smallList);

                    int field = cio.getIntMinMax("\nSelect a field: \n"
                            + "1. Last Name\n"
                            + "2. First Name\n"
                            + "3. Street Address\n"
                            + "4. City\n"
                            + "5. State\n"
                            + "6. ZIP\n"
                            + "", 1, 6);
                    String info = cio.getString("Change it to what?");
                    controlled.modifyAddress(id, field, info);
                    System.out.println("\nAddress modified to:\n");
                    printAddressBook(smallList);
                }
            } else if (selection == 4) {
                System.out.println("\n---LIST ALL ADDRESSES---\n");
                aBookCopy = controlled.getAddressBook();
                printAddressBook(aBookCopy);
                System.out.println("\n");

            } else if (selection == 5) {
                System.out.println("\n---COUNT ADDRESSES---\n");
                System.out.println("Number of addresses: " + controlled.getAddressCount());

            } else if (selection == 6) {
                System.out.println("\n---SEARCH ADDRESSES---\n");
                search();

            } else {
                done = true;
                controlled.writeAddressBookToFile();
                System.out.println("\n---GOODBYE!---\n");
            }

        }

    }

    public void printAddressBook(ArrayList<Address> aBook) {
        for (int x = 0; x < aBook.size(); x++) {
            System.out.print((x + 1) + ": ");
            System.out.print(aBook.get(x).getFirstName() + " " + aBook.get(x).getLastName() + "\n");
            System.out.println(aBook.get(x).getStreet());
            System.out.println(aBook.get(x).getCity() + ", " + aBook.get(x).getState()
                    + " " + aBook.get(x).getZip() + "\n");

        }
    }

    public int selectAmongLastName() {

        int selection;
        int id = -1;
        String lastName;
        ArrayList<Address> smallList = new ArrayList<Address>();
        lastName = cio.getString("Enter the last name: ");
        smallList = controlled.getAddressByLastName(lastName);
        System.out.println("\n");
        if (smallList.size() == 0) {
            System.out.println("No records found.\n");
        } else if (smallList.size() == 1) {
            id = smallList.get(0).getId();
        } else {
            System.out.println("Which one did you mean?\n");
            printAddressBook(smallList);

            selection = cio.getIntMinMax("Which address number?\n", 1, smallList.size()) - 1;
            id = smallList.get(selection).getId();
        }

        return id;

    }

    public void search() {
        
        int selection = cio.getIntMinMax("Search by what?\n"
                + "1. Last name\n"
                + "2. City\n"
                + "3. State\n"
                + "4. ZIP\n", 1, 4);
        switch (selection) {
            case 1:
                {
                    String lastName = cio.getString("Enter the last name: ");
                    ArrayList<Address> smallList = controlled.getAddressByLastName(lastName);
                    System.out.println("Address(es) with that last name:\n");
                    printAddressBook(smallList);
                    break;
                }
            case 2:
                {
                    String city = cio.getString("Enter the city: ");
                    ArrayList<Address> smallList = controlled.getAddressByCity(city);
                    System.out.println("Address(es) with that city:\n");
                    printAddressBook(smallList);
                    break;
                }
            case 3:
                {
                    String state = cio.getString("Enter the state: ");
                    ArrayList<Address> smallList = controlled.getAddressByState(state);
                    System.out.println("Address(es) with that state:\n");
                    printAddressBook(smallList);
                    break;
                }
            default:
                {
                    int zip = cio.getInt("Enter the ZIP: ");
                    ArrayList<Address> smallList = controlled.getAddressByZip(zip);
                    System.out.println("Address(es) with that ZIP:\n");
                    printAddressBook(smallList);
                    break;
                }
        }
        
    }
}
