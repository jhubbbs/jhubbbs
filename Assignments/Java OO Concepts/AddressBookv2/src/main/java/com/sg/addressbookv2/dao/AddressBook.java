/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sg.addressbookv2.dao;

import com.sg.addressbookv2.dto.Address;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import java.util.stream.Collectors;

/**
 *
 * @author apprentice
 */
public class AddressBook implements AddressBookInterface {

    private String OUTPUT_FILE = "SavedData/OutFile.txt";
    private ArrayList<Address> aBook = new ArrayList<Address>();

    @Override
    public void writeAddressToFile(Address addy) {

        try {

            PrintWriter outPrinter = new PrintWriter(
                    new FileOutputStream(new File(OUTPUT_FILE), true));
            outPrinter.println(addy.encode());
            outPrinter.flush();

            outPrinter.close();

        } catch (FileNotFoundException fnf) {
            System.out.println("We cannot find the file: " + OUTPUT_FILE);
            System.out.println(fnf.getMessage());
        } catch (IOException io) {
            System.out.println("Problem writing our sample file");
            System.out.println(io.getMessage());
        }
    }

    @Override
    public void writeAddressBookToFile() {
        try {
            PrintWriter out = new PrintWriter(new FileWriter(OUTPUT_FILE));
            for (int x = 0; x < aBook.size(); x++) {
                writeAddressToFile(aBook.get(x));
            }
        } catch (FileNotFoundException fnf) {
            System.out.println("We cannot find the file: " + OUTPUT_FILE);
            System.out.println(fnf.getMessage());
        } catch (IOException io) {
            System.out.println("Problem writing our sample file");
            System.out.println(io.getMessage());
        }

    }

    @Override
    public void readAddressBookFromFile() {

        try {
            Scanner sc = new Scanner(new BufferedReader(new FileReader(OUTPUT_FILE)));
            // go through the file line-by-line
            // using Scanner's iterator
            // While there are things in the Scanner's buffer
            while (sc.hasNextLine()) {
                // Read in the next line
                String stuffToReadIn = sc.nextLine();
                // We need to decode this into a student object:
                Address myAddress = new Address();
                myAddress = myAddress.decode(stuffToReadIn);
                // Write out the object
                aBook.add(myAddress);
            }
        } catch (FileNotFoundException fnf) {
            // tell the user we can't read a file with the location
            System.out.println("Problem reading from file: " + OUTPUT_FILE);
            // show a message for our sanity
            System.out.println(fnf.getMessage());
        }
    }

    @Override
    public void addAddress(String firstName, String lastName, String street,
            String city, String state, int zip) {

        aBook.add(new Address());
        int id = aBook.size() - 1;

        aBook.get(id).setId(id);
        aBook.get(id).setFirstName(firstName);
        aBook.get(id).setLastName(lastName);
        aBook.get(id).setStreet(street);
        aBook.get(id).setCity(city);
        aBook.get(id).setState(state);
        aBook.get(id).setZip(zip);

    }

    @Override
    public void removeAddress(int id) {
        int arrayId;

        for (int x = 0; x < aBook.size(); x++) {
            if (aBook.get(x).getId() == id) {
                arrayId = x;
                aBook.remove(arrayId);
            }
        }

    }

    @Override
    public void modifyAddress(int id, int field, String info) {

        switch (field) {
            case 1:
                aBook.get(id).setLastName(info);
                break;
            case 2:
                aBook.get(id).setFirstName(info);
                break;
            case 3:
                aBook.get(id).setStreet(info);
                break;
            case 4:
                aBook.get(id).setCity(info);
                break;
            case 5:
                aBook.get(id).setState(info);
                break;
            default:
                int zip = Integer.parseInt(info);
                aBook.get(id).setZip(zip);
                break;
        }

    }

    @Override
    public ArrayList<Address> getAddressById(int id) {

        ArrayList<Address> highlander = new ArrayList<Address>();
        highlander.add(aBook.get(id));

        return highlander;
    }

    @Override
    public int getAddressCount() {

        return aBook.size();
    }

    @Override
    public ArrayList<Address> getAddressBook() {

        return aBook;
    }

    @Override
    public ArrayList<Address> getAddressByLastName(String lastName) {

        ArrayList<Address> smallList = new ArrayList<Address>();
                
                
        aBook.stream()
            .filter(address -> address.getLastName().equals(lastName))
            .forEach(address ->smallList.add(address));


        return smallList;
    }
    
    @Override
    public ArrayList<Address> getAddressByCity(String city){
        
        ArrayList<Address> smallList = new ArrayList<Address>();
                
                
        aBook.stream()
            .filter(address -> address.getCity().equals(city))
            .forEach(address ->smallList.add(address));
        
        
        return smallList;
    }
    
    @Override
    public ArrayList<Address> getAddressByState(String state){
        
        ArrayList<Address> smallList = new ArrayList<Address>();
                
                
        aBook.stream()
            .filter(address -> address.getState().equals(state))
            .forEach(address ->smallList.add(address));
        
        
        return smallList;
    }
    
    @Override
    public ArrayList<Address> getAddressByZip(int zip){
        
        ArrayList<Address> smallList = new ArrayList<Address>();
                
                
        aBook.stream()
            .filter(address -> address.getZip() == zip)
            .forEach(address ->smallList.add(address));
        
        
        return smallList;
    }
    

}
