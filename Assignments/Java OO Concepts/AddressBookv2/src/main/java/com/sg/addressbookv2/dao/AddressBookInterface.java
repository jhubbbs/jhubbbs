/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sg.addressbookv2.dao;

import com.sg.addressbookv2.dto.Address;
import java.util.ArrayList;

/**
 *
 * @author apprentice
 */
public interface AddressBookInterface {

    public void writeAddressToFile(Address addy);

    public void writeAddressBookToFile();

    public void readAddressBookFromFile();

    public void addAddress(String firstName, String lastName, String street,
            String city, String state, int zip);

    public void removeAddress(int id);

    public void modifyAddress(int id, int field, String info);

    public ArrayList<Address> getAddressById(int id);

    public int getAddressCount();

    public ArrayList<Address> getAddressBook();

    public ArrayList<Address> getAddressByLastName(String lastName);

    public ArrayList<Address> getAddressByCity(String city);

    public ArrayList<Address> getAddressByState(String state);

    public ArrayList<Address> getAddressByZip(int zip);

}
