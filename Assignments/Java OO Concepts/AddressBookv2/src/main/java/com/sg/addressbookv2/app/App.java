/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sg.addressbookv2.app;

import com.sg.addressbookv2.controller.AddressBookController;

/**
 *
 * @author apprentice
 */
public class App {
    public static void main(String[] args) {
        AddressBookController book = new AddressBookController();
        book.run();
    }
}
